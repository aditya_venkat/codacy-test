package com.cars.df.customerinquiry.utility;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cars.df.customerinquiry.bo.DealerReviewView;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;


public class DealerReviewViewValidator implements Validator{

	@Override
	public boolean supports(Class<?> review) {
		// TODO Auto-generated method stub
		return DealerReviewView.class.equals(review);
	}

	@Override
	public void validate(Object review, Errors errors) {
		// TODO Auto-generated method stub
		
		//Review Nullcheck
		if(review == null){
			errors.reject(Constants.ERROR_CNSMR_REVIEW);
		} else{
			DealerReviewView dealerReviewView = (DealerReviewView)review;


			//Review Title null check and also the size of it(should be between 5 and 50)
			if(StringUtils.isBlank(dealerReviewView.getReviewTitle())){
				errors.reject(Constants.ERROR_CNSMR_REVIEW_TITLE);
			}
			else if(!(dealerReviewView.getReviewTitle().trim().length() >= 5 && dealerReviewView.getReviewTitle().trim().length() <= 50)){
				errors.reject(Constants.INVL_CNSMR_REVIEW_TITLE);
			}


			//Review Text null check and also the size of it(should be between 50 and 5000)
			if(StringUtils.isBlank(dealerReviewView.getReviewText())){
				errors.reject(Constants.ERROR_CNSMR_REVIEW_TEXT);
			}
			else if(!(dealerReviewView.getReviewText().trim().length() >= 50 && dealerReviewView.getReviewText().trim().length() <= 4000)){
				errors.reject(Constants.INVL_CNSMR_REVIEW_TEXT);
			}



			// Email address should not be null or empty
			if(!EmailValidator.getInstance().isValid(dealerReviewView.getReviewerEmail())){
				errors.reject(Constants.INVL_CNSMR_REVIEW_EMAIL);
			}



			//Display name should not be null or empty
			if(StringUtils.isBlank(dealerReviewView.getReviewerName())){
				errors.reject(Constants.ERROR_CNSMR_REVIEW_DSP_NAME);
			}


			//Location description should not be null or empty
			if(StringUtils.isBlank(dealerReviewView.getReviewerLoc())){
				errors.reject(Constants.ERROR_CNSMR_REVIEW_LOCATION);
			}


			


			//There should be atleast one visiting reason
			if (!(dealerReviewView.getVisitNewCar() || dealerReviewView.getVisitServiceCar() || dealerReviewView.getVisitUsedCar())){
				errors.reject(Constants.INVL_CNSMR_REVIEW_VST_REASON);
			}

		
			//Check whether dealer or not
			if(!dealerReviewView.getIsDealer()){
				errors.reject(Constants.INVL_IS_DEALER);
			}
			
			
			
			//Check for ratings
			if(StringUtils.isBlank(dealerReviewView.getOverallRating())){
				errors.reject(Constants.ERROR_CNSMR_REVIEW_OVRALL_RATING);
			} else{
				validateRating(dealerReviewView.getOverallRating(), "OverallRating", errors);
			}

			if(!dealerReviewView.getNoCustomerServiceRating()){
				validateRating(dealerReviewView.getCustomerServiceRating(), "CustomerRating", errors);	
			}
			
			if(!dealerReviewView.getNoBuyingRating()){
				validateRating(dealerReviewView.getBuyingProcessRating(), "BuyingRating", errors);	
			}
			
			if(!dealerReviewView.getNoRepairsRating()){
				validateRating(dealerReviewView.getQualityOfRepairRating(), "RepairRating",errors);	
			}
			
			if(!dealerReviewView.getNoFacilitiesRating()){
				validateRating(dealerReviewView.getFacilityRating(), "FacilitiesRating", errors);	
			}
			
			//TODO: Do we need to do this????
			/*if(dealerReviewView.getRecommend() == null){
				//errors.reject(add some random message);
			}
			
			if(dealerReviewView.getPurchase() == null){
				//errors.reject(add some random message);
			}*/
		}
	}
	
	private void validateRating(String ratingString, String type, Errors errors){
		try{
			Integer ratingValue = Integer.parseInt(ratingString);
			if(!(ratingValue > 1 && ratingValue < 5)){
				errors.reject( type + " " + Constants.INVL_CNSMR_REVIEW_RATING);
			}
		} catch(Exception e){
			e.printStackTrace();
			errors.reject("Exception in " + type + " Rating Value." + Constants.NUMBER_EXCEPTION_CNSMR_REVIEW_RATING);
		}
	
	}
	
}
