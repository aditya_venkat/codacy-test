package com.cars.df.customerinquiry.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cars.framework.config.ConfigService;

@RestController
@Component
public class ConfigController {

	@Autowired
    private ConfigService configService;

    @RequestMapping(value="/config/refresh", method= RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<Map<String, String>> configRefresh() {
        configService.refresh();

        Map<String, String> message = new HashMap<>();
        message.put("status", "refresh successful");

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

	public ConfigService getConfigService() {
		return configService;
	}

	public void setConfigService(ConfigService configService) {
		this.configService = configService;
	}
}
