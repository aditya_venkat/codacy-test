package com.cars.df.customerinquiry.dao;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.cars.mdis.common.bo.ProductResponse;

public class UtilityDAO {

private RestTemplate restTemplate; 
	
	private String mdisHostName;
	
	private String mdisContext; 
	
	public String getMakeIdFromMakeName(String makeName) {
		
		String makeId = "";

		ResponseEntity<ProductResponse> response;
		
		String url = mdisHostName + mdisContext + "/rest/translate-canonical/" + makeName.toLowerCase() + "?attribute=CarsMakeID";			
		response = restTemplate.getForEntity(url, ProductResponse.class);
		
		
		if(response == null) {
			return makeId; 
		}
		
		ProductResponse responseXML = response.getBody(); 
		
		if(responseXML != null && responseXML.getAttributes() != null && responseXML.getAttributes().size() > 0) {
			makeId = responseXML.getAttributes().get(0).getValue();
		}
		return makeId; 
	}

	public String getMdisHostName() {
		return mdisHostName;
	}

	public void setMdisHostName(String mdisHostName) {
		this.mdisHostName = mdisHostName;
	}

	public String getMdisContext() {
		return mdisContext;
	}

	public void setMdisContext(String mdisContext) {
		this.mdisContext = mdisContext;
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
}
