package com.cars.df.customerinquiry.client;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

import com.cars.api.schemas.lead.NonInventoryLead;
import com.cars.rs.client.api.ServiceCustomer;
import com.cars.ss.lois.bo.generated.LocalOffers;

public class ServiceClients {

	private RestClient client;

	private String loisHost;
	private String loisContext;

	private String repairServiceHost;
	private String repairServiceContext;

	private String leadpathHost;
	private String leadpathContext;

	private static final String OFFER_DETAILS = "details=Y";
	private static final String REPAIRSERVICE_CUSTID = "/repair/customer/customerId";

	public String getOfferAndModelCount(String partyId) {

		StringBuilder getOffderAndModelCountUrl = new StringBuilder();

		getOffderAndModelCountUrl.append(loisHost);
		getOffderAndModelCountUrl.append(loisContext);
		getOffderAndModelCountUrl.append("/offerAndModelCount");
		getOffderAndModelCountUrl.append("/" + partyId);

		String offerAndModelCount = "0,0";
		try {
			Resource resource = client.resource(getOffderAndModelCountUrl
					.toString());

			offerAndModelCount = resource.get(String.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return offerAndModelCount;
	}

	public LocalOffers getLocalOffersByDealerId(String partyId) {

		StringBuilder getOffersByDealerId = new StringBuilder();

		getOffersByDealerId.append(loisHost);
		getOffersByDealerId.append(loisContext);
		getOffersByDealerId.append("/" + partyId);
		getOffersByDealerId.append("?" + OFFER_DETAILS);

		LocalOffers localOffers = null;
		try {

			Resource resource = client.resource(getOffersByDealerId.toString());
			localOffers = resource.contentType(MediaType.APPLICATION_XML)
					.accept(MediaType.APPLICATION_XML).get(LocalOffers.class);

		} catch (Exception e) {
			e.printStackTrace();
			localOffers = new LocalOffers();

		}

		return localOffers;
	}

	public ServiceCustomer getServiceRepairDetails(String customerId) {

		StringBuilder getRepairServiceDetailsUrl = new StringBuilder();

		getRepairServiceDetailsUrl.append(repairServiceHost);
		getRepairServiceDetailsUrl.append(repairServiceContext);
		getRepairServiceDetailsUrl.append(REPAIRSERVICE_CUSTID);
		getRepairServiceDetailsUrl.append("/");
		getRepairServiceDetailsUrl.append(customerId);
		ServiceCustomer serviceCustomer = null;
		try {

			Resource resource = client.resource(getRepairServiceDetailsUrl
					.toString());
			serviceCustomer = resource.contentType(MediaType.APPLICATION_XML)
					.accept(MediaType.APPLICATION_XML)
					.get(ServiceCustomer.class);

		} catch (Exception e) {
			e.printStackTrace();
			serviceCustomer = new ServiceCustomer();

		}

		return serviceCustomer;

	}

	public Boolean sendDealerEmailLead(NonInventoryLead lead) {

		StringBuilder sendDealerEmailLeadUrl = new StringBuilder();

		sendDealerEmailLeadUrl.append(leadpathHost);
		sendDealerEmailLeadUrl.append(leadpathContext);

		try {
			Resource resource = client.resource(sendDealerEmailLeadUrl
					.toString());

			// TODO: Potentially change behavior based on save results.
			String returnedLead = resource.contentType(MediaType.TEXT_XML)
					.accept(MediaType.APPLICATION_XML).post(String.class, lead);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public RestClient getClient() {
		return client;
	}

	public void setClient(RestClient client) {
		this.client = client;
	}

	public String getLoisHost() {
		return loisHost;
	}

	public void setLoisHost(String loisHost) {
		this.loisHost = loisHost;
	}

	public String getLoisContext() {
		return loisContext;
	}

	public void setLoisContext(String loisContext) {
		this.loisContext = loisContext;
	}

	public String getRepairServiceHost() {
		return repairServiceHost;
	}

	public void setRepairServiceHost(String repairServiceHost) {
		this.repairServiceHost = repairServiceHost;
	}

	public String getRepairServiceContext() {
		return repairServiceContext;
	}

	public void setRepairServiceContext(String repairServiceContext) {
		this.repairServiceContext = repairServiceContext;
	}

	public String getLeadpathHost() {
		return leadpathHost;
	}

	public void setLeadpathHost(String leadpathHost) {
		this.leadpathHost = leadpathHost;
	}

	public String getLeadpathContext() {
		return leadpathContext;
	}

	public void setLeadpathContext(String leadpathContext) {
		this.leadpathContext = leadpathContext;
	}

}
