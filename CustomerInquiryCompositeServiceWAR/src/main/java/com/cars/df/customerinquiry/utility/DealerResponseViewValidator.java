
package com.cars.df.customerinquiry.utility;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cars.df.customerinquiry.bo.DealerResponseView;

public class DealerResponseViewValidator implements Validator{

	@Override
	public boolean supports(Class responseView) {
		return DealerResponseView.class.equals((responseView));
	}

	@Override
	public void validate(Object responseView, Errors errors) {

		if(responseView == null){
			errors.reject(Constants.ERROR_DEALER_REPONSE_VIEW);
		} else{
			DealerResponseView dealerResponseView = (DealerResponseView)responseView;
			
			
			if(StringUtils.isBlank(dealerResponseView.getName())){
				errors.reject(Constants.ERROR_DEALER_REPONSE_VIEW_NAME);

			}
			
			if(StringUtils.isBlank(dealerResponseView.getText())){
				errors.reject(Constants.ERROR_DEALER_REPONSE_VIEW_TEXT);
			}
			
			if(!EmailValidator.getInstance().isValid(dealerResponseView.getEmail())){
				errors.reject(Constants.INVL_DEALER_REPONSE_VIEW_EMAIL);
			}
			
			if(!CICSUtility.isValidPhoneNumber(dealerResponseView.getPhoneNumber())){
				errors.reject(Constants.INVL_DEALER_REPONSE_VIEW_PHONE);
			}

			if(!CICSUtility.isValidZip(dealerResponseView.getZipCode())){
				errors.reject(Constants.INVL_DEALER_REPONSE_VIEW_ZIP);
			}
		}
		
	}

}
