package com.cars.df.customerinquiry.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.cars.df.customerinquiry.bo.DealerLocalOfferAddOn;
import com.cars.df.customerinquiry.bo.DealerLocalOfferCash;
import com.cars.df.customerinquiry.bo.DealerLocalOfferDealerPromo;
import com.cars.df.customerinquiry.bo.DealerLocalOfferFinance;
import com.cars.df.customerinquiry.bo.DealerLocalOfferLease;
import com.cars.df.customerinquiry.bo.DealerLocalOfferService;
import com.cars.df.customerinquiry.bo.DealerSpecialOfferDetails;
import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerLocalOffer;
import com.cars.df.customerinquiry.bo.YearMakeModelOffer;
import com.cars.ss.lois.bo.generated.LocalOffer;
import com.cars.ss.lois.bo.generated.LocalOffers;
import com.cars.ss.lois.bo.generated.OfferCash;
import com.cars.ss.lois.bo.generated.OfferFinance;
import com.cars.ss.lois.bo.generated.OfferLease;

public class DealerLocalOfferUtilityMapper {
	
	private DealerCommonUtilityMapper dealerCommonUtilityMapper;

	private static final Integer SINGLE_DEALER = 1;

	private static final String DEALER_PROMO = "DealerPromotion";
	
	public DealerSpecialOfferDetails mapLocalOfferBOToYearMakeModeOfferBO(Dealers dealers,LocalOffers localOfferList){
	
		DealerSpecialOfferDetails dealerSpecialOfferDetails = new DealerSpecialOfferDetails();
		if(dealers != null && dealers.getDealers() != null && dealers.getDealers().size() == SINGLE_DEALER){
			Dealer dealer = dealers.getDealers().get(0);
			
			//Common dealer details
			dealerCommonUtilityMapper.createDealerDetailSummary(dealer, dealerSpecialOfferDetails);
			
			Map<String, YearMakeModelOffer> ymmo = new HashMap<String, YearMakeModelOffer>();

			if(localOfferList != null && localOfferList.getLocalOffer() != null && localOfferList.getLocalOffer().size() > 0){
				for(LocalOffer localOffer: localOfferList.getLocalOffer()){
									
					// Get year Id for description
					String yearMakeModelId = "";
					if(localOffer.getVehicleYearId() == null && localOffer.getVehicleMakeId() == null && localOffer.getVehicleModelId() == null){
						yearMakeModelId = DEALER_PROMO;
					} else if(localOffer.getVehicleYearId() != null && localOffer.getVehicleMakeId() != null && localOffer.getVehicleModelId() != null){
						yearMakeModelId = localOffer.getVehicleYearDescription() + " " +  localOffer.getVehicleMakeDescription() +  " " + localOffer.getVehicleModelDescription();
					} else if(localOffer.getVehicleYearId() != null && localOffer.getVehicleMakeId() != null){
						yearMakeModelId = localOffer.getVehicleYearDescription() + " " + localOffer.getVehicleMakeDescription();
					}
					
					
					if(StringUtils.isNotBlank(yearMakeModelId)){
						Boolean hasYearMakeModelId = this.hasYMMId(yearMakeModelId, ymmo);
						
						if(!hasYearMakeModelId){
							this.addNewMMYIdToMap(yearMakeModelId, ymmo);
							
						}
						YearMakeModelOffer yearMakeModelOffer = ymmo.get(yearMakeModelId);
						this.addOffer(localOffer, yearMakeModelOffer);
						yearMakeModelOffer.incrementCount();
						yearMakeModelOffer.setName(yearMakeModelId);
					}
				}
				
				//this.addNameAndCount(ymmo);
			}
			
			dealerSpecialOfferDetails.setYearMakeModelOfferList(new ArrayList<YearMakeModelOffer>(ymmo.values()));
			
		}
		
		return dealerSpecialOfferDetails;
	}
	
	/*private void addNameAndCount(Map<String, YearMakeModelOffer> ymmo){
		
		for(String key: ymmo.keySet()){
			
			YearMakeModelOffer yearMakeModelOffer = ymmo.get(key);
			
			yearMakeModelOffer.setName(key);
			yearMakeModelOffer.setCount(ymmo.values().size());
		}
	}*/
	
	private Boolean hasYMMId(String yearMakeModelId , Map<String, YearMakeModelOffer> ymmo){
		
		if(ymmo.get(yearMakeModelId) != null){
			return true;
		}
		return false;
	}
	
	
	private void addNewMMYIdToMap(String yearMakeModelId, Map<String, YearMakeModelOffer> ymmo){
		YearMakeModelOffer yearMakeModelOfferBO = new YearMakeModelOffer();
		ymmo.put(yearMakeModelId, yearMakeModelOfferBO);
	}
	
		
	private void addOffer(LocalOffer localOffer, YearMakeModelOffer yearMakeModelOffer){
		if(localOffer.getType() != null){
			switch(localOffer.getType().getCode()){
			case "ADD_ON":
				yearMakeModelOffer.addAddonOffer(createDealerLocalOfferAddOn(localOffer));
				break;
			case "CASH":
				yearMakeModelOffer.addCashOffer(createDealerLocalOfferCash(localOffer));
				break;
			case "DLR_PROMO":
				yearMakeModelOffer.addDealerpromoOffer(createDealerLocalOfferDealerPromo(localOffer));
				break;
			case "FINANCE":
				yearMakeModelOffer.addFinanceOffer(createDealerLocalOfferFinance(localOffer));
				break;
			case "LEASE":
				yearMakeModelOffer.addLeaseOffer(createDealerLocalOfferLease(localOffer));
				break;
			case "SERVICE":
				yearMakeModelOffer.addServiceOffer(createDealerLocalOfferService(localOffer));
				break;
			default:
				//do some shit here
		}	
		}
	}
	
	private DealerLocalOfferAddOn createDealerLocalOfferAddOn(LocalOffer localOffer){
		
		DealerLocalOfferAddOn dealerLocalOfferAddOn = new DealerLocalOfferAddOn();
		
		this.createLocalOffer(localOffer, dealerLocalOfferAddOn);
		
		if(localOffer.getAddOn() != null){
			dealerLocalOfferAddOn.setOfferTitle(localOffer.getAddOn().getDescription());
		}
		//?? Do we need to do this.??
		//OfferAddOn offerAddOn = localOffer.getAddOn();
		//offerAddOn.get		
		
		
		return dealerLocalOfferAddOn;
	}
	
	private DealerLocalOfferCash createDealerLocalOfferCash(LocalOffer localOffer){
		
		DealerLocalOfferCash dealerLocalOfferCash = new DealerLocalOfferCash();
		this.createLocalOffer(localOffer, dealerLocalOfferCash);
		
		OfferCash offerCash = localOffer.getCash();
		
		if(offerCash != null){
			dealerLocalOfferCash.setCashbackAmount(offerCash.getAmount());
			dealerLocalOfferCash.setCashLabel(offerCash.getCashLabel());
		}
		
		
		if(localOffer.getCash() != null && localOffer.getCash().getAmount() != null){
			if(localOffer.getCash().getAmount() - localOffer.getCash().getAmount().intValue() > 0){
				dealerLocalOfferCash.setOfferTitle(String.format("$%.2f %s ", localOffer.getCash().getAmount(), localOffer.getCash().getCashLabel()));
			}
			else{
				dealerLocalOfferCash.setOfferTitle(String.format("$%.0f %s",localOffer.getCash().getAmount(), localOffer.getCash().getCashLabel()));
			}
		}
		
		return dealerLocalOfferCash;
	}
	
	private DealerLocalOfferLease createDealerLocalOfferLease(LocalOffer localOffer){
		
			DealerLocalOfferLease dealerLocalOfferLease = new DealerLocalOfferLease();
			this.createLocalOffer(localOffer, dealerLocalOfferLease);
			//do some local offer lease shit and add it to dealer local offer lease object
			if(localOffer.getLease() != null){
				if(localOffer.getLease().size() > 0){
					dealerLocalOfferLease.setOfferTitle("Dealership lease specials");
				} else{
					OfferLease offerLease = localOffer.getLease().get(0);
					if(offerLease != null){
						dealerLocalOfferLease.setOfferTitle(String.format("%s/month for %.0f months", CICSUtility.formatCurrency(offerLease.getMonthlyPayment()), offerLease.getMonthlyTerm()));
					
						if(offerLease.getDueAtSigning() != null && offerLease.getDueAtSigning() != 0){
							dealerLocalOfferLease.setOfferTitle(String.format(dealerLocalOfferLease.getOfferTitle() + "%s due at signing", CICSUtility.formatCurrency(offerLease.getDueAtSigning())));
						}
					}
				}
			}
		
		return dealerLocalOfferLease; 
		
	}
	
	private DealerLocalOfferFinance createDealerLocalOfferFinance(LocalOffer localOffer){		
			DealerLocalOfferFinance dealerLocalOfferFinance = new DealerLocalOfferFinance();
			this.createLocalOffer(localOffer, dealerLocalOfferFinance);
			//do all local offer finance shit and add it to dealer local offer finance object
			
			if(localOffer.getFinancing() != null){
				if(localOffer.getFinancing().size() > 1){
					if(localOffer.isOemInd()){
						dealerLocalOfferFinance.setOfferTitle("Manufacturer financing specials");
					} else{
						dealerLocalOfferFinance.setOfferTitle("Dealership financing specials");
					}
					
				} else {
					OfferFinance offerFinance = localOffer.getFinancing().get(0);
					
					if(offerFinance != null){
						dealerLocalOfferFinance.setOfferTitle(String.format( "%s for %.0f months", CICSUtility.formatPercentage(offerFinance.getApr()), offerFinance.getTerm()));
						
						if ((offerFinance.getBonusCash() != null) && (offerFinance.getBonusCash() != 0)) {
							dealerLocalOfferFinance.setOfferTitle(String.format(dealerLocalOfferFinance.getOfferDescription() + " %s Bonus Cash", CICSUtility.formatCurrency(offerFinance.getBonusCash())));
						}
					} 
				}
			}
			
			return dealerLocalOfferFinance;
	}
	
	private DealerLocalOfferService createDealerLocalOfferService(LocalOffer localOffer){
		DealerLocalOfferService dealerLocalOfferService = new DealerLocalOfferService();
		this.createLocalOffer(localOffer, dealerLocalOfferService);
		
		if(localOffer.getService() != null){
			dealerLocalOfferService.setOfferTitle(localOffer.getService().getDescription());
		}
		
		
		return dealerLocalOfferService;
	}
	
	private DealerLocalOfferDealerPromo createDealerLocalOfferDealerPromo(LocalOffer localOffer){
		DealerLocalOfferDealerPromo dealerLocalOfferPromo = new DealerLocalOfferDealerPromo();
		this.createLocalOffer(localOffer, dealerLocalOfferPromo);
		
		return dealerLocalOfferPromo;
	}
	
	
	/*
	  What is make logo client??? http://www.cars.com/go/core/data/oem/makeLogos.json
	  What is research Client???? 
	  What is listingID?? Why do we need it??
	  
	  
	  private void getListingId(){
	  }
	  private void getPhotoUrl(LocalOffer localOffer){
		
		if(localOffer != null && localOffer.getPhoto() == null){
			if(localOffer.getVehicleMakeId() != null && localOffer.getVehicleMakeId() != null && localOffer.getVehicleYearId() != null){
				String photo = researchClient.getDefaultPhoto(offer.getVehicleMakeId(), offer.getVehicleModelId(), offer.getVehicleYearId());
				.setPhoto(photo);
			}
			else if(offer.getMedia() != null && offer.getMedia().size() != 0 && 
					!StringUtils.isStringEmpty(offer.getMedia().get(0).getMediaURI())){
				String photoURI = offer.getMedia().get(0).getMediaURI();
				String photoUrl = baseUrlMediaImage + photoURI;
				offer.setPhoto(photoUrl);
			}
		}
	}*/
	
	private void createLocalOffer(LocalOffer localOffer, DealerLocalOffer dealerLocalOffer){
		
		dealerLocalOffer.setOfferId(localOffer.getOfferId());
		dealerLocalOffer.setType(localOffer.getType().getCode());
		dealerLocalOffer.setPartyId(String.valueOf(localOffer.getDealerPartyId()));
		dealerLocalOffer.setProductId(String.valueOf(localOffer.getProductId()));
		dealerLocalOffer.setStockType(localOffer.getStockType());
		dealerLocalOffer.setStockNumber(localOffer.getStockNum());
		
		//should we use Long or String for ID????
		dealerLocalOffer.setVehicleYearId(String.valueOf(localOffer.getVehicleYearId()));
		dealerLocalOffer.setVehicleYearDescription(localOffer.getVehicleYearDescription());
		dealerLocalOffer.setVehicleMakeId(String.valueOf(localOffer.getVehicleMakeId()));
		dealerLocalOffer.setVehicleMakeDescription(localOffer.getVehicleMakeDescription());
		dealerLocalOffer.setVehicleModelId(String.valueOf(localOffer.getVehicleModelId()));
		dealerLocalOffer.setVehicleModelDescription(localOffer.getVehicleModelDescription());
		dealerLocalOffer.setVehicleTrimId(String.valueOf(localOffer.getVehicleTrimId()));
		dealerLocalOffer.setVehicleTrimDescription(localOffer.getVehicleTrimDescription());
		dealerLocalOffer.setVehicleBodyStyleId(String.valueOf(localOffer.getVehicleBodyStyleId()));
		dealerLocalOffer.setVehicleBodyStyleDescription(localOffer.getVehicleBodyStyleIdDescription());
		dealerLocalOffer.setIsOem(localOffer.isOemInd());
		dealerLocalOffer.setIsCertified(localOffer.isCertifiedInd());
		dealerLocalOffer.setIsDisplay(localOffer.isDisplayInd());
		//set this stuff
		//dealerLocalOffer.setPhotoUrl(localOffer.p);
		//dealerLocalOffer.setListingId(localOffer.Lis);
		
		dealerLocalOffer.setOfferDisclaimer(localOffer.getOfferDisclaimer());
		dealerLocalOffer.setOfferDisclaimer(localOffer.getOfferDisclaimer());
		//dealerLocalOffer.setOfferDescription(localOffer.getDes);
		
		
		//do some check here for date xmlgregorian calendar to Date
		if(localOffer.getStartDate() != null){
			dealerLocalOffer.setStartDate(localOffer.getStartDate().toString());
		}
		if(localOffer.getEndDate() != null){
			dealerLocalOffer.setEndDate(localOffer.getEndDate().toString());
		}
		
		
		
		    //protected OfferType type; -- do we need description??? 
		   // protected OfferTarget target; -- do we need to check STYMM, STYM .....??
		   //protected List<OfferMedia> media;
		
	}

	public DealerCommonUtilityMapper getDealerCommonUtilityMapper() {
		return dealerCommonUtilityMapper;
	}

	public void setDealerCommonUtilityMapper(DealerCommonUtilityMapper dealerCommonUtilityMapper) {
		this.dealerCommonUtilityMapper = dealerCommonUtilityMapper;
	}

	
}
