	package com.cars.df.customerinquiry.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;

import com.cars.df.customerinquiry.bo.DealerDetails;
import com.cars.df.customerinquiry.bo.DealerOverviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewSummaryView;
import com.cars.df.customerinquiry.bo.DealerSearchResults;
import com.cars.df.customerinquiry.bo.Pagination;
import com.cars.df.customerinquiry.bo.Sort;
import com.cars.df.customerinquiry.bo.SortType;
import com.cars.df.customerinquiry.dao.CustomerSearchDAO;
import com.cars.df.customerinquiry.dao.InventorySearchDAO;
import com.cars.df.customerinquiry.dao.SpecialOfferDAO;
import com.cars.df.customerinquiry.dao.UtilityDAO;
import com.cars.df.customerinquiry.utility.CICSUtility;
import com.cars.df.customerinquiry.utility.Constants;
import com.cars.df.customerinquiry.utility.DealerSummaryMapper;
import com.cars.service.listingsearch.bo.RefinementBO;
import com.cars.service.listingsearch.bo.RefinementValueBO;
import com.cars.service.listingsearch.bo.SearchResultBO;
import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.DealerSearchCriteriaBO;
import com.cars.df.css.bo.Dealers;

public class CustomerSearchService {

	private static final Integer SINGLE_DEALER = 1;
	
	private static final String OVERVIEW_REVIEW_START_INDEX = "0";
	
	private static final String OVERVIEW_REVIEW_COUNT = "3";
	
	private CustomerSearchDAO customerSearchDAO;
	
	
	private InventorySearchDAO inventorySearchDAO;
	
	private DealerReviewService dealerReviewService;
	
	private SpecialOfferDAO specialOfferDAO;
	
	private DealerSummaryMapper dealerSummaryMapper;
	
	
	private UtilityDAO utilityDAO; 
	
	//TODO: Change this to searchSalesDealers
	public DealerSearchResults searchSalesDealers(String zip, String[] mkId, String radius, String sortBy, String sortOrder, String startPage, String numResults, String keyword, String ratingId, String dealerTypeId) {
		
		DealerSearchCriteriaBO criteria = this.buildSearchCriteria(zip, mkId, radius, sortBy, sortOrder, startPage, numResults, keyword,ratingId,null,dealerTypeId);
		
		Dealers dealers = customerSearchDAO.searchDealers(criteria); 
		
		DealerSearchResults dealerSearchResults = dealerSummaryMapper.mapDealerToDealerSummary(dealers);
		
		this.addSearchDetails(dealerSearchResults, criteria);
		
		return dealerSearchResults;
	}
	
	public DealerSearchResults searchServiceDealers(String zip, String[] makeIds, String radius, String sortBy, String sortOrder, String startPage, String numResults, String keyword,
													String ratingId, String certifiedId, String dealerTypeId){
		DealerSearchCriteriaBO criteria = this.buildSearchCriteria(zip, makeIds, radius, sortBy, sortOrder, startPage, numResults, keyword, ratingId,certifiedId,dealerTypeId);
		
		//criteria.setRating(rating);
		Dealers dealers = customerSearchDAO.searchDealers(criteria); 
		
		DealerSearchResults dealerSearchResults = dealerSummaryMapper.mapDealerToDealerSummary(dealers);
		
		this.addSearchDetails(dealerSearchResults, criteria);
		
		return dealerSearchResults;
	}
	
	private DealerSearchCriteriaBO buildSearchCriteria(String zip, String[] makeIds, String radius, String sortBy, String sortOrder, String startPage, String numResults, String keyword,
			String ratingId, String certificationId, String serviceCenterId){
		
		DealerSearchCriteriaBO criteria = new DealerSearchCriteriaBO(); 
		
		criteria.setZip(zip);
		criteria.setRadius(radius);
		criteria.setSortDirection(sortOrder);
		if(makeIds != null){
			criteria.setMakes(Arrays.asList(makeIds));
		}
		
		criteria.setRating(ratingId);
		criteria.setRpcCertified(certificationId);
		criteria.setDealerType(serviceCenterId);
	
		
		if(NumberUtils.isNumber(radius)){
			criteria.setRadius(radius);
		}else {
			criteria.setRadius("0");
		}
		
		if(NumberUtils.isNumber(startPage) && NumberUtils.isNumber(numResults)){
			Integer pageIndex = ((Integer.valueOf(startPage) - 1) * Integer.valueOf(numResults)) + 1;
			String  pageNumber = String.valueOf(pageIndex);
			criteria.setPageNumber(pageNumber);
		}else {
			criteria.setPageNumber("0");
		}
		
		if(NumberUtils.isNumber(numResults)){
			criteria.setResultsPerPage(numResults);
		}else {
			criteria.setResultsPerPage("0");
		}
		
		if(StringUtils.isNotBlank(sortBy)){
			if("RATING".equalsIgnoreCase(sortBy)){
				criteria.setSortColumn("OVERALL_RATING");
				criteria.setSecondarySortColumn("DISTANCE");
				criteria.setSecondarySortDirection("ASC");
				
			} else if("DEALERNAME".equalsIgnoreCase(sortBy)){
				criteria.setSortColumn("NAME");
				criteria.setSecondarySortColumn("DISTANCE");
				criteria.setSecondarySortDirection("ASC");
			} else if("CERTIFIED_REPAIR".equalsIgnoreCase(sortBy)){
				//sort by certified repair code
			} else if("DISTANCE".equalsIgnoreCase(sortBy)){
				criteria.setSortColumn("DISTANCE");
			}
			
		}else{
			criteria.setSortColumn("DISTANCE");
		}
		
		if(StringUtils.isNotBlank(keyword)){
			
			keyword = keyword.replace(" ", "*") + "*";
			
			if(keyword.contains("&")){
				criteria.setMatch("EXACT");
			}
			else{
				criteria.setMatch("ALL");
			}
			criteria.setKeyword(keyword);
		}
		
		return criteria;
	}

	
	public String convertCustomerIdtoPartyId(String customerId){
		
		String partyId = customerSearchDAO.convertCustomerIdToPartyId(customerId);
		
		return partyId;
		
	}
	
	public String convertPartyIdToCustomerId(String partyId){
		
		return customerSearchDAO.convertPartyIdToCustomerId(partyId);
		
	}
	
	private void addSearchDetails(DealerSearchResults dealerSearchResults, DealerSearchCriteriaBO dealerSearchCriteriaBO){
		
		//add exception block
		Integer pageNumber = Integer.valueOf(dealerSearchCriteriaBO.getPageNumber()) + 1;
		Integer resultsPerPage = Integer.valueOf(dealerSearchCriteriaBO.getResultsPerPage());
		Integer numberOfPages = (int) Math.ceil(dealerSearchResults.getTotalDealers() / resultsPerPage);

		Pagination pagination = new Pagination(pageNumber, resultsPerPage, numberOfPages);
		
		//setting sort
		SortType primarySort = new SortType(dealerSearchCriteriaBO.getSortColumn(), dealerSearchCriteriaBO.getSortDirection());
		
		SortType secondarySort = new SortType(dealerSearchCriteriaBO.getSecondarySortColumn(), dealerSearchCriteriaBO.getSecondarySortDirection());
		
		SortType tertiarySort = new SortType(dealerSearchCriteriaBO.getTertiarySortColumn(), dealerSearchCriteriaBO.getTertiarySortDirection());
		
		Sort sort = new Sort(primarySort, secondarySort, tertiarySort);
		
		dealerSearchResults.setPagination(pagination);
		
		dealerSearchResults.setSort(sort);
		
		dealerSearchResults.setRadius(dealerSearchCriteriaBO.getRadius());
		
		dealerSearchResults.setZipCode(dealerSearchCriteriaBO.getZip());
		
	}

	public CustomerSearchDAO getCustomerSearchDAO() {
		return customerSearchDAO;
	}

	public void setCustomerSearchDAO(CustomerSearchDAO customerSearchDAO) {
		this.customerSearchDAO = customerSearchDAO;
	}

	public UtilityDAO getUtilityDAO() {
		return utilityDAO;
	}

	public void setUtilityDAO(UtilityDAO utilityDAO) {
		this.utilityDAO = utilityDAO;
	}


	public InventorySearchDAO getInventorySearchDAO() {
		return inventorySearchDAO;
	}

	public void setInventorySearchDAO(InventorySearchDAO inventorySearchDAO) {
		this.inventorySearchDAO = inventorySearchDAO;
	}

	public DealerReviewService getDealerReviewService() {
		return dealerReviewService;
	}

	public void setDealerReviewService(DealerReviewService dealerReviewService) {
		this.dealerReviewService = dealerReviewService;
	}


	public SpecialOfferDAO getSpecialOfferDAO() {
		return specialOfferDAO;
	}


	public void setSpecialOfferDAO(SpecialOfferDAO specialOfferDAO) {
		this.specialOfferDAO = specialOfferDAO;
	}


	public DealerSummaryMapper getDealerSummaryMapper() {
		return dealerSummaryMapper;
	}


	public void setDealerSummaryMapper(DealerSummaryMapper dealerSummaryMapper) {
		this.dealerSummaryMapper = dealerSummaryMapper;
	}



}
