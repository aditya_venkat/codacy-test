package com.cars.df.customerinquiry.service;

import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerServiceRepairDetails;
import com.cars.df.customerinquiry.dao.CustomerSearchDAO;
import com.cars.df.customerinquiry.dao.ServiceRepairDAO;
import com.cars.df.customerinquiry.utility.DealerServiceRepairUtilityMapper;
import com.cars.rs.client.api.ServiceCustomer;

public class DealerServiceRepairService {

	private ServiceRepairDAO serviceRepairDAO;
	
	private CustomerSearchDAO customerSearchDAO;
	
	private DealerServiceRepairUtilityMapper dealerServiceRepairUtilityMapper;
	
	public DealerServiceRepairDetails getServiceRepairDetails(String customerId){
		
		String partyId = customerSearchDAO.convertCustomerIdToPartyId(customerId);
		
		Dealers dealers =  customerSearchDAO.getDealer(partyId); 
		
		ServiceCustomer serviceCustomer = serviceRepairDAO.getServiceRepairDetails(customerId);
		
		
		DealerServiceRepairDetails dealerServiceRepairDetails = dealerServiceRepairUtilityMapper.convertServiceCustomerToServiceRepairDetails(dealers,serviceCustomer);
		
		return dealerServiceRepairDetails;
		
	}

	public ServiceRepairDAO getServiceRepairDAO() {
		return serviceRepairDAO;
	}

	public void setServiceRepairDAO(ServiceRepairDAO serviceRepairDAO) {
		this.serviceRepairDAO = serviceRepairDAO;
	}

	public DealerServiceRepairUtilityMapper getDealerServiceRepairUtilityMapper() {
		return dealerServiceRepairUtilityMapper;
	}

	public void setDealerServiceRepairUtilityMapper(DealerServiceRepairUtilityMapper dealerServiceRepairUtilityMapper) {
		this.dealerServiceRepairUtilityMapper = dealerServiceRepairUtilityMapper;
	}

	public CustomerSearchDAO getCustomerSearchDAO() {
		return customerSearchDAO;
	}

	public void setCustomerSearchDAO(CustomerSearchDAO customerSearchDAO) {
		this.customerSearchDAO = customerSearchDAO;
	}
}
