package com.cars.df.customerinquiry.controller;


import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cars.df.customerinquiry.bo.DealerOverviewDetails;
import com.cars.df.customerinquiry.bo.DealerResponseView;
import com.cars.df.customerinquiry.bo.DealerReviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewView;
import com.cars.df.customerinquiry.bo.DealerSearchResults;
import com.cars.df.customerinquiry.bo.DealerServiceRepairDetails;
import com.cars.df.customerinquiry.bo.DealerSpecialOfferDetails;
import com.cars.df.customerinquiry.service.CustomerSearchService;
import com.cars.df.customerinquiry.service.DealerOverviewService;
import com.cars.df.customerinquiry.service.DealerReviewService;
import com.cars.df.customerinquiry.service.DealerServiceRepairService;
import com.cars.df.customerinquiry.service.DealerSpecialOfferService;
import com.cars.df.customerinquiry.utility.DealerEmailLeadValidator;
import com.cars.df.customerinquiry.utility.DealerResponseViewValidator;
import com.cars.df.customerinquiry.utility.DealerReviewViewValidator;
import com.cars.framework.config.feature.FeatureFlag;
import com.cars.df.css.bo.ReturnCode;

import io.swagger.annotations.Api;

@RestController
@Api(value = "/", description = "Operations about Dealer Inquiry stuffs")
public class CustomerInquiryController {
	
	@Autowired
	private String testName; 
	
	@Autowired
	private CustomerSearchService customerSearchService; 
	
	@Autowired
	private DealerReviewService dealerReviewService; 
	
	@Autowired
	private DealerSpecialOfferService dealerSpecialOfferService;
	
	@Autowired
	private DealerServiceRepairService dealerServiceRepairService;
	
	@Autowired 
	private DealerOverviewService dealerOverviewService;
	
	@Autowired
	private FeatureFlag featureFlag;
	
	@Autowired
	private DealerReviewViewValidator dealerReviewViewValidator;
	
	@Autowired
	private DealerResponseViewValidator dealerResponseViewValidator;
	
	
	private static final Integer ZIPCODE_LENGTH = 5;
	
	@RequestMapping(value = "greetings", method = RequestMethod.GET)
	public String getGreetings(){

		
		return "Hi I am the new composite service: "+ testName;
	}
	
	@RequestMapping(value="dealer/search/sales", method = RequestMethod.GET,produces = "application/json") 
	public DealerSearchResults dealersSearchSales(@RequestParam(value="zc", required=true) String zip, 
								@RequestParam(value="mkId", required=false) String[] mkId,
								@RequestParam(value="rd", required=false, defaultValue="30") String radius, 
								@RequestParam(value="sortBy", required=false, defaultValue="DISTANCE") String sortyBy,
								@RequestParam(value="order", required=false, defaultValue="DSC") String sortOrder,
								@RequestParam(value="page", required=false, defaultValue="1") String startPage,
								@RequestParam(value="perPage", required=false, defaultValue="50")String numResult,
								@RequestParam(value="keyword", required=false) String keyword,
								@RequestParam(value="dlrRwRtg", required=false, defaultValue="")String ratingId,
								@RequestParam(value="dealerType", required=false, defaultValue="") String dealerTypeId) {
		DealerSearchResults dealerSearchResults;
		
		if(!validateZip(zip)){
			dealerSearchResults = new DealerSearchResults(); 
			//dealers.setReturnCode(buildReturnCode(Constants.INVALID_ZIP_CODE, Constants.INVALID_ZIP_MESSAGE));
			return dealerSearchResults; 
		}
		
		dealerSearchResults = customerSearchService.searchSalesDealers(zip, mkId, radius, sortyBy, sortOrder, startPage, numResult, keyword,ratingId, dealerTypeId); 
		
		return dealerSearchResults; 
	}
	
	@RequestMapping(value="dealer/search/service", method = RequestMethod.GET,produces = "application/json") 
	public DealerSearchResults dealersSearchService(@RequestParam(value="zc", required=true) String zip, 
								@RequestParam(value="mkId", required=false) String[] makeId,
								@RequestParam(value="rd", required=false, defaultValue="30") String radius, 
								@RequestParam(value="sortBy", required=false, defaultValue="DISTANCE") String sortyBy,
								@RequestParam(value="order", required=false, defaultValue="DSC") String sortOrder,
								@RequestParam(value="page", required=false, defaultValue="1") String startPage,
								@RequestParam(value="perPage", required=false, defaultValue="50")String numResult,
								@RequestParam(value="keyword", required=false) String keyword,
								@RequestParam(value="dlrRwRtg", required=false, defaultValue="")String ratingId,
								@RequestParam(value="certified", required=false, defaultValue="") String certifiedId,
								@RequestParam(value="dealerType", required=false, defaultValue="") String dealerTypeId) {
		DealerSearchResults dealerSearchResults = null;
		
		if(!validateZip(zip)){
			dealerSearchResults = new DealerSearchResults(); 
			//dealers.setReturnCode(buildReturnCode(Constants.INVALID_ZIP_CODE, Constants.INVALID_ZIP_MESSAGE));
			return dealerSearchResults; 
		}
		
		dealerSearchResults = customerSearchService.searchServiceDealers(zip, makeId, radius, sortyBy, sortOrder, startPage, numResult, keyword, ratingId,certifiedId,dealerTypeId); 
		

		return dealerSearchResults; 
	}
	
	/*@RequestMapping(value="search/{customerId}", method = RequestMethod.GET,produces = "application/json") 
	public DealerDetails getDealerDetails(@PathVariable("customerId") String customerId) {
		DealerDetails dealerDetails;
		
		if(invalidNumber(customerId)){
			dealerDetails = new DealerDetails(); 
			return dealerDetails; 
		}
		
		dealerDetails = customerSearchService.getDealerDetails(customerId);
		

		return dealerDetails; 
	}*/
	
	@RequestMapping(value="dealer/{customerId}/overview", method = RequestMethod.GET, produces="application/json")
	public DealerOverviewDetails getDealerOverviewDetails(@PathVariable("customerId") String customerId){
		
		
		DealerOverviewDetails dealerOverviewDetails = dealerOverviewService.getDealerOverviewDetails(customerId);
		
		return dealerOverviewDetails;
	}
	
	@RequestMapping(value="dealer/{customerId}/reviews", method = RequestMethod.GET, produces="application/json")
	public DealerReviewDetails getDealerReviewDetails(@PathVariable("customerId") String customerId,
														@RequestParam(value = "start", required=false, defaultValue="0") String start,
														@RequestParam(value = "count", required=false, defaultValue="10") String count,
														@RequestParam(value = "sortBy", required=false, defaultValue="featured-most-recent") String sortBy,
														@RequestParam(value = "filterBy", required=false, defaultValue="all-results") String filterBy){
		
		
		DealerReviewDetails dealerReviewDetails = dealerReviewService.getDealerReviewDetails(customerId, start, count, sortBy, filterBy);

		
		return dealerReviewDetails;
	}
	
	@RequestMapping(value="dealer/{customerId}/specialoffer", method = RequestMethod.GET, produces="application/json")
	public DealerSpecialOfferDetails getDealerSpecialOffersDetails(@PathVariable("customerId") String customerId){
		
		

		return dealerSpecialOfferService.getLocalOffersByPartyId(customerId);
		
	}
	
	@RequestMapping(value="dealer/{customerId}/servicerepair", method = RequestMethod.GET, produces="application/json")
	public DealerServiceRepairDetails getServiceRepairDetails(@PathVariable("customerId") String customerId){
		
		

		return dealerServiceRepairService.getServiceRepairDetails(customerId);
		
	}
	
	/*@RequestMapping(value="dealer/{customerId}/reviews", method = RequestMethod.POST, produces="application/json")
	public Boolean saveDealerReview(@PathVariable("customerId") String customerId,
									@RequestParam(value = "title", required = false) String reviewTitle, 
									@RequestParam(value = "review", required = false) String reviewText,
									@RequestParam(value = "recommend", required = false) boolean recommend,
									@RequestParam(value = "purchase", required = false) boolean purchase,
									@RequestParam(value = "newCar", required = false) boolean visitNewCar,
									@RequestParam(value = "usedCar", required = false) boolean visitServiceCar,
									@RequestParam(value = "serviceCar", required = false) boolean visitUsedCar,
									@RequestParam(value = "reviewerName", required = false) String reviewerName,
									@RequestParam(value = "reviewerLoc", required = false) String reviewerLoc,
									@RequestParam(value = "reviewerEmail", required = false) String reviewerEmail,
									@RequestParam(value = "promoCode", required = false) String promoCode,
									@RequestParam(value = "isDealer", required = false) boolean isDealer,
									@RequestParam(value = "overallRating", required = false) String overallRating,
									@RequestParam(value = "customerRating", required = false) String customerRating,
									@RequestParam(value = "na-customerService", required = false) boolean noCustomerServiceRating,
									@RequestParam(value = "buyingRating", required = false) String buyingRating,
									@RequestParam(value = "na-buyingProcess", required = false) boolean noBuyingRating,
									@RequestParam(value = "repairRating", required = false) String repairRating,
									@RequestParam(value = "na-qualityOfRepairs", required = false) boolean noRepairsRating,
									@RequestParam(value = "facilityRating", required = false) String facilityRating,
									@RequestParam(value = "na-facilities", required = false) boolean noFacilitiesRating){
	
		String ipAddress = "127.0.0.1";
	
		DealerReviewView dealerReviewView = new DealerReviewView(reviewTitle, reviewText, recommend, purchase, visitNewCar, visitServiceCar, visitUsedCar, 
				isDealer, noCustomerServiceRating, noBuyingRating, noRepairsRating,noFacilitiesRating, reviewerName,reviewerLoc,promoCode, overallRating, 
				customerRating, buyingRating, repairRating, facilityRating, reviewerEmail, customerId,ipAddress);
		Boolean status = dealerReviewService.saveDealerReview(dealerReviewView);

		
		return status;
	}*/
	@RequestMapping(value="dealer/{customerId}/reviews", method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<StringBuffer> saveDealerReview(@PathVariable("customerId") String customerId,
									@RequestBody DealerReviewView dealerReviewView){
	
		
		
		BeanPropertyBindingResult bindingResults = new BeanPropertyBindingResult(dealerReviewView, "Errors");
		dealerReviewViewValidator.validate(dealerReviewView, bindingResults);

		if(bindingResults.hasErrors()){
			StringBuffer message = new StringBuffer();
			List<ObjectError> errors =  bindingResults.getGlobalErrors();
			for (ObjectError error : errors) {
				if (message.length() > 0) {
					message.append(";");
				}
				message.append(error.getCode());
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);

		} else{
			Boolean status =  dealerReviewService.saveDealerReview(dealerReviewView);
			
			if(status){
				return ResponseEntity.status(HttpStatus.OK).body( new StringBuffer("SAVE_SUCCESSFUL"));
			} else{
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new StringBuffer("SAVE_UNSUCCESSFUL"));
			}
		}

	}
	
	@RequestMapping(value = {"/dealer/{customerId}/review/{reviewId}/feedback"}, method = RequestMethod.PUT)
	@ResponseBody
	public Boolean saveFeedback(@PathVariable("customerId") String customerId,
							 @PathVariable("reviewId") String reviewId, 
							 @RequestBody boolean helpful,
							 @RequestBody boolean isAbuse){
		
		Boolean status = dealerReviewService.saveReviewFeedback(customerId, reviewId, helpful, isAbuse);
		
		return status;
	}
	
	
	@RequestMapping(value = {"/dealer/{customerId}/review/{reviewId}/response"}, method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<StringBuffer> saveResponse(@PathVariable("customerId") String customerId,
			 					@PathVariable("reviewId") String reviewId,
			 					@RequestBody DealerResponseView dealerResponseView){
		
		
		BeanPropertyBindingResult bindingResults = new BeanPropertyBindingResult(dealerResponseView, "Errors");
		dealerReviewViewValidator.validate(dealerResponseView, bindingResults);

		if(bindingResults.hasErrors()){
			StringBuffer message = new StringBuffer();
			List<ObjectError> errors =  bindingResults.getGlobalErrors();
			for (ObjectError error : errors) {
				if (message.length() > 0) {
					message.append(";");
				}
				message.append(error.getCode());
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);

		} else{
			Boolean status =  dealerReviewService.saveReviewResponse(customerId, reviewId,dealerResponseView);
			
			if(status){
				return ResponseEntity.status(HttpStatus.OK).body( new StringBuffer("SAVE_SUCCESSFUL"));
			} else{
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new StringBuffer("SAVE_UNSUCCESSFUL"));
			}
		}
		
	}
	
	
	private boolean validateZip(String zip){
		return (zip.length() == ZIPCODE_LENGTH && NumberUtils.isNumber(zip)); 
	}

	public CustomerSearchService getCustomerSearchService() {
		return customerSearchService;
	}

	public void setCustomerSearchService(CustomerSearchService customerSearchService) {
		this.customerSearchService = customerSearchService;
	}

	public DealerReviewService getDealerReviewService() {
		return dealerReviewService;
	}

	public void setDealerReviewService(DealerReviewService dealerReviewService) {
		this.dealerReviewService = dealerReviewService;
	}

	public DealerSpecialOfferService getDealerSpecialOfferService() {
		return dealerSpecialOfferService;
	}

	public void setDealerSpecialOfferService(DealerSpecialOfferService dealerSpecialOfferService) {
		this.dealerSpecialOfferService = dealerSpecialOfferService;
	}

	public DealerServiceRepairService getDealerServiceRepairService() {
		return dealerServiceRepairService;
	}

	public void setDealerServiceRepairService(DealerServiceRepairService dealerServiceRepairService) {
		this.dealerServiceRepairService = dealerServiceRepairService;
	}

	public DealerOverviewService getDealerOverviewService() {
		return dealerOverviewService;
	}

	public void setDealerOverviewService(DealerOverviewService dealerOverviewService) {
		this.dealerOverviewService = dealerOverviewService;
	}

	public FeatureFlag getFeatureFlag() {
		return featureFlag;
	}

	public void setFeatureFlag(FeatureFlag featureFlag) {
		this.featureFlag = featureFlag;
	}

	public DealerReviewViewValidator getDealerReviewViewValidator() {
		return dealerReviewViewValidator;
	}

	public void setDealerReviewViewValidator(DealerReviewViewValidator dealerReviewViewValidator) {
		this.dealerReviewViewValidator = dealerReviewViewValidator;
	}

	public DealerResponseViewValidator getDealerResponseViewValidator() {
		return dealerResponseViewValidator;
	}

	public void setDealerResponseViewValidator(DealerResponseViewValidator dealerResponseViewValidator) {
		this.dealerResponseViewValidator = dealerResponseViewValidator;
	}


	
	
}
