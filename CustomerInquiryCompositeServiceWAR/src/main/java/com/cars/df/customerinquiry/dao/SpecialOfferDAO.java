package com.cars.df.customerinquiry.dao;

import com.cars.df.customerinquiry.client.ServiceClients;
import com.cars.ss.lois.bo.generated.LocalOffers;

public class SpecialOfferDAO {

	private ServiceClients serviceClients;
	
	
	public String getOfferAndModelCount(String partyId){
		
		return serviceClients.getOfferAndModelCount(partyId);
	}
	
	public LocalOffers getLocalOffers(String partyId){
		return serviceClients.getLocalOffersByDealerId(partyId);
	}


	public ServiceClients getServiceClients() {
		return serviceClients;
	}


	public void setServiceClients(ServiceClients serviceClients) {
		this.serviceClients = serviceClients;
	}
	
	
}
