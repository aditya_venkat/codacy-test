package com.cars.df.customerinquiry.dao;


import com.cars.DRMS.client.DRMSClient;
import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.api.schemas.dealer_review.ReturnCode;

public class DealerReviewDAO {
	
	
	private DRMSClient drmsClient;
	
	
	public DealerReview getReviews(String dealerId, Integer start, Integer count, String sortBy, String filterBy){
		
		
		DealerReview dealerReview = drmsClient.getWiredReviewsWithPagination(dealerId, start, count, "featured-most-recent", "all-results");
		
		return dealerReview;
	}
	
	public ReturnCode saveDealerReview(DealerReview dealerReview){
				
		ReturnCode returnCode = drmsClient.saveReview(dealerReview);
		return returnCode;
	}
	
	public ReturnCode saveFeedback(DealerReview dealerReview){
		
		ReturnCode returnCode = drmsClient.saveFeedback(dealerReview);
		
		return returnCode;
	}
	
	public ReturnCode saveReviewResponse(DealerReview dealerReview){
		
		ReturnCode returnCode = drmsClient.saveDealerResponse(dealerReview);
		return returnCode;
	}

	public DRMSClient getDrmsClient() {
		return drmsClient;
	}

	public void setDrmsClient(DRMSClient drmsClient) {
		this.drmsClient = drmsClient;
	}
	
	
	

}
