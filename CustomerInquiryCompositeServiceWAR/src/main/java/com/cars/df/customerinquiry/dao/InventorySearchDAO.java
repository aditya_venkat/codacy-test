package com.cars.df.customerinquiry.dao;

import java.util.ArrayList;
import java.util.List;

import com.cars.df.customerinquiry.utility.CICSUtility;
import com.cars.service.listingsearch.bo.SearchCriteriaBO;
import com.cars.service.listingsearch.bo.SearchResultBO;
import com.cars.ss.iss.client.InventorySearchServiceClient;

public class InventorySearchDAO {
	
	private InventorySearchServiceClient inventorySearchServiceClient;
	
	private static final Integer MAX_RADIUS = 10000;
	private static final Integer ZERO = 0;
	
	public SearchResultBO getInventoryDetails(String partyId, String zipCode){
		
		SearchCriteriaBO searchCriteriaBO = new SearchCriteriaBO();
		List<Long> dlId = new ArrayList<Long>();
		dlId.add(CICSUtility.convertStringToLong(partyId));
		searchCriteriaBO.setDlId(dlId);
		searchCriteriaBO.setZc(zipCode);
		searchCriteriaBO.setRd(MAX_RADIUS);
		searchCriteriaBO.setRpp(ZERO);
		searchCriteriaBO.setRn(ZERO);
		
		SearchResultBO inventoryResults = inventorySearchServiceClient.search(searchCriteriaBO);

		
		return inventoryResults;

	}


	public InventorySearchServiceClient getInventorySearchServiceClient() {
		return inventorySearchServiceClient;
	}


	public void setInventorySearchServiceClient(InventorySearchServiceClient inventorySearchServiceClient) {
		this.inventorySearchServiceClient = inventorySearchServiceClient;
	}
	

}
