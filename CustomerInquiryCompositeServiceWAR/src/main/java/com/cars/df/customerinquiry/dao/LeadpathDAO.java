/**
 * 
 */
package com.cars.df.customerinquiry.dao;

import com.cars.api.schemas.lead.NonInventoryLead;
import com.cars.df.customerinquiry.client.ServiceClients;
import com.cars.rs.client.api.ServiceCustomer;

/**
 * @author leonard
 *
 */
public class LeadpathDAO {
	private ServiceClients serviceClients;

	public Boolean sendDealerEmailLead(NonInventoryLead lead) {

		return serviceClients.sendDealerEmailLead(lead);

	}

	public ServiceClients getServiceClients() {
		return serviceClients;
	}

	public void setServiceClients(ServiceClients serviceClients) {
		this.serviceClients = serviceClients;
	}
}
