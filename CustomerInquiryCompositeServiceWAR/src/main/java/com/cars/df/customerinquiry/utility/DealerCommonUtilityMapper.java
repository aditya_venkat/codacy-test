package com.cars.df.customerinquiry.utility;

import com.cars.df.css.bo.Dealer;
import com.cars.df.customerinquiry.bo.DealerDetails;

public class DealerCommonUtilityMapper {

	public void createDealerDetailSummary(Dealer dealer, DealerDetails dealerDetails){
		dealerDetails.setName(dealer.getName());
		dealerDetails.setAddressLine1(dealer.getAddressLine1());
		dealerDetails.setAddressLine2(dealer.getAddressLine2());
		dealerDetails.setCity(dealer.getCity());
		dealerDetails.setState(dealer.getState());
		dealerDetails.setZip(dealer.getZip());
		dealerDetails.setRating(dealer.getRating());
		dealerDetails.setTotalReviewsNum(dealer.getTotalReviewsNum());
		dealerDetails.setPartyId(dealer.getPartyId());
		dealerDetails.setCustomerId(dealer.getCustomerId());
		dealerDetails.setPromoTagLine(dealer.getPromoTagLine());
		
		dealerDetails.setNewCarEmail(dealer.getNewCarEmail());
		dealerDetails.setUsedCarEmail(dealer.getUsedCarEmail());
		dealerDetails.setNewCarTollFreePhoneNumber(CICSUtility.formatPhoneNumber(dealer.getNewCarTollFreePhoneNumber()));
		dealerDetails.setUsedCarTollFreePhoneNumber(CICSUtility.formatPhoneNumber(dealer.getUsedCarTollFreePhoneNumber()));
		dealerDetails.setServiceTollFreePhoneNumber(CICSUtility.formatPhoneNumber(dealer.getServiceTollFreePhoneNumber()));
				
	}
}
