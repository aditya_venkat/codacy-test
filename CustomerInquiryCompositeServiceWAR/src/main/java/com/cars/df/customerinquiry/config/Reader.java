package com.cars.df.customerinquiry.config;

import java.io.IOException;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

import com.cars.framework.config.impl.AbstractResourceConfigReader;
import com.cars.ss.seo.client.util.RestClientFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Reader extends AbstractResourceConfigReader{


    @Override
    protected Map<String, Object> doRead(String resourceUrl) {

        Map<String, Object> obj = null;

        if( StringUtils.isEmpty(resourceUrl) ) {
            return obj;
        }

        try {

            RestClient restClient = RestClientFactory.getRestClientWithJsonProvider();
            Resource resource = restClient.resource(resourceUrl);
            String message = resource.accept(MediaType.APPLICATION_JSON_TYPE).get(String.class);
            obj = new ObjectMapper().readValue(message, Map.class);

        } catch (IOException e) {
            throw new RuntimeException(e);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return obj;
    }
}
