package com.cars.df.customerinquiry.service;

import java.util.List;

import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerSpecialOfferDetails;
import com.cars.df.customerinquiry.bo.YearMakeModelOffer;
import com.cars.df.customerinquiry.dao.CustomerSearchDAO;
import com.cars.df.customerinquiry.dao.SpecialOfferDAO;
import com.cars.df.customerinquiry.utility.DealerCommonUtilityMapper;
import com.cars.df.customerinquiry.utility.DealerLocalOfferUtilityMapper;
import com.cars.ss.lois.bo.generated.LocalOffers;

public class DealerSpecialOfferService {

	
	private SpecialOfferDAO specialOfferDAO;
	
	private CustomerSearchDAO customerSearchDAO;
		
	private DealerLocalOfferUtilityMapper dealerLocalOfferUtilityMapper;
	
	private DealerCommonUtilityMapper dealerCommonUtilityMapper;

	private static final Integer SINGLE_DEALER = 1;

	
	public DealerSpecialOfferDetails getLocalOffersByPartyId(String customerId){
		
		String partyId = customerSearchDAO.convertCustomerIdToPartyId(customerId);
		
		Dealers dealers =  customerSearchDAO.getDealer(partyId); 
		
		LocalOffers localOffers = specialOfferDAO.getLocalOffers(partyId);
		
		DealerSpecialOfferDetails dealerSpecialOfferDetails = dealerLocalOfferUtilityMapper.mapLocalOfferBOToYearMakeModeOfferBO(dealers,localOffers);	
		
		return dealerSpecialOfferDetails;
	}


	public SpecialOfferDAO getSpecialOfferDAO() {
		return specialOfferDAO;
	}


	public void setSpecialOfferDAO(SpecialOfferDAO specialOfferDAO) {
		this.specialOfferDAO = specialOfferDAO;
	}


	public DealerLocalOfferUtilityMapper getDealerLocalOfferUtilityMapper() {
		return dealerLocalOfferUtilityMapper;
	}


	public void setDealerLocalOfferUtilityMapper(DealerLocalOfferUtilityMapper dealerLocalOfferUtilityMapper) {
		this.dealerLocalOfferUtilityMapper = dealerLocalOfferUtilityMapper;
	}


	public CustomerSearchDAO getCustomerSearchDAO() {
		return customerSearchDAO;
	}


	public void setCustomerSearchDAO(CustomerSearchDAO customerSearchDAO) {
		this.customerSearchDAO = customerSearchDAO;
	}


	public DealerCommonUtilityMapper getDealerCommonUtilityMapper() {
		return dealerCommonUtilityMapper;
	}


	public void setDealerCommonUtilityMapper(DealerCommonUtilityMapper dealerCommonUtilityMapper) {
		this.dealerCommonUtilityMapper = dealerCommonUtilityMapper;
	}


}
