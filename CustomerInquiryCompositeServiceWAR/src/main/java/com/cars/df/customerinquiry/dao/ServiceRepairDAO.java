package com.cars.df.customerinquiry.dao;

import com.cars.df.customerinquiry.client.ServiceClients;
import com.cars.rs.client.api.ServiceCustomer;

public class ServiceRepairDAO {

	private ServiceClients serviceClients;
	
	public ServiceCustomer getServiceRepairDetails(String customerId){
		
		return serviceClients.getServiceRepairDetails(customerId);
	}
	

	public ServiceClients getServiceClients() {
		return serviceClients;
	}

	public void setServiceClients(ServiceClients serviceClients) {
		this.serviceClients = serviceClients;
	}
	
}
