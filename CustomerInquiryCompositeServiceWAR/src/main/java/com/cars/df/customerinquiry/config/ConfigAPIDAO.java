package com.cars.df.customerinquiry.config;

import org.apache.wink.client.RestClient;

public class ConfigAPIDAO {

	//TOOD: Add config service and assign it to feature flag. 
	//We need to do this for feature flagging.
	private RestClient client;
	
	private String configapiHost;
	private String configapiContext;
	
	
	public void getFlag(){
		
	}


	public RestClient getClient() {
		return client;
	}


	public void setClient(RestClient client) {
		this.client = client;
	}


	public String getConfigapiHost() {
		return configapiHost;
	}


	public void setConfigapiHost(String configapiHost) {
		this.configapiHost = configapiHost;
	}


	public String getConfigapiContext() {
		return configapiContext;
	}


	public void setConfigapiContext(String configapiContext) {
		this.configapiContext = configapiContext;
	}
}
