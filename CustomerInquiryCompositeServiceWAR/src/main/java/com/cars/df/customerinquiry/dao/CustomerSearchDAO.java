package com.cars.df.customerinquiry.dao;

import com.cars.df.css.bo.DealerSearchCriteriaBO;
import com.cars.df.css.bo.Dealers;
import com.cars.df.css.client.CustomerSearchServiceClient;


public class CustomerSearchDAO {

	private CustomerSearchServiceClient cssClient; 
	
	
	public Dealers searchDealers(DealerSearchCriteriaBO criteria) {	
		
		return cssClient.getDealers(criteria);
	}
	
	public Dealers getDealer(String partyId) {	

		return cssClient.getDealer(partyId);
		
	}
	
	public String convertCustomerIdToPartyId(String customerId){
		
		return cssClient.convertLegacyDealerIdToOdsId(customerId);
	
	}
	
	public String convertPartyIdToCustomerId(String partyId){
		
		return cssClient.convertOdsIdToLegacyDealerId(partyId)	;
		
	}

	public CustomerSearchServiceClient getCssClient() {
		return cssClient;
	}

	public void setCssClient(CustomerSearchServiceClient cssClient) {
		this.cssClient = cssClient;
	}
}
