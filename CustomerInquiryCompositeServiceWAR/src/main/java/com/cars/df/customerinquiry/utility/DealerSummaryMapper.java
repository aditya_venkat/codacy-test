package com.cars.df.customerinquiry.utility;


import com.cars.df.css.bo.BreadCrumb;
import com.cars.df.css.bo.BreadCrumbValue;
import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.css.bo.Refinement;
import com.cars.df.css.bo.RefinementValue;
import com.cars.df.customerinquiry.bo.DealerSearchResults;
import com.cars.df.customerinquiry.bo.DealerSummary;
import com.cars.df.customerinquiry.bo.DirectoryBreadCrumb;
import com.cars.df.customerinquiry.bo.DirectoryBreadCrumbValue;
import com.cars.df.customerinquiry.bo.DirectoryRefinement;
import com.cars.df.customerinquiry.bo.DirectoryRefinementValue;


public class DealerSummaryMapper {

	
	public DealerSearchResults mapDealerToDealerSummary(Dealers dealers){
		
		DealerSearchResults dealerSearchResults = new DealerSearchResults();
		if(dealers != null && dealers.getDealers() != null && dealers.getDealers().size() > 0){
			
			for(Dealer dealer: dealers.getDealers()){
				DealerSummary dealerSummary = new DealerSummary();
				
				dealerSummary.setName(dealer.getName());
				dealerSummary.setAddressLine1(dealer.getAddressLine1());
				dealerSummary.setAddressLine2(dealer.getAddressLine2());
				dealerSummary.setCity(dealer.getCity());
				dealerSummary.setState(dealer.getState());
				dealerSummary.setZip(dealer.getZip());
				dealerSummary.setTotalReviewsNum(dealer.getTotalReviewsNum());
				dealerSummary.setRating(dealer.getRating());
				dealerSummary.setCustomerId(dealer.getCustomerId());
				dealerSummary.setPartyId(dealer.getPartyId());
				dealerSummary.setDealerType(dealer.getDealerType());
				dealerSummary.setRpcCertified(dealer.getRpcCertified());
				dealerSummary.setLogoUrl(dealer.getLogoUrl());
				dealerSummary.setPromoTagLine(dealer.getPromoTagLine());
				dealerSummary.setNewCarTollFreePhoneNumber(dealer.getNewCarTollFreePhoneNumber());
				dealerSummary.setUsedCarTollFreePhoneNumber(dealer.getUsedCarTollFreePhoneNumber());
				dealerSummary.setServiceTollFreePhoneNumber(dealer.getServiceTollFreePhoneNumber());
				dealerSummary.setLatitude(dealer.getLatitude());
				dealerSummary.setLongitude(dealer.getLongitude());
				dealerSummary.setDistance(dealer.getDistance());
				
				
				dealerSearchResults.addDealerSummary(dealerSummary);
				
				
			}
			
			if(dealers.getRefinements() != null){
				for(Refinement refinement: dealers.getRefinements()){
					
					DirectoryRefinement directoryRefinement = new DirectoryRefinement();
					
					directoryRefinement.setId(refinement.getId());
					directoryRefinement.setName(refinement.getName());
					
					for(RefinementValue refinementValue: refinement.getRefinementValues()){
						DirectoryRefinementValue directoryRefinementValue = new DirectoryRefinementValue();
						directoryRefinementValue.setId(refinementValue.getId());
						directoryRefinementValue.setName(refinementValue.getName());
						
						directoryRefinement.addRefinementValue(directoryRefinementValue);
						
					}
					dealerSearchResults.addRefinement(directoryRefinement);
					
				}
			}
			
			if(dealers.getBreadCrumbs() != null){
				for(BreadCrumb breadCrumb: dealers.getBreadCrumbs()){
					
					DirectoryBreadCrumb directoryBreadCrumb = new DirectoryBreadCrumb();
					
					directoryBreadCrumb.setId(breadCrumb.getId());
					directoryBreadCrumb.setName(breadCrumb.getName());
					
					for(BreadCrumbValue breadCrumbValue: breadCrumb.getBreadCrumbValues()){
						DirectoryBreadCrumbValue directoryBreadCrumbValue = new DirectoryBreadCrumbValue();
						directoryBreadCrumbValue.setId(breadCrumbValue.getId());
						directoryBreadCrumbValue.setName(breadCrumbValue.getName());
						
						directoryBreadCrumb.addBreadCrumbValue(directoryBreadCrumbValue);
						
					}
					dealerSearchResults.addBreadCrumb(directoryBreadCrumb);
					
				}
			}
			
			dealerSearchResults.setTotalDealers(dealers.getTotalDealers());
		}
		return dealerSearchResults;
		
	}
	
	
}
