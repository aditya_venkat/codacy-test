package com.cars.df.customerinquiry.config;

import java.io.StringWriter;

import com.cars.framework.config.ConfigJsonWriter;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Writer implements ConfigJsonWriter {

	 private ObjectMapper mapper = new ObjectMapper();

	    @Override
	    public String write(Object o) {

	        StringWriter sw = new StringWriter();
	        try {
	            mapper.writeValue(sw, o);
	        } catch (Exception e) {
	            throw new RuntimeException(e);
	        }
	        return sw.toString();
	    }
}
