/**
 * 
 */
package com.cars.df.customerinquiry.service;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.jvnet.jaxb2_commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.cars.api.schemas.lead.AddressType;
import com.cars.api.schemas.lead.DealerSeller;
import com.cars.api.schemas.lead.ExtType;
import com.cars.api.schemas.lead.ExtensionItem;
import com.cars.api.schemas.lead.HeaderItem;
import com.cars.api.schemas.lead.LeadEvent;
import com.cars.api.schemas.lead.NameType;
import com.cars.api.schemas.lead.NonInventoryLead;
import com.cars.api.schemas.lead.LeadEvent.Consumer;
import com.cars.api.schemas.lead.NonInventoryLead.Seller;
import com.cars.api.schemas.lead.PhoneType;
import com.cars.df.customerinquiry.bo.DealerEmailLead;
import com.cars.df.customerinquiry.dao.LeadpathDAO;

/**
 * @author lgiovenco
 *
 */
public class LeadpathService {
	
	LeadpathDAO leadpathDAO; 
	Logger logger = LoggerFactory.getLogger(LeadpathService.class);

	public Boolean sendDealerEmail(DealerEmailLead emailForm, HttpServletRequest request, 
			String affiliate, String customerId) {
		
		//Convert here
		NonInventoryLead nonInventoryLead = convertDealerEmailToJaxb(emailForm, affiliate, request, customerId);
		//Send to DAO here
		return leadpathDAO.sendDealerEmailLead(nonInventoryLead);
	}
	
	public NonInventoryLead convertDealerEmailToJaxb(DealerEmailLead emailForm, String affiliate, 
			HttpServletRequest request, String customerId) {
			
			NonInventoryLead nonInvLead = new NonInventoryLead();
			LeadEvent leadEvent = new LeadEvent();
			
			// determine if this is a new or used lead
			String leadType = emailForm.getIsNew().equals("true") ? "N" : "U";
			
			ExtensionItem item = new ExtensionItem();
			item.setKey("stockType");
			item.setValue(leadType);

			ExtType extType = new ExtType();
			nonInvLead.setExt(extType);
			nonInvLead.getExt().getExtensionItem().add(item);

			leadEvent
					.setLeadEventDeliveryStatusCode(new com.cars.api.schemas.lead.LeadEvent.LeadEventDeliveryStatusCode());
			leadEvent.getLeadEventDeliveryStatusCode()
					.setLeadConfirmationEmailSent(false);
			leadEvent.getLeadEventDeliveryStatusCode()
					.setLeadEventDeliveryCodeDescription(
							new com.cars.api.schemas.lead.CodeDescription());
			leadEvent.getLeadEventDeliveryStatusCode()
					.getLeadEventDeliveryCodeDescription().setTypeCode("3");
			leadEvent.getLeadEventDeliveryStatusCode()
					.getLeadEventDeliveryCodeDescription()
					.setTypeDescription("Send");

			if(leadType.equals("N")) {
				leadEvent.setLeadTypeCode("N_INV_NEW");
			}
			else {
				leadEvent.setLeadTypeCode("N_INV_USED");
			}
			
			leadEvent.setAffiliateProfileName(affiliate);
			leadEvent.setInformationPartnerRolePartyIdentifier(new BigInteger("0"));
			leadEvent.setInformationPartnerRoleOrgName("cars.com");

			GregorianCalendar c = new GregorianCalendar();
			c.setTime(Calendar.getInstance().getTime());
			try {
				XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance()
						.newXMLGregorianCalendar(c);
				leadEvent.setLeadEventRequestDate(xmlDate);
			} catch (DatatypeConfigurationException e) {
				logger.error(e.getMessage());
			}

			NameType name = new NameType();
			name.setPersonFirstName(emailForm.getFirstName());
			name.setPersonLastName(emailForm.getLastName());
			leadEvent
					.setConsumer(new com.cars.api.schemas.lead.LeadEvent.Consumer());
			leadEvent.getConsumer().setComment(emailForm.getComments());
			leadEvent.getConsumer().setName(name);
			leadEvent.getConsumer().setEmailAddress(emailForm.getEmail());
			
			if(!StringUtils.isEmpty(emailForm.getJscCollector())) {
				leadEvent.getConsumer().setJscDataCollector(emailForm.getJscCollector());
			}
			addHttpHeaders(leadEvent.getConsumer(), request);
			addConsumerIpAddress(leadEvent.getConsumer(), request);

			AddressType addressType = new AddressType();
			addressType.setPostalCode(emailForm.getZipCode());
			leadEvent.getConsumer().setStreetAddress(addressType);

			PhoneType phone = new PhoneType();
			if (emailForm.getDaytimeNumber() != null) {
				phone.setNumber(emailForm.getDaytimeNumber());
				phone.setContactMethodType("PHNDAY");
				leadEvent.getConsumer().getPhone().add(phone);
			}

			PhoneType ephone = new PhoneType();
			if (emailForm.getEveningNumber() != null) {
				ephone.setNumber(emailForm.getEveningNumber());
				ephone.setContactMethodType("PHNEVE");
				leadEvent.getConsumer().getPhone().add(ephone);
			}

			nonInvLead.setEvent(leadEvent);

			Seller seller = new Seller();
			DealerSeller dealerSeller = new DealerSeller();
			dealerSeller.setSellerRolePartyIdentifier(new BigInteger(String
					.valueOf(emailForm.getOdsId())));

			NameType dealerName = new NameType();
			dealerName.setPersonFirstName(emailForm.getDealerName());
			dealerSeller.setSellerName(dealerName);

			AddressType dealerAddress = new AddressType();
			dealerAddress.setPostalCode(emailForm.getZipCode());
			dealerSeller.setStreetAddress(dealerAddress);
			dealerSeller.setContactEmail(emailForm.getDealerEmail());
			dealerSeller.setCustomerIntId(new BigInteger(customerId));

			PhoneType dealerPhone = new PhoneType();
			dealerPhone.setNumber(emailForm.getDealerNumber());
			dealerPhone.setContactMethodType("PHNDAY");

			dealerSeller.setPrimaryPhone(dealerPhone);

			seller.getDealerSeller().add(dealerSeller);
			nonInvLead.setSeller(seller);

			leadEvent.setLeadEventIdentifier(new BigInteger("0"));
			
			return nonInvLead;

	}
	
private Consumer addHttpHeaders(Consumer consumer, HttpServletRequest request) {
		
		//Put all the headers in from the HTTP Request
		Enumeration<String> enumn = request.getHeaderNames();

		while (enumn.hasMoreElements()) {
			String header = enumn.nextElement();
			String headerVal = request.getHeader(header);
			HeaderItem headerItem = new HeaderItem();
			headerItem.setHeaderType("httpHeader");
			headerItem.setName(header);
			headerItem.setValue(headerVal);
			consumer.getHttpHeaders().add(headerItem);
			logger.debug("Header Name=>" + header + ":Header value==>"
					+ headerVal);
		}
		
		return consumer; 
	}

	private Consumer addConsumerIpAddress(Consumer consumer, HttpServletRequest request) {
		String ipStr = ""; 
		try {
			ipStr = request.getHeader("X-FORWARDED-FOR");
	         if (ipStr == null || ipStr.length() < 1) {
	        	 ipStr = request.getRemoteAddr();
	         }
	       } catch (Exception e) {
	    	   	logger.error(e.getMessage());
	            return consumer;
	       }
		consumer.setConsumerIpAddress(ipStr);
		return consumer; 
	}
	
	public LeadpathDAO getLeadpathDAO() {
		return leadpathDAO;
	}

	public void setLeadpathDAO(LeadpathDAO leadpathDAO) {
		this.leadpathDAO = leadpathDAO;
	}
	
}
