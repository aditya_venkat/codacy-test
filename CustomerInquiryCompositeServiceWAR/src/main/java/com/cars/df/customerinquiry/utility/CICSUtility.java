package com.cars.df.customerinquiry.utility;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.validation.Errors;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class CICSUtility {
	
	private static final Integer ZIPCODE_LENGTH = 5;

	public static String formatPhoneNumber(String phoneNumber) {

		if (isLong(phoneNumber) && phoneNumber.length() == 10) {
			
			phoneNumber = String.format("%s-%s-%s", phoneNumber.substring(0, 3), 
						  				phoneNumber.substring(3, 6), 
						  				phoneNumber.substring(6, 10));
		}

		return phoneNumber;
	}
	
	public static boolean isLong(String value) {
		boolean isLong = true;

		try {
			Long.valueOf(value);
		} catch (Exception e) {
			isLong = false;
		}

		return isLong;
	}
	
	public static Double convertStringToDouble(String doubleString){
		
		Double doubleValue = 0.0;
				
		try{
			doubleValue = Double.valueOf(doubleString);
		} catch(Exception e){
			e.printStackTrace();
		}
		
		return doubleValue;
	}
	
	public static String convertDoubleToSingleDecimal(String overallRating){
		
		if(!StringUtils.isEmpty(overallRating) && isDouble(overallRating)){
			overallRating = String.format("%.1f", Double.valueOf(overallRating));
		}
		return overallRating;
		
	}
	
	public static boolean isDouble(String overallRating){
		boolean isDouble = true;
		
		try{
			
			Double.valueOf(overallRating);
			
		} catch(Exception e){
			isDouble = false;
		}
		
		return isDouble;
	}
	
	public static String[] convertPhotoStringToURL(String photoURLString){
		
		String [] photos = null;
		if(StringUtils.isNotBlank(photoURLString)){
			
			String photoURLStringWithHost  = Constants.STATIC_IMAGES_HOST +  photoURLString;
			photos = photoURLStringWithHost.replace("@", "@"+Constants.STATIC_IMAGES_HOST).split("@|;;");
		}
		return photos;
	}
	
	public static String addURLtoLogoFile(String logoFileName){
		if(StringUtils.isNotBlank(logoFileName)){
			return Constants.STATIC_LOGO_IMAGE_URL + "/" + logoFileName;
		}
		return logoFileName;
		
	}
	
	public static Map<String, String> convertWorkingHoursToMap(String workingHoursString){
		Map<String, String> workingHours = new HashMap<String, String>();
		
		if(StringUtils.isNotBlank(workingHoursString)){ 
			for(String daysAndHoursString: workingHoursString.split("@")){
				String[] daysAndHour = daysAndHoursString.split(" ",2);
				if(daysAndHour.length == 2){
					String hours = "CLOSED".equals(daysAndHour[1]) ? StringUtils.capitalize(daysAndHour[1].toLowerCase()) : daysAndHour[1];
					workingHours.put(getDay(daysAndHour[0]), hours);
				}
			}
		}
		
		return workingHours;
		
	}
	public static String formatMemberSinceDate(String dateString){
		
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date convertedCurrentDate = null;
		try {
			convertedCurrentDate = date.parse(dateString);
		}catch(Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(convertedCurrentDate != null){
			return String.format("%s %tB %<tY", "Dealer on Cars.com since",convertedCurrentDate);
		} else{
			return "";
		}

		
	}
	
	public static String formatOfferAndModelCount(String offerAndModelCount){
		
		String[] counts = null;
		if(StringUtils.isNotBlank(offerAndModelCount)){
			counts = offerAndModelCount.split(",");
		}
		
		if(counts[0].equals("0") && counts[1].equals("0")){
			return "";
		} else if(counts[1].equals("0")){
			return counts[0] + " Offers Available";
		} else{
			return counts[0] + " Offers on " + counts[1] + " Models";
		}
	}
	
	public static Long convertStringToLong(String longValue){
		
		Long value = 0L;
		try{
			value = Long.valueOf(longValue);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return value;
	}
	
	//Copied from dealer locator app. 
	public static String formatCurrency(Double number) {
		//if(number != null){
			if (Math.abs(Math.round(number) - number) < 0.004) {
				return String.format("$%,.0f", number);
			} else {
				return String.format("$%,.2f", number);
			}
		//}
	}
	
	//Copied from dealer locator app
	public static String formatPercentage(Double number) {
		DecimalFormat df = new DecimalFormat("#.###");
        
        StringBuffer sb = new StringBuffer();
        sb.append(df.format(number));
        sb.append("%");
        
        return sb.toString();
	}
	
	public static Boolean isValidPhoneNumber(String phoneNumber){

		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		Boolean isValid = false;
		try {
		  PhoneNumber swissNumberProto = phoneUtil.parse(phoneNumber, "US");
		  isValid = phoneUtil.isValidNumber(swissNumberProto);

		} catch (NumberParseException e) {
			e.printStackTrace();
		}
		
		return isValid;
	}
	
	public static Boolean isValidZip(String zip){
		return (NumberUtils.isNumber(zip) && zip.length() == ZIPCODE_LENGTH); 
	}
	
	//@Aditya Timmaraju
	//I have never used switch case(except in my assignments) and i wanted to use it
	//once in my life to make Dennis Ritchie, Bjarne Stroustrup and James Gosling
	//happy. Com'on guys, it is there for a reason.
	private static String getDay(String day){
		
		switch(day.toUpperCase()){
			case "MON":
				return "Monday";
			case "TUE":
				return "Tuesday";
			case "WED":
				return "Wednesday";
			case "THU":
				return "Thursday";
			case "FRI":
				return "Friday";
			case "SAT":
				return "Saturday";
			case "SUN":
				return "Sunday";
			default:
				return "";
		}
		
	}
}
