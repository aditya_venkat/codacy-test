package com.cars.df.customerinquiry.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cars.api.schemas.dealer_review.CategoryRate;
import com.cars.api.schemas.dealer_review.CategoryRateId;
import com.cars.api.schemas.dealer_review.CategoryRating;
import com.cars.api.schemas.dealer_review.CategoryType;
import com.cars.api.schemas.dealer_review.ConsumerDealerReview;
import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.api.schemas.dealer_review.DealerReviewResponse;
import com.cars.api.schemas.dealer_review.FeedbackResponse;
import com.cars.api.schemas.dealer_review.OverallCategoryRating;
import com.cars.api.schemas.dealer_review.ReturnCode;
import com.cars.api.schemas.dealer_review.VisitReason;
import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerDetails;
import com.cars.df.customerinquiry.bo.DealerResponseView;
import com.cars.df.customerinquiry.bo.DealerReviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewSummaryView;
import com.cars.df.customerinquiry.bo.DealerReviewView;
import com.cars.df.customerinquiry.dao.CustomerSearchDAO;
import com.cars.df.customerinquiry.dao.DealerReviewDAO;
import com.cars.df.customerinquiry.utility.Constants;
import com.cars.df.customerinquiry.utility.DealerReviewUtilityMapper;

public class DealerReviewService {
	
	private DealerReviewDAO dealerReviewDAO;
	
	private CustomerSearchDAO customerSearchDAO;
	
	private DealerReviewUtilityMapper dealerReviewUtilityMapper;
	

	
	public DealerReviewDetails getDealerReviewDetails(String customerId, String start, String count, String sortBy, String filterBy){
		
		String partyId = this.convertCustomerIdtoPartyId(customerId);
				
		Dealers dealers =  customerSearchDAO.getDealer(partyId);
		
		DealerReview dealerReview = dealerReviewDAO.getReviews(customerId, convertStringToNumber(start), 
				   convertStringToNumber(count), sortBy, filterBy);
		
		
		DealerReviewDetails dealerReviewDetails = dealerReviewUtilityMapper.createDealerReviewDetails(dealers, dealerReview);
		
		return dealerReviewDetails;
	}
	
	
	private Integer convertStringToNumber(String stringNumber){
		
		Integer intNumber = 0;
		
		try{
			intNumber = Integer.valueOf(stringNumber);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return intNumber;		
	}
	
	public Boolean saveDealerReview(DealerReviewView dealerReviewView){
		
		String partyId = this.convertCustomerIdtoPartyId(dealerReviewView.getDealerId());

		DealerReview dealerReview = dealerReviewUtilityMapper.generateDealerReview(dealerReviewView, partyId);
		
		ReturnCode returnCode = dealerReviewDAO.saveDealerReview(dealerReview);
		
		Boolean isreviewSaved = false;
		if (returnCode != null && returnCode.getCode().equals("SAVE_SUCCESSFUL")) {
			isreviewSaved = true;
		}
		return isreviewSaved;
		
	}
	
	public Boolean saveReviewFeedback(String customerId, String reviewId, Boolean helpful, Boolean isAbuse){
		
		String partyId = this.convertCustomerIdtoPartyId(customerId);
		
		Boolean status = true;
		
		DealerReview dealerReview = new DealerReview();
		dealerReview.setDealerPartyIdentifier(Long.valueOf(partyId));
		dealerReview.getConsumerDealerReview().add(new ConsumerDealerReview());
		ConsumerDealerReview consumerDealerReview = dealerReview.getConsumerDealerReview().get(0);
		
		consumerDealerReview.setConsumerDealerReviewIdentifier(Integer.valueOf(reviewId));
		consumerDealerReview.getFeedbackResponse().add(new FeedbackResponse());
		//TODO: get ip address
		//consumerDealerReview.getFeedbackResponse().get(0).setIpAddress(ipAddress);
		
		if(isAbuse){
			consumerDealerReview.getFeedbackResponse().get(0).setAbuse(true);
			consumerDealerReview.getFeedbackResponse().get(0).setHelpful(false);
		} else{
			consumerDealerReview.getFeedbackResponse().get(0).setAbuse(false);
			consumerDealerReview.getFeedbackResponse().get(0).setHelpful(helpful);
		}
		
		consumerDealerReview.getFeedbackResponse().get(0).setModeratorSubmittedDate(new Date());
		try {
			ReturnCode returnCode = dealerReviewDAO.saveFeedback(dealerReview);
			if(returnCode != null && !returnCode.getCode().equals("SAVE_SUCCESSFUL")){
				status = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			//logger.error("Review Feedback Error on Review ID" + reviewId + ":" + e.getMessage());
			status = false;
		}
		
		return status;
		
	}
	
	public Boolean saveReviewResponse(String customerId, String reviewId, DealerResponseView dealerResponseView){
		String partyId = this.convertCustomerIdtoPartyId(customerId);
		
		DealerReview dealerReview = dealerReviewUtilityMapper.generateDealerReviewResponse(partyId,reviewId,dealerResponseView) ;
		
		ReturnCode returnCode = dealerReviewDAO.saveReviewResponse(dealerReview);
		
		Boolean isResponseSaved = false;
		if (returnCode != null && returnCode.getCode().equals("SAVE_SUCCESSFUL")) {
			isResponseSaved = true;
		}
		return isResponseSaved;
		
	}
	private  String convertCustomerIdtoPartyId(String customerId){
		
		String partyId = customerSearchDAO.convertCustomerIdToPartyId(customerId);
		return partyId;
		
	}
	
	
	public DealerReviewDAO getDealerReviewDAO() {
		return dealerReviewDAO;
	}
	
	public void setDealerReviewDAO(DealerReviewDAO dealerReviewDAO) {
		this.dealerReviewDAO = dealerReviewDAO;
	}

	public CustomerSearchDAO getCustomerSearchDAO() {
		return customerSearchDAO;
	}

	public void setCustomerSearchDAO(CustomerSearchDAO customerSearchDAO) {
		this.customerSearchDAO = customerSearchDAO;
	}

	public DealerReviewUtilityMapper getDealerReviewUtilityMapper() {
		return dealerReviewUtilityMapper;
	}

	public void setDealerReviewUtilityMapper(DealerReviewUtilityMapper dealerReviewUtilityMapper) {
		this.dealerReviewUtilityMapper = dealerReviewUtilityMapper;
	}
}
