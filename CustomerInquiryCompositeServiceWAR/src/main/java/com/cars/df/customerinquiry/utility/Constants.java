package com.cars.df.customerinquiry.utility;

public class Constants {
	
	public static final Integer INVALID_ZIP_CODE = 9999;
	public static final String INVALID_ZIP_MESSAGE = "Invalid Zip Code. Please go back to high school to learn more about zip codes";
	
	public static final String FEATURED_MOST_RECENT = "featured-most-recent";
	public static final String ALL_RESULTS = "all-results";
	
	public static final String CUSTOMER_SERVICE = "CustomerService";
	public static final String BUYING_PROCESS = "BuyingProcess";
	public static final String QUALITY_OF_REPAIR = "QualityOfRepair";
	public static final String OVERALL_FACILITIES = "OverallFacilities";
	
	public static final String CUSTOMER_SERVICE_DSCRP = "Customer Service";
	public static final String BUYING_PROCESS_DSCRP = "Buying Process";
	public static final String QUALITY_OF_REPAIR_DSCRP = "Quality Of Repair";
	public static final String OVERALL_FACILITIES_DSCRP = "Overall Facilities";
	
	public static final String SHOP_NEW = "SHOPNEW";
	public static final String SHOP_USED = "SHOPUSED";
	public static final String SERVICE_REPAIR = "SVCREPAIR";
	
	public static final String STATIC_IMAGES_HOST = "http://www.cstatic-images.com";
	public static final String STATIC_LOGO_IMAGE_URL = "http://www.cstatic-images.com/logo-dealer/88-pixel";
	
	
	//Validaiton Messages
	
	//Consumer Dealer Review.
	public static final String ERROR_CNSMR_REVIEW = "Consumer Dealer Review cannot be null";
	
	public static final String ERROR_CNSMR_REVIEW_TITLE = "Review Title cannot be null or empty";
	public static final String INVL_CNSMR_REVIEW_TITLE = "Review title must be between 5 and 50 characters in length.";
	
	public static final String ERROR_CNSMR_REVIEW_TEXT = "Review Text cannot be null or empty";
	public static final String INVL_CNSMR_REVIEW_TEXT = "Review must be between 50 and 4000 characters in length.";
	
	public static final String ERROR_CNSMR_REVIEW_EMAIL = "Email address cannot be null or empty";
	public static final String INVL_CNSMR_REVIEW_EMAIL = "Invalid Email Address.Email address format:abc@def.com";
	
	public static final String ERROR_CNSMR_REVIEW_DSP_NAME = "Display Name cannot be null or empty";
	
	public static final String ERROR_CNSMR_REVIEW_LOCATION = "Location cannot be null or empty";
	
	public static final String INVL_CNSMR_REVIEW_VST_REASON = "Invalid visitng reason:There must be atleast one visiting reason ";
	
	
	public static final String INVL_IS_DEALER = "Is Dealer value can only be false. A dealer cannot submit a review";

	
	public static final String ERROR_CNSMR_REVIEW_OVRALL_RATING = "Overall Rating cannot be null or empty";

	public static final String INVL_CNSMR_REVIEW_RATING = "Rating should be between 1 and 5";
	
	public static final String NUMBER_EXCEPTION_CNSMR_REVIEW_RATING = "Rating should be an Integer between 1 and 5";

	
	//Dealer Email Lead Form
	public static final String ERROR_DEALER_EMAIL_LEAD = "Dealer Email Lead cannot be null";
	
	public static final String ERROR_FIRST_NAME = "First Name cannot be null or empty";

	public static final String ERROR_LAST_NAME = "Last Name cannot be null or empty";
	
	public static final String INVL_EMAIL = "Invalid Email Address.Email address format:abc@def.com";
	
	public static final String ERROR_ZIP = "Zipcode cannot be empty or null";

	public static final String INVL_ZIP = "Zipcode must be a valid 5 digit integer";
	
	public static final String INVL_DEALER_PHONE_NUMBER = "Invalid Dealer Phone Number";
	
	public static final String INVL_EVE_PHONE_NUMBER = "Invalid Evening Phone Number";
	
	public static final String INVL_DAY_PHONE_NUMBER = "Invalid Day Phone Number";
	
	public static final String ERROR_DEALER_EMAIL_COMMENTS = "Comments cannot be empty or null";
	
	public static final String ERROR_DEALER_EMAIL_ODSID = "Party Id cannot be empty or null";
	
	public static final String ERROR_DEALER_NAME = "Dealer Name cannot be empty or null";
	
	public static final String INVL_DEALER_EMAIL = "Invalid Dealer Email Address";
	
	public static final String INVL_DEALER_NUMBER = "Invalid Dealer Number";
	
	public static final String ERROR_JSC_COLLECTOR = "JSC Collector cannot be empty or null";
	
	public static final String ERROR_WEB_PAGE_ID = "Web Page Type Id cannot be empty or null";


	


	//Dealer Response View
	public static final String ERROR_DEALER_REPONSE_VIEW = "Dealer Response cannot be null";

	public static final String ERROR_DEALER_REPONSE_VIEW_NAME = "Name cannot be empty or null in dealer response view";

	public static final String ERROR_DEALER_REPONSE_VIEW_TEXT = "Text cannot be empty or null in dealer response view";

	public static final String INVL_DEALER_REPONSE_VIEW_EMAIL = "Invalid Email Address";

	public static final String INVL_DEALER_REPONSE_VIEW_PHONE = "Invalid Phone Number";
	
	public static final String INVL_DEALER_REPONSE_VIEW_ZIP = "Invalid Zip Code";


}
