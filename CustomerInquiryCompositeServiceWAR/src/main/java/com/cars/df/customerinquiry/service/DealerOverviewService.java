package com.cars.df.customerinquiry.service;

import java.util.concurrent.ForkJoinPool;

import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerOverviewDetails;
import com.cars.df.customerinquiry.dao.CustomerSearchDAO;
import com.cars.df.customerinquiry.dao.DealerReviewDAO;
import com.cars.df.customerinquiry.dao.InventorySearchDAO;
import com.cars.df.customerinquiry.dao.SpecialOfferDAO;
import com.cars.df.customerinquiry.utility.Constants;
import com.cars.df.customerinquiry.utility.DealerCommonUtilityMapper;
import com.cars.df.customerinquiry.utility.DealerOverviewUtilityMapper;
import com.cars.service.listingsearch.bo.SearchResultBO;


public class DealerOverviewService {

	
	private CustomerSearchDAO customerSearchDAO;
	
	private DealerReviewDAO dealerReviewDAO;
	
	private InventorySearchDAO inventorySearchDAO;
	
	private SpecialOfferDAO specialOfferDAO;
	
	private DealerOverviewUtilityMapper dealerOverviewUtilityMapper;
	
	private static final Integer SINGLE_DEALER = 1;
	
	private static final Integer OVERVIEW_REVIEW_START_INDEX = 0;
	
	private static final Integer OVERVIEW_REVIEW_COUNT = 3;
	
	
	
	//TODO: Please multi-thread me. I am calling 5(yes you heard it right, 5!!) different services.
	// I will be really happy you that(Don't like slowing down things).
	public DealerOverviewDetails getDealerOverviewDetails(String customerId){
		
		
		String partyId = customerSearchDAO.convertCustomerIdToPartyId(customerId);
		
		Dealers dealers =  customerSearchDAO.getDealer(partyId);
		
		DealerReview dealerReview = dealerReviewDAO.getReviews(customerId, OVERVIEW_REVIEW_START_INDEX, 
				OVERVIEW_REVIEW_COUNT, Constants.FEATURED_MOST_RECENT, Constants.ALL_RESULTS);
				
		SearchResultBO inventorySearchResults = new SearchResultBO();
		if(dealers != null && dealers.getDealers() != null && dealers.getDealers().size() == SINGLE_DEALER){
			Dealer dealer = dealers.getDealers().get(0);
			inventorySearchResults =  inventorySearchDAO.getInventoryDetails(partyId, dealer.getZip());
		}
		
		String countOffersAndModels = specialOfferDAO.getOfferAndModelCount(partyId);
		
		//TODO:Add a service call to DDCS for sales representative
		//Not doing it now because its not MVP
		
		DealerOverviewDetails dealerOverviewDetails = dealerOverviewUtilityMapper.createDealerOverviewDetails(dealers, dealerReview, inventorySearchResults, countOffersAndModels);
		
		return dealerOverviewDetails;
						
	}
	
	/*private void testingshit(){
		ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
	}*/


	public CustomerSearchDAO getCustomerSearchDAO() {
		return customerSearchDAO;
	}


	public void setCustomerSearchDAO(CustomerSearchDAO customerSearchDAO) {
		this.customerSearchDAO = customerSearchDAO;
	}


	public DealerReviewDAO getDealerReviewDAO() {
		return dealerReviewDAO;
	}


	public void setDealerReviewDAO(DealerReviewDAO dealerReviewDAO) {
		this.dealerReviewDAO = dealerReviewDAO;
	}


	public InventorySearchDAO getInventorySearchDAO() {
		return inventorySearchDAO;
	}


	public void setInventorySearchDAO(InventorySearchDAO inventorySearchDAO) {
		this.inventorySearchDAO = inventorySearchDAO;
	}


	public SpecialOfferDAO getSpecialOfferDAO() {
		return specialOfferDAO;
	}


	public void setSpecialOfferDAO(SpecialOfferDAO specialOfferDAO) {
		this.specialOfferDAO = specialOfferDAO;
	}


	public DealerOverviewUtilityMapper getDealerOverviewUtilityMapper() {
		return dealerOverviewUtilityMapper;
	}


	public void setDealerOverviewUtilityMapper(DealerOverviewUtilityMapper dealerOverviewUtilityMapper) {
		this.dealerOverviewUtilityMapper = dealerOverviewUtilityMapper;
	}

	
}
