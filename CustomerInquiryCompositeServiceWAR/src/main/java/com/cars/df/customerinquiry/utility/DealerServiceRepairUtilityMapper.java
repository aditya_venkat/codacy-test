package com.cars.df.customerinquiry.utility;

import java.util.ArrayList;
import java.util.List;

import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerAmenity;
import com.cars.df.customerinquiry.bo.DealerCommonService;
import com.cars.df.customerinquiry.bo.DealerFastFact;
import com.cars.df.customerinquiry.bo.DealerServiceRepairDetails;
import com.cars.df.customerinquiry.bo.DealerServiceWarranty;
import com.cars.rs.client.api.Amenities;
import com.cars.rs.client.api.Amenity;
import com.cars.rs.client.api.CommonService;
import com.cars.rs.client.api.CommonServices;
import com.cars.rs.client.api.FastFact;
import com.cars.rs.client.api.FastFacts;
import com.cars.rs.client.api.ServiceCustomer;
import com.cars.rs.client.api.ServiceWarranty;

public class DealerServiceRepairUtilityMapper {
	
	private DealerCommonUtilityMapper dealerCommonUtilityMapper;
	
	private static final Integer SINGLE_DEALER = 1;


	public DealerServiceRepairDetails convertServiceCustomerToServiceRepairDetails(Dealers dealers,ServiceCustomer serviceCustomer){
		
		DealerServiceRepairDetails dealerServiceRepairDetails = new DealerServiceRepairDetails();
		
		if(dealers != null && dealers.getDealers() != null && dealers.getDealers().size() == SINGLE_DEALER){
			Dealer dealer = dealers.getDealers().get(0);
			
			//Common dealer details
			dealerCommonUtilityMapper.createDealerDetailSummary(dealer, dealerServiceRepairDetails);

			dealerServiceRepairDetails.setServiceAppointmentUrl(dealer.getServiceAppointmentURl());
			
			dealerServiceRepairDetails.setServiceHoursOperation(CICSUtility.convertWorkingHoursToMap(dealer.getServiceHours()));
			Amenities amenities = serviceCustomer.getAmenities();
			
			if(amenities != null && amenities.getAmenity() != null){
				List<DealerAmenity> exclusiveAmenititesList = new ArrayList<DealerAmenity>();
				for(Amenity amenity: amenities.getAmenity()){
					DealerAmenity dealerAmenity = new DealerAmenity();
					
					dealerAmenity.setCategory(amenity.getAmenityCategory());
					dealerAmenity.setCode(amenity.getAmenityCd());
					dealerAmenity.setDescription(amenity.getAmenityDescription());
					dealerAmenity.setFreeFormText(amenity.getAmenityFreeFormText());
					
					
					//Adding exclusive amenity to a different list. 
					//Why are we doing this????? 
					//Ans: Service Repair page wants ameities list to be sort by EXCLUSIVE
					//We are adding exlusive amenities to a different list and finally we will add
					//it to the top of the list.
					if(!"EXC".equals(amenity.getAmenityCd())){
						dealerServiceRepairDetails.addAmenity(dealerAmenity);
					} else{
						exclusiveAmenititesList.add(dealerAmenity);
					}
				}
				//Add exclusive amenity list to the top of the list
				dealerServiceRepairDetails.addAllAmenity(0, exclusiveAmenititesList);
			}
			
			
			FastFacts fastFacts = serviceCustomer.getFastFacts();
			
			if(fastFacts != null && fastFacts.getFastFact() != null){
				for(FastFact fastFact: fastFacts.getFastFact()){
					
					DealerFastFact dealerFastFact = new DealerFastFact();
					
					dealerFastFact.setCode(fastFact.getFastFactCd());
					dealerFastFact.setDescription(fastFact.getFastFactDescription());
					dealerFastFact.setValue(fastFact.getFastFactValue());
					
					dealerServiceRepairDetails.addFastFact(dealerFastFact);
					
				}
			}
			
			
			CommonServices commonServices = serviceCustomer.getCommonServices();
			
			if(commonServices != null && commonServices.getCommonService() != null){
				for(CommonService commonService: commonServices.getCommonService()){
					
					DealerCommonService dealerCommonService = new DealerCommonService();
					
					dealerCommonService.setCode(commonService.getCommonServiceCd());
					dealerCommonService.setDescription(commonService.getCommonServiceDescription());
					dealerCommonService.setDetail(commonService.getCommonServiceDetail());
					dealerCommonService.setDisclaimer(commonService.getCommonServiceDisclaimer());
					dealerCommonService.setFreeIndicator(commonService.getCommonServiceFreeIndicator());
					dealerCommonService.setPrice(commonService.getCommonServicePrice());
					dealerCommonService.setPriceType(commonService.getCommonServicePriceType());
					dealerCommonService.setProductDescription(commonService.getCommonServiceProductDescription());
					
					
					dealerServiceRepairDetails.addCommonService(dealerCommonService);
					
				}
			}
			
			
			ServiceWarranty serviceWarranty = serviceCustomer.getServiceWarranty();
			
			if(serviceWarranty != null){
				DealerServiceWarranty dealerServiceWarranty = new DealerServiceWarranty();
				
				dealerServiceWarranty.setDetails(serviceWarranty.getServiceWarrantyDetails());
				dealerServiceWarranty.setDisclaimer(serviceWarranty.getServiceWarrantyDisclaimer());
				dealerServiceWarranty.setMiles(serviceWarranty.getServiceWarrantyMiles());
				dealerServiceWarranty.setMonths(serviceWarranty.getServiceWarrantyMonths());
				
				dealerServiceRepairDetails.setDealerServiceWarranty(dealerServiceWarranty);
					
			}
			
			dealerServiceRepairDetails.setServiceOverview( serviceCustomer.getServiceOverview());
						
		}
		return dealerServiceRepairDetails;
		
	}


	public DealerCommonUtilityMapper getDealerCommonUtilityMapper() {
		return dealerCommonUtilityMapper;
	}


	public void setDealerCommonUtilityMapper(DealerCommonUtilityMapper dealerCommonUtilityMapper) {
		this.dealerCommonUtilityMapper = dealerCommonUtilityMapper;
	}
	
}
