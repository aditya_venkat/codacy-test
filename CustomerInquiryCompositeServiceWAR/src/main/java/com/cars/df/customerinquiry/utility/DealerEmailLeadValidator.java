package com.cars.df.customerinquiry.utility;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cars.df.customerinquiry.bo.DealerEmailLead;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

public class DealerEmailLeadValidator implements Validator{

	@Override
	public boolean supports(Class dealerEmailLead) {
		// TODO Auto-generated method stub
		return DealerEmailLead.class.equals((dealerEmailLead));
	}

	@Override
	public void validate(Object emailLead, Errors errors) {
		// TODO Auto-generated method stub
		
		if(emailLead == null){
			errors.reject(Constants.ERROR_DEALER_EMAIL_LEAD);
		} else{
			DealerEmailLead dealerEmailLead = (DealerEmailLead)emailLead;
			
			if(StringUtils.isBlank(dealerEmailLead.getFirstName())){
				errors.reject(Constants.ERROR_FIRST_NAME);

			}
			
			if(StringUtils.isBlank(dealerEmailLead.getLastName())){
				errors.reject(Constants.ERROR_LAST_NAME);
			}
			
			if(!EmailValidator.getInstance().isValid(dealerEmailLead.getEmail())){
				errors.reject(Constants.INVL_EMAIL);
			}
			
			if(StringUtils.isBlank(dealerEmailLead.getZipCode())){
				errors.reject(Constants.ERROR_ZIP);
			} else if(!(NumberUtils.isNumber(dealerEmailLead.getZipCode()) && dealerEmailLead.getZipCode().length() == 5)){
				errors.reject(Constants.INVL_ZIP);
			}
			
			
			if(!CICSUtility.isValidPhoneNumber(dealerEmailLead.getEveningNumber())){
				errors.reject(Constants.INVL_EVE_PHONE_NUMBER);
			}
			
			
			if(!CICSUtility.isValidPhoneNumber(dealerEmailLead.getDaytimeNumber())){
				errors.reject(Constants.INVL_DAY_PHONE_NUMBER);
			}
			
			
			if(StringUtils.isBlank(dealerEmailLead.getComments())){
				errors.reject(Constants.ERROR_DEALER_EMAIL_COMMENTS);
			}
			
			if(StringUtils.isEmpty(dealerEmailLead.getOdsId())){
				errors.reject(Constants.ERROR_DEALER_EMAIL_ODSID);
			}
			
			if(StringUtils.isEmpty(dealerEmailLead.getDealerName())){
				errors.reject(Constants.ERROR_DEALER_NAME);
			}
			
			if(!EmailValidator.getInstance().isValid(dealerEmailLead.getDealerEmail())){
				//errors.reject(Constants.ERROR_DEALER_EMAIL_LEAD);
			}
			
		
			if(!CICSUtility.isValidPhoneNumber(dealerEmailLead.getDealerNumber())){
				errors.reject(Constants.INVL_DEALER_NUMBER);
			}
			
			
			if(StringUtils.isBlank(dealerEmailLead.getIsNew())){
				//errors.reject(Constant.ERROR_CNSMR_REVIEW);
			}
			
			if(StringUtils.isBlank(dealerEmailLead.getLeadType())){
				//errors.reject(Constant.ERROR_CNSMR_REVIEW);
			}
				
			if(dealerEmailLead.getPhonetype() != null && dealerEmailLead.getPhonetype().length > 0 &&  dealerEmailLead.getPhonetype().length < 3){
				//errors.reject(Constant.ERROR_CNSMR_REVIEW);
			} 
			
			if(StringUtils.isBlank(dealerEmailLead.getJscCollector())){
				errors.reject(Constants.ERROR_JSC_COLLECTOR);
			}
			
			if(StringUtils.isBlank(dealerEmailLead.getWebPageTypeId())){
				errors.reject(Constants.ERROR_WEB_PAGE_ID);
			}
		}
		
	}


}
