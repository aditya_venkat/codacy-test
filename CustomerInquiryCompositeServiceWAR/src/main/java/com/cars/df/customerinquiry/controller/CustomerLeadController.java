/**
 * 
 */
package com.cars.df.customerinquiry.controller;

import java.util.List;







import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cars.df.customerinquiry.bo.DealerEmailLead;
import com.cars.df.customerinquiry.service.LeadpathService;
import com.cars.df.customerinquiry.utility.DealerEmailLeadValidator;


/**
 * @author lgiovenco
 *
 */

@RestController
public class CustomerLeadController {
	
	@Autowired
	LeadpathService leadpathService; 
	
	@Autowired
	private DealerEmailLeadValidator dealerEmailLeadValidator;

	@RequestMapping(value = "dealer/{customerId}/emailLead", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<StringBuffer> handleEmailLead(@RequestBody DealerEmailLead dealerEmailLead, 
			@PathVariable("customerId") String customerId, 
			@CookieValue(value = "affiliate", defaultValue = "") String affiliateCookie,
			HttpServletRequest request){
		
		
		BeanPropertyBindingResult bindingResults = new BeanPropertyBindingResult(dealerEmailLead, "Errors");
		dealerEmailLeadValidator.validate(dealerEmailLead, bindingResults);

		if(bindingResults.hasErrors()){
			StringBuffer message = new StringBuffer();
			List<ObjectError> errors =  bindingResults.getGlobalErrors();
			for (ObjectError error : errors) {
				if (message.length() > 0) {
					message.append(";");
				}
				message.append(error.getCode());
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);

		} else{
			Boolean status =  leadpathService.sendDealerEmail(dealerEmailLead, request, affiliateCookie, customerId);
			
			if(status){
				return ResponseEntity.status(HttpStatus.OK).body( new StringBuffer("SAVE_SUCCESSFUL"));
			} else{
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new StringBuffer("SAVE_UNSUCCESSFUL"));
			}
		}
	}

	public LeadpathService getLeadpathService() {
		return leadpathService;
	}

	public void setLeadpathService(LeadpathService leadpathService) {
		this.leadpathService = leadpathService;
	}

	public DealerEmailLeadValidator getDealerEmailLeadValidator() {
		return dealerEmailLeadValidator;
	}

	public void setDealerEmailLeadValidator(DealerEmailLeadValidator dealerEmailLeadValidator) {
		this.dealerEmailLeadValidator = dealerEmailLeadValidator;
	}
	
}
