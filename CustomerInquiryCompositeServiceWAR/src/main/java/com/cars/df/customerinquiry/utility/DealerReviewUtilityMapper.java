package com.cars.df.customerinquiry.utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cars.api.schemas.dealer_review.CategoryRate;
import com.cars.api.schemas.dealer_review.CategoryRateId;
import com.cars.api.schemas.dealer_review.CategoryRating;
import com.cars.api.schemas.dealer_review.CategoryType;
import com.cars.api.schemas.dealer_review.ConsumerDealerReview;
import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.api.schemas.dealer_review.DealerReviewResponse;
import com.cars.api.schemas.dealer_review.OverallCategoryRating;
import com.cars.api.schemas.dealer_review.VisitReason;
import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerResponseView;
import com.cars.df.customerinquiry.bo.DealerReviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewSummaryView;
import com.cars.df.customerinquiry.bo.DealerReviewView;


public class DealerReviewUtilityMapper {
	
	private DealerCommonUtilityMapper dealerCommonUtilityMapper;
	
	private static final Integer SINGLE_DEALER = 1;

	
	
	public DealerReviewDetails createDealerReviewDetails(Dealers dealers, DealerReview dealerReview){
		
		DealerReviewDetails dealerReviewDetails = new DealerReviewDetails();
		
		if(dealers != null && dealers.getDealers() != null && dealers.getDealers().size() == SINGLE_DEALER){
			
			Dealer dealer = dealers.getDealers().get(0);
			dealerCommonUtilityMapper.createDealerDetailSummary(dealer, dealerReviewDetails);
			
			DealerReviewSummaryView dealerReviewSummaryView = this.createDealerReviewSummaryView(dealerReview);
			dealerReviewDetails.setDealerReviewSummaryView(dealerReviewSummaryView);

		}
		return dealerReviewDetails;
		
	}
	
	public DealerReviewSummaryView createDealerReviewSummaryView(DealerReview dealerReview){
		DealerReviewSummaryView dealerReviewSummaryView = new DealerReviewSummaryView();
		
		if(dealerReview != null){
			dealerReviewSummaryView.setOverallRating(String.valueOf(dealerReview.getOverallDealershipRating()));
			
			List<OverallCategoryRating> overallRatings = dealerReview.getOverallSubCategoryRating();
			for (OverallCategoryRating overallCategoryRating : overallRatings) {
				if(Constants.CUSTOMER_SERVICE.equals(overallCategoryRating.getId().getCategoryTypeCode())){
					dealerReviewSummaryView.setCustomerServiceRating(String.valueOf(overallCategoryRating.getRating()));
				}
				else if(Constants.BUYING_PROCESS.equals(overallCategoryRating.getId().getCategoryTypeCode())){
					dealerReviewSummaryView.setBuyingProcessRating(String.valueOf(overallCategoryRating.getRating()));
				}
				else if(Constants.QUALITY_OF_REPAIR.equals(overallCategoryRating.getId().getCategoryTypeCode())){
					dealerReviewSummaryView.setQualityOfRepairRating(String.valueOf(overallCategoryRating.getRating()));
				}
				else if(Constants.OVERALL_FACILITIES.equals(overallCategoryRating.getId().getCategoryTypeCode())){
					dealerReviewSummaryView.setFacilityRating(String.valueOf(overallCategoryRating.getRating()));
				}
			}
			
			dealerReviewSummaryView.setRecommendedNumber(String.valueOf(dealerReview.getTotalRecommendedNumber()));
			dealerReviewSummaryView.setTotalReviews(String.valueOf(dealerReview.getFilteredReviewCount()));
			dealerReviewSummaryView.setTotalReviewsOnPage(String.valueOf(dealerReview.getConsumerDealerReview().size()));
			
			dealerReviewSummaryView.setDealerReviewList(convertDealeReviewToDealerReviewView(dealerReview));
		}
		
		return dealerReviewSummaryView;
	}
		
	private List<DealerReviewView> convertDealeReviewToDealerReviewView(DealerReview dealerReview){
		
		List<DealerReviewView> dealerReviewViewList = new ArrayList<DealerReviewView>();
		
		if(dealerReview != null && dealerReview.getConsumerDealerReview() != null){
			
			for(ConsumerDealerReview consumerDealerReview: dealerReview.getConsumerDealerReview()){
				
				DealerReviewView dealerReviewView = new DealerReviewView();
				
				dealerReviewView.setReviewId(String.valueOf(consumerDealerReview.getConsumerDealerReviewIdentifier()));
				
				dealerReviewView.setReviewerEmail(consumerDealerReview.getEmail());
				dealerReviewView.setReviewerLoc(consumerDealerReview.getLocationDescription());
				dealerReviewView.setReviewerName(consumerDealerReview.getDisplayName());
				dealerReviewView.setReviewText(consumerDealerReview.getText());
				dealerReviewView.setReviewTitle(consumerDealerReview.getTitle());
				dealerReviewView.setHelpfulCount(String.valueOf(consumerDealerReview.getHelpfulCount()));
				dealerReviewView.setUnHelpfulCount(String.valueOf(consumerDealerReview.getUnhelpfulCount()));
				dealerReviewView.setOverallRating(String.valueOf(consumerDealerReview.getOverallRating()));
				
				dealerReviewView.setReviewDate(consumerDealerReview.getSubmittedDate().toString());
			
				dealerReviewView.setRecommend(consumerDealerReview.isRecommendDealer());
			
				dealerReviewView.setPurchase(consumerDealerReview.isPurchaseVehicle());
				
				// fill ratings for this dealer
				//extractRatings(dealerReviewView, review);
				List<CategoryRating> categoryRatings = consumerDealerReview.getCategoryRatings();;
				for (CategoryRating categoryRating : categoryRatings) {
					if(Constants.CUSTOMER_SERVICE.equals(categoryRating.getCategoryType().getCode())){
						dealerReviewView.setCustomerServiceRating(String.valueOf(categoryRating.getRating().getId().getRatingNumber()));
					}
					else if(Constants.BUYING_PROCESS.equals(categoryRating.getCategoryType().getCode())){
						dealerReviewView.setBuyingProcessRating(String.valueOf(categoryRating.getRating().getId().getRatingNumber()));
					}
					else if(Constants.QUALITY_OF_REPAIR.equals(categoryRating.getCategoryType().getCode())){
						dealerReviewView.setQualityOfRepairRating(String.valueOf(categoryRating.getRating().getId().getRatingNumber()));
					}
					else if(Constants.OVERALL_FACILITIES.equals(categoryRating.getCategoryType().getCode())){
						dealerReviewView.setFacilityRating(String.valueOf(categoryRating.getRating().getId().getRatingNumber()));
					}
				}
			
				// fill visit reason
				//extractVisitReasons(dealerReviewView, review);
				
				List<VisitReason> visitReasons = consumerDealerReview.getVisitReason();
				for (VisitReason visitReason : visitReasons) {
					if(visitReason.getCode().equals("SHOPNEW")){
						dealerReviewView.setVisitNewCar(true);
					}
					else if(visitReason.getCode().equals("SHOPUSED")){
						dealerReviewView.setVisitUsedCar(true);
					}
					else if(visitReason.getCode().equals("SVCREPAIR")){
						dealerReviewView.setVisitServiceCar(true);
					}
				}
				
				// fill in dealer responses if there are any
				//dealerReviewView.setDealerResponseView(fillDealerReviews(review.getDealerReviewResponse()));
				DealerResponseView dealerResponsesView = new DealerResponseView();
				for (DealerReviewResponse dealerReviewResponse : consumerDealerReview.getDealerReviewResponse()) {
					if(dealerReviewResponse.getStatus().getCode().equals("APP")){
						if (dealerReviewResponse.getModeratorResponseDate() != null) {
							dealerResponsesView.setDate(dealerReviewResponse.getModeratorResponseDate().toString());
						}
						dealerResponsesView.setText(dealerReviewResponse.getText());
						dealerResponsesView.setName(dealerReviewResponse.getResponseName());
						dealerReviewView.setDealerResponseView(dealerResponsesView);
						break;
					}
				}
				
				
				dealerReviewView.setIsFeatured(consumerDealerReview.getFeaturedReview() == 1 ? true: false);
				
				dealerReviewViewList.add(dealerReviewView);
			}
		}
		
		return dealerReviewViewList;
		
	}
	
	public DealerReview generateDealerReview(DealerReviewView dealerReviewView, String partyId) {
		DealerReview dealerReview = new DealerReview();
		
		
		dealerReview.setDealerPartyIdentifier(Long.valueOf(partyId));
		ConsumerDealerReview consumerDealerReview = new ConsumerDealerReview();
		List<VisitReason> visitReasonList = new ArrayList<VisitReason>();
		
		// new car
		if(dealerReviewView.getVisitNewCar()){
			VisitReason visitReason = new VisitReason();
			visitReason.setCode("SHOPNEW");
			visitReasonList.add(visitReason);
		}
		
		// used car
		if(dealerReviewView.getVisitUsedCar()){
			VisitReason visitReason = new VisitReason();
			visitReason.setCode("SHOPUSED");
			visitReasonList.add(visitReason);
		}
		
		// service car
		if(dealerReviewView.getVisitServiceCar()){
			VisitReason visitReason = new VisitReason();
			visitReason.setCode("SVCREPAIR");
			visitReasonList.add(visitReason);
		}
		consumerDealerReview.setVisitReason(visitReasonList);

		consumerDealerReview.setTitle(dealerReviewView.getReviewTitle());
		consumerDealerReview.setText(dealerReviewView.getReviewText());
		consumerDealerReview.setHelpfulCount(0);
		consumerDealerReview.setUnhelpfulCount(0);
		consumerDealerReview.setDisplayName(dealerReviewView.getReviewerName());
		consumerDealerReview.setLocationDescription(dealerReviewView.getReviewerLoc());
		consumerDealerReview.setEmail(dealerReviewView.getReviewerEmail());
		consumerDealerReview.setPromotionTrackingCode(dealerReviewView.getPromoCode());
		consumerDealerReview.setIpAddress(dealerReviewView.getIpAddress());
		consumerDealerReview.setSubmittedDate(new Date());
		consumerDealerReview.setOverallRating(Double.valueOf(dealerReviewView.getOverallRating()));
		consumerDealerReview.setPurchaseVehicle(dealerReviewView.getPurchase());
		consumerDealerReview.setRecommendDealer(dealerReviewView.getRecommend());
		consumerDealerReview.setDealerEmployed(dealerReviewView.getIsDealer());
		List<CategoryRating> csr = consumerDealerReview.getCategoryRatings();
		
		csr.add(getNewCategoryRating(Constants.CUSTOMER_SERVICE, Constants.CUSTOMER_SERVICE_DSCRP,dealerReviewView.getCustomerServiceRating()));
		csr.add(getNewCategoryRating(Constants.BUYING_PROCESS, Constants.BUYING_PROCESS_DSCRP,dealerReviewView.getBuyingProcessRating()));
		csr.add(getNewCategoryRating(Constants.QUALITY_OF_REPAIR, Constants.QUALITY_OF_REPAIR_DSCRP,dealerReviewView.getQualityOfRepairRating()));
		csr.add(getNewCategoryRating(Constants.OVERALL_FACILITIES,Constants.OVERALL_FACILITIES_DSCRP,dealerReviewView.getFacilityRating()));
		dealerReview.getConsumerDealerReview().add(consumerDealerReview);
		
		return dealerReview;
	}
	
	public DealerReview generateDealerReviewResponse(String partyId, String reviewId,DealerResponseView dealerResponseView){
		DealerReview dealerReview = new DealerReview();
		
		dealerReview.setDealerPartyIdentifier(Long.valueOf(partyId));
		
		ConsumerDealerReview consumerDealerReview = new ConsumerDealerReview();
		
		consumerDealerReview.setConsumerDealerReviewIdentifier(Integer.valueOf(reviewId));
		consumerDealerReview.setHelpfulCount(0);
		consumerDealerReview.setUnhelpfulCount(0);

		DealerReviewResponse dealerReviewResponse = new DealerReviewResponse();
		
		dealerReviewResponse.setEmail(dealerResponseView.getEmail());
		dealerReviewResponse.setText(dealerResponseView.getText());
		dealerReviewResponse.setPostalCode(dealerResponseView.getZipCode());
		dealerReviewResponse.setPhoneDisplay(dealerResponseView.getPhoneNumber());
		dealerReviewResponse.setResponseName(dealerResponseView.getName());
		
		//TOOD: how do we get ipaddress
		//dealerReviewResponse.setIpAddress(getIP4DRMS());
		
		consumerDealerReview.getDealerReviewResponse().add(dealerReviewResponse);
		dealerReview.getConsumerDealerReview().add(consumerDealerReview);

		
		return dealerReview;
	}
	
	private CategoryRating getNewCategoryRating(String code, String description,String rating) {

		CategoryRating categoryRating = new CategoryRating();
		CategoryType categoryType = new CategoryType();
		categoryType.setDescription(description);
		categoryType.setCode(code);
		categoryRating.setCategoryType(categoryType);
		CategoryRate categoryRate = new CategoryRate();
		CategoryRateId categoryRateId = new CategoryRateId();
		categoryRateId.setCode(code);
		try {
			categoryRateId.setRatingNumber(Double.valueOf(rating));
		} catch (NumberFormatException e) {
			categoryRateId.setRatingNumber(0.0);
		}
		categoryRate.setId(categoryRateId);
		categoryRating.setRating(categoryRate);
		return categoryRating;
	}


	public DealerCommonUtilityMapper getDealerCommonUtilityMapper() {
		return dealerCommonUtilityMapper;
	}

	public void setDealerCommonUtilityMapper(DealerCommonUtilityMapper dealerCommonUtilityMapper) {
		this.dealerCommonUtilityMapper = dealerCommonUtilityMapper;
	}	
		
}
