package com.cars.df.customerinquiry.utility;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerOverviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewSummaryView;
import com.cars.service.listingsearch.bo.RefinementBO;
import com.cars.service.listingsearch.bo.RefinementValueBO;
import com.cars.service.listingsearch.bo.SearchResultBO;

public class DealerOverviewUtilityMapper {

	private static final Integer SINGLE_DEALER = 1;
	
	private DealerCommonUtilityMapper dealerCommonUtilityMapper;
	
	private DealerReviewUtilityMapper dealerReviewUtilityMapper;
	
	
	public DealerOverviewDetails createDealerOverviewDetails(Dealers dealers, DealerReview dealerReview, SearchResultBO inventorySearchResults,String countOffersAndModels){
		
		DealerOverviewDetails dealerOverviewDetails = new DealerOverviewDetails();
		
		if(dealers != null && dealers.getDealers() != null && dealers.getDealers().size() == SINGLE_DEALER){
			Dealer dealer = dealers.getDealers().get(0);
			
			//Common dealer details
			dealerCommonUtilityMapper.createDealerDetailSummary(dealer, dealerOverviewDetails);
			
			
			//OverviewDealer Stuffs
			if(StringUtils.isNotBlank(dealer.getPhotos())){
				dealerOverviewDetails.setPhotoUrlList(Arrays.asList(CICSUtility.convertPhotoStringToURL(dealer.getPhotos())));
			}
			dealerOverviewDetails.setPromoVideoUrl(dealer.getPromoUrl());
			dealerOverviewDetails.setLogoUrl(CICSUtility.addURLtoLogoFile(dealer.getLogoUrl()));
			dealerOverviewDetails.setDealerWebsiteLink(dealer.getWebsiteUrl());

			dealerOverviewDetails.setSalesHoursOperation(CICSUtility.convertWorkingHoursToMap(dealer.getSalesHours()));
			dealerOverviewDetails.setServiceHoursOperation(CICSUtility.convertWorkingHoursToMap(dealer.getServiceHours()));
			
			
			//Dealer Review Stuffs
			DealerReviewSummaryView dealerReviewSummaryView = dealerReviewUtilityMapper.createDealerReviewSummaryView(dealerReview);
			
			if(dealerReviewSummaryView.getDealerReviewList().size() > 0){
				dealerOverviewDetails.setHasDealerReview(true);
				dealerOverviewDetails.setDealerReviewSummaryView(dealerReviewSummaryView);;
			} else{
				dealerOverviewDetails.setHasDealerReview(false);
			}
			
			
			//Inventory stuffs
			RefinementBO refinement = inventorySearchResults.getRefinement("stkTypId");
			
			if(refinement != null && refinement.getValues() != null){
				for(RefinementValueBO valueBO: refinement.getValues()){
					if("New".equals(valueBO.getName())){
						dealerOverviewDetails.setNewInventoryCount(String.valueOf(valueBO.getCount()));
						dealerOverviewDetails.setHasInventory(true);
					} else if("Used".equals(valueBO.getName())){
						dealerOverviewDetails.setUsedInventoryCount(String.valueOf(valueBO.getCount()));
						dealerOverviewDetails.setHasInventory(true);
					}
				}
			}
			
			//Service and repair stuffs
			dealerOverviewDetails.setHasServiceRepair(dealer.getHasServiceAndRepairContent());
			dealerOverviewDetails.setServiceAppointmentUrl(dealer.getServiceAppointmentURl());
			dealerOverviewDetails.setRpcCertified(dealer.getRpcCertified());
			
			//Sell and Trade Stuffs
			dealerOverviewDetails.setHasSellAndTrade(dealer.getHasSellAndTradeMarket());
			
			//Special Offer stuffs
			String formattedOfferAndModelCount = CICSUtility.formatOfferAndModelCount(countOffersAndModels);
			if(StringUtils.isBlank(formattedOfferAndModelCount)){
				dealerOverviewDetails.setHasSpecialOffer(false);
			} else{
				dealerOverviewDetails.setHasSpecialOffer(true);
				dealerOverviewDetails.setOfferAndModelString(formattedOfferAndModelCount);
				
			}
			
			//About us Stuffs
			//We are doing/not doing staff information. Yet be decided by Product people
			//dealerOverviewDetails.setHasAboutUs(hasAboutUs);
			if(StringUtils.isBlank(dealer.getDealerLongDescription()) && StringUtils.isBlank(dealer.getMemberSince())){
				dealerOverviewDetails.setHasAboutUs(false);
			} else{
				dealerOverviewDetails.setHasAboutUs(true);
				dealerOverviewDetails.setDealerDescription(dealer.getDealerLongDescription());
				dealerOverviewDetails.setMemberSince(CICSUtility.formatMemberSinceDate(dealer.getMemberSince()));
			}	
		}
		
		return dealerOverviewDetails;	
	}


	public DealerCommonUtilityMapper getDealerCommonUtilityMapper() {
		return dealerCommonUtilityMapper;
	}


	public void setDealerCommonUtilityMapper(DealerCommonUtilityMapper dealerCommonUtilityMapper) {
		this.dealerCommonUtilityMapper = dealerCommonUtilityMapper;
	}


	public DealerReviewUtilityMapper getDealerReviewUtilityMapper() {
		return dealerReviewUtilityMapper;
	}


	public void setDealerReviewUtilityMapper(DealerReviewUtilityMapper dealerReviewUtilityMapper) {
		this.dealerReviewUtilityMapper = dealerReviewUtilityMapper;
	}
	
	
}
