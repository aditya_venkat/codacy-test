package com.cars.df.customerinquiry.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.cars.mdis.client.impl.MasterDataInquiryServiceClientImpl;
import com.cars.mdis.common.bo.Attribute;
import com.cars.mdis.common.bo.ProductResponse;

public class UtilityDAOTest {

	MasterDataInquiryServiceClientImpl mdisClient;
	UtilityDAO utilityDAO;
	RestTemplate restTemplate;

	@Before
	public void setUp() {
		restTemplate = PowerMockito.mock(RestTemplate.class);
		utilityDAO = new UtilityDAO();

		utilityDAO.setMdisHostName("http://services-dq01.cars.com");
		utilityDAO.setMdisContext("/MDIService/1.0");

		utilityDAO.setRestTemplate(restTemplate);
	}

	@Test
	public void testGetMakeIdFromMakeName() {
		try {
			String url = utilityDAO.getMdisHostName()
					+ utilityDAO.getMdisContext()
					+ "/rest/translate-canonical/ford?attribute=CarsMakeID";
			PowerMockito.when(restTemplate, "getForEntity", url,
					ProductResponse.class).thenReturn(createMakeInfo());
			String makeName = utilityDAO.getMakeIdFromMakeName("ford");

			assertNotNull(
					"In testGetMakeIdFromMakeName, makename was null when a call was made to MDIS client.",
					makeName);
			assertTrue(
					"In testGetMakeIdFromMakeName, makeName is not same as expected",
					makeName.equals("20015"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Test testGetMakeIdFromMakeName Dao test failed due to exception");
		}
	}

	private ResponseEntity<ProductResponse> createMakeInfo() {

		ProductResponse response = new ProductResponse();

		Attribute attribute = new Attribute();

		attribute.setAttributeId("CarsMakeID");
		attribute.setTitle("CarsMakeID");
		attribute.setValue("20015");

		List<Attribute> attributes = new ArrayList<Attribute>();
		attributes.add(attribute);

		response.setAttributes(attributes);
		response.setProductId("CarsMake_20000015");

		ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<ProductResponse>(
				response, HttpStatus.OK);
		return responseEntity;

	}
}
