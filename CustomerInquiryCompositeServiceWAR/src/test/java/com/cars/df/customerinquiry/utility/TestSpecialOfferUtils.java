	package com.cars.df.customerinquiry.utility;

import java.util.GregorianCalendar;


import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.cars.df.customerinquiry.bo.DealerLocalOfferCash;
import com.cars.df.customerinquiry.bo.DealerSpecialOfferDetails;
import com.cars.df.customerinquiry.bo.YearMakeModelOffer;
import com.cars.ss.lois.bo.generated.LocalOffer;
import com.cars.ss.lois.bo.generated.LocalOffers;
import com.cars.ss.lois.bo.generated.OfferAddOn;
import com.cars.ss.lois.bo.generated.OfferCash;
import com.cars.ss.lois.bo.generated.OfferDealerPromo;
import com.cars.ss.lois.bo.generated.OfferFinance;
import com.cars.ss.lois.bo.generated.OfferLease;
import com.cars.ss.lois.bo.generated.OfferService;
import com.cars.ss.lois.bo.generated.OfferTarget;
import com.cars.ss.lois.bo.generated.OfferType;

public class TestSpecialOfferUtils {

	public static LocalOffers createLocalOffers(){
		LocalOffers localOffers = new LocalOffers();
		
		LocalOffer localOfferAddOn = createLocalOffer();
		OfferType offerTypeAddOn = new OfferType();
		offerTypeAddOn.setCode("ADD_ON");
		offerTypeAddOn.setDescription("ADD_ON");
		localOfferAddOn.setType(offerTypeAddOn);
		
		LocalOffer localOfferService = createLocalOffer();
		OfferType offerTypeService = new OfferType();
		offerTypeService.setCode("SERVICE");
		offerTypeService.setDescription("SERVICE");
		localOfferService.setType(offerTypeService);
		
		
		LocalOffer localOfferFinance = createLocalOffer();
		OfferType offerTypeFinance = new OfferType();
		offerTypeFinance.setCode("FINANCE");
		offerTypeFinance.setDescription("FINANCE");
		localOfferFinance.setType(offerTypeFinance);
		
		LocalOffer localOfferLease = createLocalOffer();
		OfferType offerTypeLease = new OfferType();
		offerTypeLease.setCode("LEASE");
		offerTypeLease.setDescription("LEASE");
		localOfferLease.setType(offerTypeLease);
		
		LocalOffer localOfferCash = createLocalOffer();
		OfferType offerTypeCash = new OfferType();
		offerTypeCash.setCode("CASH");
		offerTypeCash.setDescription("CASH");
		localOfferCash.setType(offerTypeCash);
		
		LocalOffer localOfferPromo = createLocalOffer();
		OfferType offerTypePromo = new OfferType();
		offerTypePromo.setCode("DLR_PROMO");
		offerTypePromo.setDescription("DLR_PROMO");
		localOfferPromo.setType(offerTypePromo);
		
		
		localOffers.getLocalOffer().add(localOfferAddOn);
		localOffers.getLocalOffer().add(localOfferService);
		localOffers.getLocalOffer().add(localOfferFinance);
		localOffers.getLocalOffer().add(localOfferLease);
		localOffers.getLocalOffer().add(localOfferCash);
		localOffers.getLocalOffer().add(localOfferPromo);
		return localOffers;
	}
	
	public static LocalOffer createLocalOffer(){
		
		LocalOffer localOffer = new LocalOffer();
		
		localOffer.setOfferId(12345L);
		localOffer.setDealerPartyId(416546L);
		localOffer.setProductId(56789L);
		localOffer.setStockType("STK");
		localOffer.setVehicleYearId(1234L);
		localOffer.setVehicleYearDescription("2016");
		localOffer.setVehicleMakeId(20015L);
		localOffer.setVehicleMakeDescription("Toyota");
		localOffer.setVehicleModelId(34634L);
		localOffer.setVehicleModelDescription("Some");
		localOffer.setVehicleTrimId(12412L);
		localOffer.setVehicleTrimDescription("TRIM");
		localOffer.setVehicleBodyStyleId(124214L);
		localOffer.setVehicleBodyStyleIdDescription("VDesc");
		localOffer.setStockNum("123");
		localOffer.setOfferDisclaimer("Please make sure");
		
		GregorianCalendar gcal = new GregorianCalendar();
	      try {
			XMLGregorianCalendar xgcal = DatatypeFactory.newInstance()
			        .newXMLGregorianCalendar(gcal);
			localOffer.setStartDate(xgcal);
			localOffer.setEndDate(xgcal);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		localOffer.setDisplayInd(true);
		localOffer.setOemInd(true);
		localOffer.setCertifiedInd(true);
		
		
		
		/*OfferType offerType = new OfferType();
		offerType.setCode("SERVICE");
		offerType.setDescription("Service");
		localOffer.setType(offerType);*/

		OfferTarget offerTarget = new OfferTarget();
		
		offerTarget.setCode("TARGET");
		offerTarget.setDescription("Target Description");
		offerTarget.setRank(2);
		localOffer.setTarget(offerTarget);
		
		OfferLease offerLease  =new OfferLease();
		offerLease.setDueAtSigning(200.00);
		offerLease.setLeaseId(123L);
		offerLease.setMonthlyPayment(123.42);
		offerLease.setMonthlyTerm(123.23);
		offerLease.setOrdinal(12L);
		localOffer.getLease().add(offerLease);
		
		//?? do we need this??
		//localOffer.getMedia().add(e)
		
		OfferFinance offerFinance = new OfferFinance();
		offerFinance.setApr(12.99);
		offerFinance.setBonusCash(21312.212);
		offerFinance.setFinanceId(1234L);
		offerFinance.setOrdinal(12L);
		offerFinance.setTerm(12.22);
		localOffer.getFinancing().add(offerFinance);
		
		
		OfferCash offerCash = new OfferCash();
		offerCash.setAmount(123.23);
		offerCash.setCashId(123L);
		offerCash.setCashLabel("Cash Label");
		offerCash.setOrdinal(12L);
		localOffer.setCash(offerCash);
		
		OfferService offerService = new OfferService();
		offerService.setDescription("Service");
		offerService.setOrdinal(12L);
		offerService.setServiceId(123L);
		offerService.setTitle("Service please");
		localOffer.setService(offerService);
		
		
		OfferAddOn offerAddOn = new OfferAddOn();
		offerAddOn.setAddOnId(12L);
		offerAddOn.setDescription("ADd on");
		offerAddOn.setOrdinal(12L);
		offerAddOn.setTitle("This is Add On");
		localOffer.setAddOn(offerAddOn);
		
		OfferDealerPromo offerDealerPromo = new OfferDealerPromo();
		offerDealerPromo.setDescription("Promo Description");
		offerDealerPromo.setOrdinal(12L);
		offerDealerPromo.setPromoId(124L);
		offerDealerPromo.setTitle("Promo Offer");
		
		return localOffer;
	}
	

	public static DealerSpecialOfferDetails createDealerSpecialOfferDetails(){
		DealerSpecialOfferDetails dealerSpecialOfferDetails = new DealerSpecialOfferDetails();
		
		
		dealerSpecialOfferDetails.setPartyId("56482");
		dealerSpecialOfferDetails.setName("Lennys Depressing Auto Barn");
		dealerSpecialOfferDetails.setAddressLine1("1798 Pebble Beach Dr");
		dealerSpecialOfferDetails.setAddressLine2("");
		dealerSpecialOfferDetails.setCity("Hoffman Estates");
		dealerSpecialOfferDetails.setState("IL");
		dealerSpecialOfferDetails.setRating("0.0");
		dealerSpecialOfferDetails.setTotalReviewsNum("2");
		
		YearMakeModelOffer yearMakeModelOffer = new YearMakeModelOffer();
		
		yearMakeModelOffer.setName("2016 BMW i3");
		yearMakeModelOffer.setCount(2);
		
		yearMakeModelOffer.addCashOffer(createDealerLocalOfferCash());
		
		return dealerSpecialOfferDetails;
	}
	
	private static DealerLocalOfferCash createDealerLocalOfferCash(){
		DealerLocalOfferCash dealerLocalOfferCash = new DealerLocalOfferCash();
		
		dealerLocalOfferCash.setCashbackAmount(124.345);
		dealerLocalOfferCash.setCashLabel("CashBack");
		return dealerLocalOfferCash;
	}
}
