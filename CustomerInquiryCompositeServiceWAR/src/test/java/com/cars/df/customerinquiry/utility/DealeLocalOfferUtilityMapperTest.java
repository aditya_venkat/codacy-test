package com.cars.df.customerinquiry.utility;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerSpecialOfferDetails;
import com.cars.ss.lois.bo.generated.LocalOffers;

public class DealeLocalOfferUtilityMapperTest {

	private DealerLocalOfferUtilityMapper dealerLocalOfferUtilityMapper;
	private DealerCommonUtilityMapper dealerCommonUtilityMapper;
	private Dealers dealers;
	private LocalOffers localOffers;
	@Before
	public void setUp(){
		dealerCommonUtilityMapper = PowerMockito.mock(DealerCommonUtilityMapper.class);
		dealers = TestDealerUtils.createDealers();
		localOffers = TestSpecialOfferUtils.createLocalOffers();
		
		dealerLocalOfferUtilityMapper = new DealerLocalOfferUtilityMapper();
		dealerLocalOfferUtilityMapper.setDealerCommonUtilityMapper(dealerCommonUtilityMapper);
		
	}
	
	@Test
	public void testMapLocalOfferBOToYearMakeModeOfferBO(){
		
		try{
			DealerSpecialOfferDetails dealerSpecialOfferDetails = dealerLocalOfferUtilityMapper.mapLocalOfferBOToYearMakeModeOfferBO(dealers, localOffers);

			Assert.assertNotNull("In testMapLocalOfferBOToYearMakeModeOfferBO, dealerSpecialOfferDetails is null", dealerSpecialOfferDetails);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testMapLocalOfferBOToYearMakeModeOfferBO failed due to Exception");
		}
	}
}
