package com.cars.df.customerinquiry.utility;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerSearchResults;


public class DealerSummaryMapperTest {

	
	private DealerSummaryMapper dealerSummaryMapper;
	
	private Dealers dealers;
	
	@Before
	public void setUp(){
		dealerSummaryMapper = new DealerSummaryMapper();
		
		dealers = TestDealerUtils.createDealers();
	}
	
	@Test
	public void testMapDealerToDealerSummary(){
		
		DealerSearchResults dealerSearchResults = dealerSummaryMapper.mapDealerToDealerSummary(dealers);
		
		Assert.assertNotNull("In testMapDealerToDealerSummary, dealerSearchResults was null", dealerSearchResults);
		Assert.assertTrue("In testMapDealerToDealerSummary, dealerSearchResults has no dealers", dealerSearchResults.getDealerSummaryList().size() == 1);
	}
}
