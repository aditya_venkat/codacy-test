package com.cars.df.customerinquiry.utility;

import com.cars.df.customerinquiry.bo.DealerServiceRepairDetails;
import com.cars.rs.client.api.Amenities;
import com.cars.rs.client.api.Amenity;
import com.cars.rs.client.api.CommonService;
import com.cars.rs.client.api.CommonServices;
import com.cars.rs.client.api.FastFact;
import com.cars.rs.client.api.FastFacts;
import com.cars.rs.client.api.ServiceCustomer;
import com.cars.rs.client.api.ServiceWarranty;

public class TestServiceRepairUtils {

	public static ServiceCustomer createServiceCustomer(){
		
		ServiceCustomer serviceCustomer = new ServiceCustomer();
		
		
		Amenity amenity = new Amenity();
		amenity.setAmenityCategory("Category");
		amenity.setAmenityCd("CTG");
		amenity.setAmenityDescription("DSCRP");
		amenity.setAmenityFreeFormText("Good");
		
		Amenities amenities = new Amenities();
		amenities.getAmenity().add(amenity);
		serviceCustomer.setAmenities(amenities);
		
		FastFact fastFact = new FastFact();
		
		fastFact.setFastFactCd("FF");
		fastFact.setFastFactDescription("DSCRP");
		fastFact.setFastFactValue("VALUE");

		FastFacts fastFacts = new FastFacts();
		fastFacts.getFastFact().add(fastFact);
		serviceCustomer.setFastFacts(fastFacts);
		
		CommonService commonService = new CommonService();
		commonService.setCommonServiceCd("code");
		commonService.setCommonServiceDescription("DSCRP");
		commonService.setCommonServiceDetail("Detail");
		commonService.setCommonServiceDisclaimer("Disclaimer");
		commonService.setCommonServiceFreeIndicator("F");
		//commonService.setCommonServicePrice(1234);
		commonService.setCommonServicePriceType("PT");
		commonService.setCommonServiceProductDescription("PD");
		
		CommonServices commonServices = new CommonServices();
		commonServices.getCommonService().add(commonService);
		serviceCustomer.setCommonServices(commonServices);
		
		
		ServiceWarranty serviceWarranty = new ServiceWarranty();
		
		serviceWarranty.setServiceWarrantyDetails("WD");
		serviceWarranty.setServiceWarrantyDisclaimer("WD");
		serviceWarranty.setServiceWarrantyMiles("WM");
		serviceWarranty.setServiceWarrantyMonths(10);
		
		serviceCustomer.setServiceWarranty(serviceWarranty);
		
		
		serviceCustomer.setServiceAppointmentURL("Some URL");
		
		return serviceCustomer;
	}
	
	public static DealerServiceRepairDetails createDealerServiceRepairDetails(){
		
		DealerServiceRepairDetails DealerServiceRepairDetails = new DealerServiceRepairDetails();
		
		return DealerServiceRepairDetails;
	}
}
