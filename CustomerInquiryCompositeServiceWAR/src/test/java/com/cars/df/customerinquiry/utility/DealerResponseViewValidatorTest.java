package com.cars.df.customerinquiry.utility;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;

import com.cars.df.customerinquiry.bo.DealerResponseView;

public class DealerResponseViewValidatorTest {

	private DealerResponseView dealerNoResponseView;
	private DealerResponseView dealerResponseView;

	private DealerResponseViewValidator dealerResponseViewValidator;
	
	@Before
	public void setUp(){
		dealerNoResponseView = new DealerResponseView();
		dealerResponseView = TestReviewUtils.createDealerResponseView();
		dealerResponseViewValidator = new DealerResponseViewValidator();
	}
	
	@Test
	public void testSupports(){
		try{
			Boolean isDealerResponseView = dealerResponseViewValidator.supports(DealerResponseView.class);
			Assert.assertTrue("In testSupports, isDealerResponseView object was false",isDealerResponseView);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSupports test failed due to exception");
		}
	}
	
	@Test
	public void testNotSupports(){
		try{
			Boolean isDealerResponseView = dealerResponseViewValidator.supports(String.class);
			Assert.assertFalse("In testNotSupports, isDealerResponseView object was true",isDealerResponseView);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testNotSupports test failed due to exception");
		}
	}
	
	@Test
	public void testValidate(){
		try{
			BeanPropertyBindingResult bindingResults = new BeanPropertyBindingResult(dealerNoResponseView, "Errors");
			dealerResponseViewValidator.validate(dealerNoResponseView, bindingResults);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testValidate test failed due to exception");
		}
	}
	
	@Test
	public void testValidateWithData(){
		try{
			BeanPropertyBindingResult bindingResults = new BeanPropertyBindingResult(dealerResponseView, "Errors");
			dealerResponseViewValidator.validate(dealerResponseView, bindingResults);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testValidateWithData test failed due to exception");
		}
	} 
}
