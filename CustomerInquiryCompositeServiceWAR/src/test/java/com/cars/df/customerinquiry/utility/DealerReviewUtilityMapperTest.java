package com.cars.df.customerinquiry.utility;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerResponseView;
import com.cars.df.customerinquiry.bo.DealerReviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewView;


public class DealerReviewUtilityMapperTest {

	private DealerReviewUtilityMapper dealerReviewUtilityMapper;
	private DealerCommonUtilityMapper dealerCommonUtilityMapper;
	private Dealers dealers;
	private DealerReview dealerReview;
	private DealerReviewView dealerReviewView;
	private DealerResponseView dealerResponseView;
	
	@Before
	public void setUp(){
		dealerCommonUtilityMapper = PowerMockito.mock(DealerCommonUtilityMapper.class);
		dealers = TestDealerUtils.createDealers();
		dealerReview = TestReviewUtils.createDealerReview();
		dealerReviewView = TestReviewUtils.createDealerReviewView();
		dealerResponseView = TestReviewUtils.createDealerResponseView();
		
		dealerReviewUtilityMapper = new DealerReviewUtilityMapper();
		dealerReviewUtilityMapper.setDealerCommonUtilityMapper(dealerCommonUtilityMapper);
		
	};
	
	@Test
	public void testCreateDealerReviewDetails(){
		
		try{
			
			DealerReviewDetails DealerReviewDetails = dealerReviewUtilityMapper.createDealerReviewDetails(dealers, dealerReview);
			
			Assert.assertNotNull("In testCreateDealerReviewDetails, DealerReviewDetails is null", DealerReviewDetails);
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testCreateDealerReviewDetails failed due to exception");
		}
	}
	
	@Test
	public void testGenerateDealerReview(){
		
		try{
			DealerReview dealerReview = dealerReviewUtilityMapper.generateDealerReview(dealerReviewView, "416546");
			Assert.assertNotNull("In testGenerateDealerReview, dealerReview is null", dealerReview);
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGenerateDealerReview failed due to exception");

		}
		
	}
	
	@Test
	public void testGenerateDealerReviewResponse(){
		
		try{
			DealerReview dealerReview = dealerReviewUtilityMapper.generateDealerReviewResponse("416546", "214124", dealerResponseView);
			Assert.assertNotNull("In testGenerateDealerReviewResponse, dealerReview is null", dealerReview);
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGenerateDealerReviewResponse failed due to exception");

		}
		
	}
	
	
}
