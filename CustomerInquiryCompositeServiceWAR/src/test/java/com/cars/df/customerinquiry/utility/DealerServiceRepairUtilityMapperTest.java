package com.cars.df.customerinquiry.utility;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerServiceRepairDetails;
import com.cars.rs.client.api.ServiceCustomer;

public class DealerServiceRepairUtilityMapperTest {

	private DealerServiceRepairUtilityMapper dealerServiceRepairUtilityMapper;
	private DealerCommonUtilityMapper dealerCommonUtilityMapper;
	private Dealers dealers;
	private ServiceCustomer serviceCustomer;
	
	@Before
	public void setUp(){
		
		dealerCommonUtilityMapper = PowerMockito.mock(DealerCommonUtilityMapper.class);
		dealers = TestDealerUtils.createDealers();
		serviceCustomer = TestServiceRepairUtils.createServiceCustomer();
		
		
		dealerServiceRepairUtilityMapper = new DealerServiceRepairUtilityMapper();
		dealerServiceRepairUtilityMapper.setDealerCommonUtilityMapper(dealerCommonUtilityMapper);
	}
	
	@Test
	public void testConvertServiceCustomerToServiceRepairDetails(){
		
		try{
			DealerServiceRepairDetails dealerServiceRepairDetails = dealerServiceRepairUtilityMapper.convertServiceCustomerToServiceRepairDetails(dealers, serviceCustomer);
			Assert.assertNotNull("In testCreateDealerDetailSummary, dealerServiceRepairDetails is null", dealerServiceRepairDetails);
		
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testConvertServiceCustomerToServiceRepairDetails failed due to Exception");
		}
	}
}
