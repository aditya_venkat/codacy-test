package com.cars.df.customerinquiry.utility;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cars.df.customerinquiry.utility.CICSUtility;


public class CICSUtilityTest {

	@Before
	public void setUp(){
	}
	
	
	@Test
	public void testFormatPhoneNumber(){
		String phoneNumber = "1234567890";
		String formattedNumber="123-456-7890";
		
		Assert.assertTrue(CICSUtility.formatPhoneNumber(phoneNumber).equals(formattedNumber));
	}
	
	@Test
	public void testIsLong(){
	    String numberString = "1234126";
	    
	    Assert.assertTrue(CICSUtility.isLong(numberString));
	}
	
	@Test
	public void testIsLongInvalid(){
	    String numberString = "1234126fee";
	    
	    Assert.assertFalse(CICSUtility.isLong(numberString));
	}
	
	@Test
	public void testConvertDoubleToSingleDecimal(){
		String doubleString = "4.700000";
		String formatedDoubleString = "4.7";
	    Assert.assertTrue(CICSUtility.convertDoubleToSingleDecimal(doubleString).equals(formatedDoubleString));

	}
	
	@Test
	public void testIsDouble(){
		String doubleString = "4.700000";
		 Assert.assertTrue(CICSUtility.isDouble(doubleString));
	}
	
	
	@Test
	public void testIsInvalidDouble(){
		String doubleString = "4.700ad0";
		 Assert.assertFalse(CICSUtility.isDouble(doubleString));
	}
	
	@Test
	public void testWorkingHours(){
		String workingHoursString = "MON 7:00 AM - 7:00 PM@TUE 7:00 AM - 7:00 PM@WED 7:00 AM - 7:00 PM@THU 7:00 AM - 7:00 PM@FRI 7:00 AM - 7:00 PM@SAT 7:00 AM - 7:00 PM@SUN - CLOSED@";
		
		Map<String, String> workingHours = CICSUtility.convertWorkingHoursToMap(workingHoursString);
		
		Assert.assertTrue(workingHours.size() == 7);
	}
	
	@Test
	public void testInvalidWorkingHours(){
		String workingInvalidHoursString = "7:00 AM - 7:00 PM@TUE 7:00 AM - 7:00 PM@7:00 AM - 7:00 PM@THU 7:00 AM - 7:00 PM@FRI 7:00 AM - 7:00 PM@SAT 7:00 AM - 7:00 PM@SUN - CLOSED@";
		
		Map<String, String> workingHours = CICSUtility.convertWorkingHoursToMap(workingInvalidHoursString);
		
		Assert.assertFalse(workingHours.size() == 7);
	}
	
	@Test
	public void testNullWorkingHours(){
		String workingInvalidHoursString = null;
		Map<String, String> workingHours = CICSUtility.convertWorkingHoursToMap(workingInvalidHoursString);
		
		Assert.assertTrue(workingHours.size() == 0);
	}
	
	@Test
	public void testConvertPhotoStringToURL(){
		String photoString = "/dealerphotos/102602/1432844567213.jpg@/dealerphotos/102602/1432855232260.jpg@/dealerphotos/102602/1432844831205.jpg@/dealerphotos/102602/1401313613533.jpg@/dealerphotos/102602/1432844712551.jpg@/dealerphotos/102602/1401308129760.jpg@/dealerphotos/102602/1401313292488.jpg@/dealerphotos/102602/1401308130078.jpg@/dealerphotos/102602/1401308802927.jpg";
		
		String[] photos = CICSUtility.convertPhotoStringToURL(photoString);
		
		Assert.assertTrue(photos.length == 9);
	}
	
	@Test
	public void testInvalidConvertPhotoStringToURL(){
		String photoString = "";
		
		String[] photos = CICSUtility.convertPhotoStringToURL(photoString);
		
		Assert.assertNull(photos);
	}
	
	@Test
	public void testFormatInvalidMemberSinceDate(){
		String date = "2016-03-07";
		
		String memberSince = CICSUtility.formatMemberSinceDate(date);
		
		Assert.assertNotNull(memberSince);
		Assert.assertTrue("".equals(memberSince));
	}
	
	@Test
	public void testFormatMemberSinceDate(){
		String date = "2016-03-07 10:10:10.10";
		
		String memberSince = CICSUtility.formatMemberSinceDate(date);
		
		Assert.assertNotNull(memberSince);
		Assert.assertTrue(memberSince.contains("March"));
	}
	
	@Test
	public void testInvalidFormatMemberSinceDate(){
		String date = "abcd";
		
		String memberSince = CICSUtility.formatMemberSinceDate(date);
		
		Assert.assertNotNull(memberSince);
		Assert.assertFalse(memberSince.contains("March"));
	}
	
	@Test
	public void testConvertStringToDouble(){
		String doubleString = "10.9";
		
		Double doubleValue = CICSUtility.convertStringToDouble(doubleString);
		
		Assert.assertNotNull(doubleValue);
		Assert.assertTrue(doubleValue == 10.9);
	}
	
	@Test
	public void testInvalidConvertStringToDouble(){
		String doubleString = "abcd";
		
		Double doubleValue = CICSUtility.convertStringToDouble(doubleString);
		
		Assert.assertNotNull(doubleValue);
		Assert.assertTrue(doubleValue == 0.0);
	}
	
	@Test
	public void testformatOfferAndModelCount(){
		String formatOfferAndModelCountString = "10,10";
		
		String specialOfferString = CICSUtility.formatOfferAndModelCount(formatOfferAndModelCountString);
		
		Assert.assertNotNull(specialOfferString);
		Assert.assertTrue("10 Offers on 10 Models".equals(specialOfferString));
	}
	@Test
	public void testformatOffersWithNoModel(){
		String formatOfferAndModelCountString = "10,0";
		
		String specialOfferString = CICSUtility.formatOfferAndModelCount(formatOfferAndModelCountString);
		
		Assert.assertNotNull(specialOfferString);
		Assert.assertTrue("10 Offers Available".equals(specialOfferString));
	}
	@Test
	public void testWithNoOfferAndModel(){
		String formatOfferAndModelCountString = "0,0";
		
		String specialOfferString = CICSUtility.formatOfferAndModelCount(formatOfferAndModelCountString);
		
		Assert.assertNotNull(specialOfferString);
		Assert.assertTrue("".equals(specialOfferString));
	}
	
	@Test
	public void testAddURLtoLogoFile(){
		String fileName = "aditya.jpg";
		
		String logoUrl = CICSUtility.addURLtoLogoFile(fileName);
		
		Assert.assertNotNull(logoUrl);
		Assert.assertTrue("http://www.cstatic-images.com/logo-dealer/88-pixel/aditya.jpg".equals(logoUrl));
	}
	
	@Test
	public void testAddURLtoNullLogoFile(){
		String fileName = null;
		
		String logoUrl = CICSUtility.addURLtoLogoFile(fileName);
		
		Assert.assertNull(logoUrl);
		Assert.assertFalse("http://www.cstatic-images.com/logo-dealer/88-pixel/aditya.jpg".equals(logoUrl));
	}
	
	@Test
	public void testConvertStringToLong(){
		String longValue = "12345";
		
		Long value = CICSUtility.convertStringToLong(longValue);
		
		Assert.assertNotNull(value);
		Assert.assertTrue(value == 12345);
	}
	
	@Test
	public void testConvertInvalidStringToLong(){
		String longValue = null;
		
		Long value = CICSUtility.convertStringToLong(longValue);
		
		Assert.assertNotNull(value);
		Assert.assertTrue(value == Long.valueOf(0L));
	}
}
