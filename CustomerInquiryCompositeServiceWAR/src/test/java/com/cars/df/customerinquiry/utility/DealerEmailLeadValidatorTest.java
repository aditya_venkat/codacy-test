package com.cars.df.customerinquiry.utility;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;

import com.cars.df.customerinquiry.bo.DealerEmailLead;

public class DealerEmailLeadValidatorTest {

	private DealerEmailLead dealerEmailLead;
	private DealerEmailLead dealerNoEmailLead;
	private DealerEmailLeadValidator dealerEmailLeadValidator;
	 
	@Before
	public void setUp(){
		dealerNoEmailLead = new DealerEmailLead();
		dealerEmailLead = this.createDealerEmailLead();
		dealerEmailLeadValidator = new DealerEmailLeadValidator();
	}
	
	@Test
	public void testSupports(){
		try{
			Boolean isEmailLead = dealerEmailLeadValidator.supports(DealerEmailLead.class);
			Assert.assertTrue("In testSupports, isEmailLead object was false",isEmailLead);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSupports test failed due to exception");
		}
	}
	
	@Test
	public void testNotSupports(){
		try{
			Boolean isEmailLead = dealerEmailLeadValidator.supports(String.class);
			Assert.assertFalse("In testNotSupports, isEmailLead object was true",isEmailLead);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testNotSupports test failed due to exception");
		}
	}
	
	@Test
	public void testValidate(){
		try{
			BeanPropertyBindingResult bindingResults = new BeanPropertyBindingResult(dealerNoEmailLead, "Errors");
			dealerEmailLeadValidator.validate(dealerNoEmailLead, bindingResults);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testValidate test failed due to exception");
		}
	}
	
	@Test
	public void testValidateWithData(){
		try{
			BeanPropertyBindingResult bindingResults = new BeanPropertyBindingResult(dealerEmailLead, "Errors");
			dealerEmailLeadValidator.validate(dealerEmailLead, bindingResults);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testValidateWithData test failed due to exception");
		}
	}
	
	private DealerEmailLead createDealerEmailLead(){
		
		DealerEmailLead dealerEmailLead = new DealerEmailLead();
		
		dealerEmailLead.setComments("Need a new Car");
		dealerEmailLead.setDaytimeNumber("3124568907");
		dealerEmailLead.setDealerEmail("Aditya@Aditya.com");
		dealerEmailLead.setDealerName("Aditya");
		dealerEmailLead.setDealerNumber("5167653489");
		dealerEmailLead.setEmail("Jen@Jen.com");
		dealerEmailLead.setEveningNumber("31250360889");
		dealerEmailLead.setFirstName("Jen");
		dealerEmailLead.setIsNew("true");
		dealerEmailLead.setJscCollector("asfasfafasfasfasfasfsa");
		dealerEmailLead.setLastName("Jen");
		dealerEmailLead.setLeadType("NEW_INV");
		dealerEmailLead.setOdsId("416546");
		dealerEmailLead.setPhone("3125467836");
		dealerEmailLead.setPhonetype(new String[]{"day", "evevnig"});
		dealerEmailLead.setWebPageTypeId("2210");
		dealerEmailLead.setZipCode("60601");
		
		return dealerEmailLead;
		
		
	}
}
