package com.cars.df.customerinquiry.client;

import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;

import com.cars.rs.client.api.ServiceCustomer;
import com.cars.ss.lois.bo.generated.LocalOffers;


public class ServiceClientsTest {

	private ServiceClients serviceClients;
	private RestClient client;
	
	@Before
	public void setUp(){
		client = PowerMockito.mock(RestClient.class);
		
		serviceClients = new ServiceClients();
		serviceClients.setClient(client);
		
	}
	
	//TODO: I have no clue how to mock client and get Resousce because Resource is an Interface. Need to read more about it
	@Test
	public void testGetOfferAndModelCount(){
		
		
		try{
			
			//PowerMockito.when(client, "resource", "").thenReturn(Resource.class);
			
			String offerAndModelCount = serviceClients.getOfferAndModelCount("416546");
			
			//Assert.asserNot
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}
	
	@Test
	public void testGetLocalOffersByDealerId(){
		try{
			
			//PowerMockito.when(client, "resource", "").thenReturn(Resource.class);
			
			LocalOffers localOffers  = serviceClients.getLocalOffersByDealerId("416546");
			
			//Assert.asserNot
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}
	
	@Test
	public void testGetServiceRepairDetails(){
		try{
			
			//PowerMockito.when(client, "resource", "").thenReturn(Resource.class);
			
			ServiceCustomer serviceCustomer = serviceClients.getServiceRepairDetails("102602");
			
			//Assert.asserNot
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}
}
