/**
 * 
 */
package com.cars.df.customerinquiry.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;

import com.cars.df.customerinquiry.bo.DealerEmailLead;
import com.cars.df.customerinquiry.bo.DealerReviewView;
import com.cars.df.customerinquiry.service.CustomerSearchService;
import com.cars.df.customerinquiry.service.LeadpathService;
import com.cars.df.customerinquiry.utility.DealerEmailLeadValidator;

/**
 * @author lgiovenco
 *
 */
public class CustomerLeadControllerTest {
	
	private CustomerLeadController customerLeadController; 
	private LeadpathService leadpathService; 
	private DealerEmailLeadValidator dealerEmailLeadValidator;
	
	@Before
	public void setup() {
		leadpathService = PowerMockito.mock(LeadpathService.class);
		dealerEmailLeadValidator = new DealerEmailLeadValidator();
		
		customerLeadController = new CustomerLeadController(); 
		customerLeadController.setLeadpathService(leadpathService);
		customerLeadController.setDealerEmailLeadValidator(dealerEmailLeadValidator);
	}
	
	@Test
	public void testSendDealerEmail(){
		
		try{
			PowerMockito.when(leadpathService, "sendDealerEmail", Matchers.any(DealerEmailLead.class), Matchers.isNull(), Matchers.any(String.class), Matchers.any(String.class)).thenReturn(true);

			ResponseEntity<StringBuffer> status = customerLeadController.handleEmailLead(createValidEmailLead(), "2111", "affiliate", null);
		
			Assert.assertTrue("In testSendDealerEmail, Lead not sent successfully", status.getBody().toString().equals("SAVE_SUCCESSFUL"));

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSendDealerEmail test failed due to exception");
		}
	}
	
	private DealerEmailLead createValidEmailLead() {
		
		DealerEmailLead dealerEmailLead = new DealerEmailLead(); 
		
		dealerEmailLead.setComments("Hi This is a test");
		dealerEmailLead.setDaytimeNumber("3128823517");
		dealerEmailLead.setDealerEmail("Aditya@cars.com");
		dealerEmailLead.setDealerName("Aditya's Super Cars");
		dealerEmailLead.setDealerNumber("3128823517");
		dealerEmailLead.setEmail("Aditya@aditya.com");
		dealerEmailLead.setEveningNumber("3128823517");
		dealerEmailLead.setFirstName("Aditya");
		dealerEmailLead.setIsNew("false");
		dealerEmailLead.setJscCollector("13alkdsjli);asjd ailsjdila");
		dealerEmailLead.setLastName("SuperVen");
		dealerEmailLead.setZipCode("60606");
		dealerEmailLead.setWebPageTypeId("2224");
		dealerEmailLead.setOdsId("572845");
		
		return dealerEmailLead; 
	}

}
