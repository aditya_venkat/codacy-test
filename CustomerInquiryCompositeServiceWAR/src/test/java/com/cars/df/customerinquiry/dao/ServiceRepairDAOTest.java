package com.cars.df.customerinquiry.dao;

import com.cars.df.customerinquiry.client.ServiceClients;
import com.cars.df.customerinquiry.utility.TestServiceRepairUtils;
import com.cars.rs.client.api.ServiceCustomer;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;

public class ServiceRepairDAOTest {

	private ServiceClients serviceClients;
	
	private ServiceRepairDAO serviceRepairDAO;
	
	private ServiceCustomer serviceCustomer;
	
	@Before
	public void setUp(){
		serviceClients = PowerMockito.mock(ServiceClients.class);
		
		serviceCustomer = TestServiceRepairUtils.createServiceCustomer();
		serviceRepairDAO = new ServiceRepairDAO();
		serviceRepairDAO.setServiceClients(serviceClients);
	}
	
	@Test
	public void testGetServiceRepairDetails(){
		try{
			PowerMockito.when(serviceClients, "getServiceRepairDetails", Matchers.any(String.class)).thenReturn(serviceCustomer);
			
			ServiceCustomer serviceCustomer = serviceRepairDAO.getServiceRepairDetails("102602");
			
			assertNotNull("In testGetServiceRepairDetails, serviceCustomer is null", serviceCustomer);
			//assertTrue("In testGetOfferAndModelCount, returnedOfferAndModelCount is not true",  "120,120".equals(returnedOfferAndModelCount));
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetServiceRepairDetails Dao test failed due to exception");
		}
	}

}
