package com.cars.df.customerinquiry.dao;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.DealerSearchCriteriaBO;
import com.cars.df.css.bo.Dealers;
import com.cars.df.css.client.CustomerSearchServiceClient;

public class CustomerSearchDAOTest {

	CustomerSearchServiceClient cssClient;
	Dealers singleDealer; 
	DealerSearchCriteriaBO searchCriteriaBO;
	CustomerSearchDAO customerSearchDAO; 
	
	@Before
	public void setup() {
		cssClient = PowerMockito.mock(CustomerSearchServiceClient.class); 
		singleDealer = createDealers(); 
		customerSearchDAO = new CustomerSearchDAO(); 
		customerSearchDAO.setCssClient(cssClient);
	}
	
	@Test
	public void testSearchDealers() {
		try {
			PowerMockito.when(cssClient, "getDealers", searchCriteriaBO).thenReturn(singleDealer);
			Dealers response = customerSearchDAO.searchDealers(searchCriteriaBO);
			
			assertNotNull("In testSearchDealers, response was null when criteria is sent  to client.", response);
			assertTrue("In testSearchDealers, single dealer was not returned when criteria is sent to client.", response.getDealers().size() == 1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Test testSearchDealers test failed due to exception");
		}
	}
	
	@Test
	public void testGetDealer(){
		try {
			PowerMockito.when(cssClient, "getDealer", "56482").thenReturn(singleDealer);
			Dealers response = customerSearchDAO.getDealer("56482");
			
			assertNotNull("In testGetDealer, response was null when party id send to client.", response);
			assertTrue("In testGetDealer, single dealer was not returned when party id is send to client.", response.getDealers().size() == 1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Test testGetDealer  test failed due to exception");
		}
	}
	
	@Test
	public void testConvertCustomerIdToPartyId(){
		
		String partyId = "416546";
		String customerID = "102602";
		try {
			PowerMockito.when(cssClient, "convertLegacyDealerIdToOdsId", customerID).thenReturn(partyId);
			String response = customerSearchDAO.convertCustomerIdToPartyId(customerID);
			
			assertNotNull("In testConvertCustomerIdToPartyId, response was null when customer id is send to client.", response);
			assertTrue("In testConvertCustomerIdToPartyId, Customer id doesn't match with party id", "416546".equals(response));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Test testConvertCustomerIdToPartyId Dao test failed due to exception");
		}
	}
	
	@Test
	public void testConvertPartyIdToCustomerId(){
		
		String partyId = "416546";
		String customerID = "102602";
		
		try {
			PowerMockito.when(cssClient, "convertOdsIdToLegacyDealerId", partyId).thenReturn(customerID);
			String response = customerSearchDAO.convertPartyIdToCustomerId(partyId);
			
			assertNotNull("In testConvertPartyIdToCustomerId, response was null when party id is send to client.", response);
			assertTrue("In testConvertPartyIdToCustomerId, Customer id doesn't match with party id.", "102602".equals(response));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Test testSearchDealers Dao test failed due to exception");
		}
	}
	
	
	private Dealer createDealerOne() {
		Dealer dealer = new Dealer(); 
		
		dealer.setPartyId("56482");
		dealer.setName("Lennys Depressing Auto Barn");
		dealer.setAddressLine1("1798 Pebble Beach Dr");
		dealer.setAddressLine2("");
		dealer.setCity("Hoffman Estates");
		dealer.setState("IL");
		dealer.setZip("60169");
		dealer.setRating("0.0");
		dealer.setTotalReviewsNum("2");
		dealer.setRpcCertified(false);
		
		return dealer;
	}
	
	private Dealers createDealers(){
		
		Dealers dealers = new Dealers();
		
		dealers.addDealer(createDealerOne());
		
		return dealers;
		
	}
	
}
