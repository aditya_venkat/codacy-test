/**
 * 
 */
package com.cars.df.customerinquiry.utility;

import com.cars.df.customerinquiry.bo.DealerDetails;
import com.cars.df.customerinquiry.bo.DealerOverviewDetails;
import com.cars.df.customerinquiry.bo.DealerResponseView;
import com.cars.df.customerinquiry.bo.DealerReviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewSummaryView;
import com.cars.df.customerinquiry.bo.DealerReviewView;
import com.cars.df.customerinquiry.bo.DealerSearchResults;
import com.cars.df.customerinquiry.bo.DealerSummary;
import com.cars.df.customerinquiry.bo.DirectoryBreadCrumb;
import com.cars.df.customerinquiry.bo.DirectoryBreadCrumbValue;
import com.cars.df.customerinquiry.bo.DirectoryRefinement;
import com.cars.df.customerinquiry.bo.DirectoryRefinementValue;
import com.cars.df.customerinquiry.bo.Pagination;
import com.cars.df.customerinquiry.bo.Sort;
import com.cars.df.customerinquiry.bo.SortType;
import com.cars.service.listingsearch.bo.RefinementBO;
import com.cars.service.listingsearch.bo.RefinementValueBO;
import com.cars.service.listingsearch.bo.SearchResultBO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cars.api.schemas.dealer_review.Affiliate;
import com.cars.api.schemas.dealer_review.CategoryRate;
import com.cars.api.schemas.dealer_review.CategoryRateId;
import com.cars.api.schemas.dealer_review.CategoryRating;
import com.cars.api.schemas.dealer_review.CategoryType;
import com.cars.api.schemas.dealer_review.ConsumerDealerReview;
import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.api.schemas.dealer_review.DealerReviewResponse;
import com.cars.api.schemas.dealer_review.FeedbackResponse;
import com.cars.api.schemas.dealer_review.OverallCategoryRating;
import com.cars.api.schemas.dealer_review.OverallCategoryRatingId;
import com.cars.api.schemas.dealer_review.RejectReason;
import com.cars.api.schemas.dealer_review.ReturnCode;
import com.cars.api.schemas.dealer_review.Status;
import com.cars.api.schemas.dealer_review.VisitReason;
import com.cars.df.css.bo.BreadCrumb;
import com.cars.df.css.bo.BreadCrumbValue;
import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.css.bo.Refinement;
import com.cars.df.css.bo.RefinementValue;

/**
 * @author leonard and aditya
 *
 */
public class TestUtils {
	
	public static  DealerSearchResults createDealerSearchResults(){
	
		DealerSearchResults dealerSearchResults = new DealerSearchResults();
		
		dealerSearchResults.addDealerSummary(createDealerSumary());
		
		Pagination pagination = new Pagination(1,10, 1000);
				
		//setting sort
		SortType primarySort = new SortType("DISTANCE", "DSC");
				
		SortType secondarySort = new SortType("RATING", "DSC");
				
		SortType tertiarySort = new SortType("NAME", "DSC");
				
		Sort sort = new Sort(primarySort, secondarySort, tertiarySort);
				
		dealerSearchResults.setPagination(pagination);
				
		dealerSearchResults.setSort(sort);
		
		dealerSearchResults.setTotalDealers(1L);
		
		
		return dealerSearchResults;
		
		
		
	}
	
	public static DealerSummary createDealerSumary(){
		
		DealerSummary dealerSummary = new DealerSummary();
		
		dealerSummary.setName("Lenny's Auto Dealer");
		dealerSummary.setAddressLine1("123 South West North East");
		dealerSummary.setAddressLine2("East");
		dealerSummary.setCity("Chciago");
		dealerSummary.setState("IL");
		dealerSummary.setZip("60601");
		dealerSummary.setTotalReviewsNum("120");
		dealerSummary.setRating("1.1");
		dealerSummary.setCustomerId("123456");
		dealerSummary.setPartyId("78906");
		dealerSummary.setRpcCertified(false);
		dealerSummary.setLogoUrl("http://www.lenny.com/lenny.gif");
		dealerSummary.setPromoTagLine("We are not good dealers. Please don't visit our store");
		dealerSummary.setNewCarTollFreePhoneNumber("123-456-7890");
		dealerSummary.setUsedCarTollFreePhoneNumber("123-456-7890");
		dealerSummary.setServiceTollFreePhoneNumber("123-456-7890");
		
		return dealerSummary;
	}
	
	public static Dealers createSingleDealer(){
		
		Dealers dealers = new Dealers();
		
		dealers.addDealer(createDealerOne());
		dealers.setTotalDealers(1L);
		
		
		return dealers;
		
	}
	

	
	
	public static DealerDetails getDealerDetails(){
		
		DealerDetails dealerDetails = new DealerDetails();
		
		dealerDetails.setPartyId("56482");
		dealerDetails.setName("Lennys Depressing Auto Barn");
		dealerDetails.setAddressLine1("1798 Pebble Beach Dr");
		dealerDetails.setAddressLine2("");
		dealerDetails.setCity("Hoffman Estates");
		dealerDetails.setState("IL");
		dealerDetails.setRating("0.0");
		dealerDetails.setTotalReviewsNum("2");
		return dealerDetails;
	}
	
	
	public static Dealer createDealerOne() {
		Dealer dealer = new Dealer(); 
		
		dealer.setPartyId("56482");
		dealer.setName("Lennys Depressing Auto Barn");
		dealer.setAddressLine1("1798 Pebble Beach Dr");
		dealer.setAddressLine2("");
		dealer.setCity("Hoffman Estates");
		dealer.setState("IL");
		dealer.setZip("60169");
		dealer.setRating("0.0");
		dealer.setTotalReviewsNum("2");
		dealer.setRpcCertified(false);
		
		return dealer;
	}
	
	
	
	
	
	//START - Search Result BO (ISS returned Object) Mock
	
	public static SearchResultBO createSearchResultBO(){
		SearchResultBO searchResultBO = new SearchResultBO();
		
		RefinementBO refinementBO = new RefinementBO();
		
		List<RefinementValueBO> refinementValuesBOList = new ArrayList<RefinementValueBO>();
		refinementValuesBOList.add(createRefinementValueBO("New", 100));
		refinementValuesBOList.add(createRefinementValueBO("Used", 100));
		
		refinementBO.replaceRefinementValues(refinementValuesBOList);
		refinementBO.setName("stkTypId");
		
		searchResultBO.addRefinement(refinementBO);
		
		return searchResultBO;
	}
	
	private static RefinementValueBO createRefinementValueBO(String code, Integer value){
		
		RefinementValueBO refinementValueBO = new RefinementValueBO();
		
		refinementValueBO.setName(code);
		refinementValueBO.setCount(value);
		return refinementValueBO;
		
	}
	
	
	//END - Search Result BO (ISS returned Object) Mock
	
	
	public static DealerSummary createDealerSummary(){
		
		DealerSearchResults dealerSearchResults = new DealerSearchResults();
		DealerSummary dealerSummary = new DealerSummary();
		
		dealerSummary.setName("Aditya's Dealer");
		dealerSummary.setAddressLine1("Downtown");
		dealerSummary.setAddressLine2("Downtown Again");
		dealerSummary.setCity("Chicago");
		
		dealerSearchResults.addDealerSummary(dealerSummary);
		dealerSearchResults.addRefinement(createDirectoryRefinement());
		dealerSearchResults.addBreadCrumb(createDirectoryBreadCrumb());
		//dealerSummary.re
		
		return dealerSummary;
	}
	
	private static DirectoryBreadCrumb createDirectoryBreadCrumb(){
		
		DirectoryBreadCrumb directoryBreadCrumb = new DirectoryBreadCrumb();
		
		directoryBreadCrumb.setId("12412");
		directoryBreadCrumb.setName("mkId");
		directoryBreadCrumb.addBreadCrumbValue(createBreadCrumbValue("20018", "Toyota"));
		return directoryBreadCrumb;
	}
	private static DirectoryBreadCrumbValue createBreadCrumbValue(String id, String name){
		DirectoryBreadCrumbValue directoryBreadCrumbValue= new DirectoryBreadCrumbValue();

		directoryBreadCrumbValue.setId(id);
		directoryBreadCrumbValue.setName(name);
		return directoryBreadCrumbValue;
	}
	
	private static DirectoryRefinement createDirectoryRefinement(){
		
		DirectoryRefinement directoryRefinement = new DirectoryRefinement();
		
		directoryRefinement.setId("12412");
		directoryRefinement.setName("mkId");
		directoryRefinement.addRefinementValue(createRefinementValue("20018", "Toyota"));
		return directoryRefinement;
	}
	
	private static DirectoryRefinementValue createRefinementValue(String id, String name){
		DirectoryRefinementValue directoryRefinementValue= new DirectoryRefinementValue();

		directoryRefinementValue.setId(id);
		directoryRefinementValue.setName(name);
		return directoryRefinementValue;
	}
	
}
