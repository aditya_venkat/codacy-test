package com.cars.df.customerinquiry.utility;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cars.df.css.bo.Dealer;
import com.cars.df.customerinquiry.bo.DealerDetails;
public class DealerCommonUtilityMapperTest {

	private DealerCommonUtilityMapper dealerCommonUtilityMapper;
	private Dealer dealer;
	
	@Before
	public void setUp(){
		
		dealerCommonUtilityMapper  = new DealerCommonUtilityMapper(); 
		dealer = TestUtils.createDealerOne();
	}
	
	@Test
	public void testCreateDealerDetailSummary(){
	
		DealerDetails dealerDetails = new DealerDetails();
		
		dealerCommonUtilityMapper.createDealerDetailSummary(dealer, dealerDetails);
		
		Assert.assertNotNull("In testCreateDealerDetailSummary, dealerDetails is null", dealerDetails);
		Assert.assertTrue("In testCreateDealerDetailSummary, dealerDetails details don't match with dealer", "Hoffman Estates".equals(dealerDetails.getCity()));
	}
}
