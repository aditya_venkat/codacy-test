package com.cars.df.customerinquiry.dao;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;

import com.cars.df.customerinquiry.utility.TestUtils;
import com.cars.service.listingsearch.bo.SearchCriteriaBO;
import com.cars.service.listingsearch.bo.SearchResultBO;
import com.cars.ss.iss.client.InventorySearchServiceClient;


public class InventorySearchDAOTest {

	private InventorySearchServiceClient inventorySearchServiceClient;
	private InventorySearchDAO inventorySearchDAO;
	private SearchResultBO searchResultBO;
	
	@Before
	public void setUp(){
		inventorySearchServiceClient = PowerMockito.mock(InventorySearchServiceClient.class);
		searchResultBO = TestUtils.createSearchResultBO();
		
		inventorySearchDAO = new InventorySearchDAO();
		inventorySearchDAO.setInventorySearchServiceClient(inventorySearchServiceClient);
	}
	

	@Test
	public void testGetInventoryDetails(){
		
		try{
			PowerMockito.when(inventorySearchServiceClient, "search", Matchers.any(SearchCriteriaBO.class)).thenReturn(searchResultBO);
		
			SearchResultBO returnedSearchResultBO = inventorySearchDAO.getInventoryDetails("416546", "60601");

			Assert.assertNotNull("In testGetReviews, inventory search result was null", returnedSearchResultBO);
			//Assert.assertTrue("In testGetReviews, dealerReview has no consumer reviews", returnedDealerReview.getConsumerDealerReview().size() > 0);
			
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetInventoryDetails test failed due to exception");
		}
	}

	
	
	public InventorySearchServiceClient getInventorySearchServiceClient() {
		return inventorySearchServiceClient;
	}

	public void setInventorySearchServiceClient(InventorySearchServiceClient inventorySearchServiceClient) {
		this.inventorySearchServiceClient = inventorySearchServiceClient;
	}
}
