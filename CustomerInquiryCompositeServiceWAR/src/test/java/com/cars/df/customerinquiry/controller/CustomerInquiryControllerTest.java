package com.cars.df.customerinquiry.controller;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;

import com.cars.df.customerinquiry.bo.DealerOverviewDetails;
import com.cars.df.customerinquiry.bo.DealerResponseView;
import com.cars.df.customerinquiry.bo.DealerReviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewView;
import com.cars.df.customerinquiry.bo.DealerSearchResults;
import com.cars.df.customerinquiry.bo.DealerServiceRepairDetails;
import com.cars.df.customerinquiry.bo.DealerSpecialOfferDetails;
import com.cars.df.customerinquiry.service.CustomerSearchService;
import com.cars.df.customerinquiry.service.DealerOverviewService;
import com.cars.df.customerinquiry.service.DealerReviewService;
import com.cars.df.customerinquiry.service.DealerServiceRepairService;
import com.cars.df.customerinquiry.service.DealerSpecialOfferService;
import com.cars.df.customerinquiry.utility.DealerReviewViewValidator;
import com.cars.df.customerinquiry.utility.TestOverviewUtils;
import com.cars.df.customerinquiry.utility.TestReviewUtils;
import com.cars.df.customerinquiry.utility.TestServiceRepairUtils;
import com.cars.df.customerinquiry.utility.TestSpecialOfferUtils;
import com.cars.df.customerinquiry.utility.TestUtils;

public class CustomerInquiryControllerTest {


	private CustomerSearchService customerSearchService; 
	
	private DealerReviewService dealerReviewService; 
	
	private DealerSpecialOfferService dealerSpecialOfferService;
	
	private DealerServiceRepairService dealerServiceRepairService;
	 
	private DealerOverviewService dealerOverviewService;
	
	private CustomerInquiryController customerInquiryController;
	
	private DealerSearchResults dealerSearchResults;
	
	private DealerOverviewDetails dealerOverviewDetails;
	
	private DealerReviewDetails dealerReviewDetails;
	
	private DealerSpecialOfferDetails dealerSpecialOfferDetails;
	
	private DealerServiceRepairDetails dealerServiceRepairDetails;
	
	private DealerReviewView dealerReviewView;
	
	private DealerResponseView dealerResponseView;
	
	private DealerReviewViewValidator dealerReviewViewValidator;
	

	@Before
	public void setUp(){
		
		customerSearchService = PowerMockito.mock(CustomerSearchService.class);
		dealerReviewService = PowerMockito.mock(DealerReviewService.class);
		dealerSpecialOfferService = PowerMockito.mock(DealerSpecialOfferService.class);
		dealerServiceRepairService = PowerMockito.mock(DealerServiceRepairService.class);
		dealerOverviewService = PowerMockito.mock(DealerOverviewService.class);
		dealerReviewViewValidator = PowerMockito.mock(DealerReviewViewValidator.class);
		
		dealerSearchResults = TestUtils.createDealerSearchResults();
		dealerOverviewDetails = TestOverviewUtils.createDealerOverviewDetails();
		dealerReviewDetails = TestReviewUtils.createDealerReviewDetails();
		dealerSpecialOfferDetails = TestSpecialOfferUtils.createDealerSpecialOfferDetails();
		dealerServiceRepairDetails = TestServiceRepairUtils.createDealerServiceRepairDetails();		
		dealerReviewView = TestReviewUtils.createDealerReviewView();
		dealerResponseView = TestReviewUtils.createDealerResponseView();
		
		customerInquiryController = new CustomerInquiryController();
		customerInquiryController.setCustomerSearchService(customerSearchService);
		customerInquiryController.setDealerReviewService(dealerReviewService);
		customerInquiryController.setDealerOverviewService(dealerOverviewService);
		customerInquiryController.setDealerServiceRepairService(dealerServiceRepairService);
		customerInquiryController.setDealerSpecialOfferService(dealerSpecialOfferService);
		customerInquiryController.setDealerReviewViewValidator(dealerReviewViewValidator);
	}
	
	@Test
	public void testDealersSearchSales(){
		
		try{
			PowerMockito.when(customerSearchService, "searchSalesDealers", Matchers.any(String.class), Matchers.any(String[].class),
							  Matchers.any(String.class), Matchers.any(String.class), Matchers.any(String.class),
							  Matchers.any(String.class),Matchers.any(String.class),Matchers.any(String.class),
							  Matchers.any(String.class),Matchers.any(String.class)).thenReturn(dealerSearchResults);
			
			DealerSearchResults dealerSearchResults = customerInquiryController.dealersSearchSales("60601", new String[]{"20018"}, "100", "DISTANCE", "DSC", "1", "10", "", "20118", "43576");
			Assert.assertNotNull("In testDealersSearchSales, dealerSearchResults object was null", dealerSearchResults);
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testDealersSearchSales test failed due to exception");
		}
	}
	
	@Test
	public void testDealersSearchService(){
		try{
			PowerMockito.when(customerSearchService, "searchServiceDealers", Matchers.any(String.class), Matchers.any(String[].class),
							  Matchers.any(String.class), Matchers.any(String.class), Matchers.any(String.class),
							  Matchers.any(String.class),Matchers.any(String.class),Matchers.any(String.class),
							  Matchers.any(String.class),Matchers.any(String.class), Matchers.any(String.class)).thenReturn(dealerSearchResults);
			
			DealerSearchResults dealerSearchResults = customerInquiryController.dealersSearchService("60601", new String[]{"20018"}, "100", "DISTANCE", "DSC", "1", "10", "","501657", "20118", "43576");
			Assert.assertNotNull("In testDealersSearchService, dealerSearchResults object was null", dealerSearchResults);
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testDealersSearchService test failed due to exception");
		}
	}
	
	@Test
	public void testGetDealerOverviewDetails(){
		try{
			PowerMockito.when(dealerOverviewService, "getDealerOverviewDetails", Matchers.any(String.class)).thenReturn(dealerOverviewDetails);
			
			DealerOverviewDetails dealerOverviewDetails = customerInquiryController.getDealerOverviewDetails("102602");
			
			Assert.assertNotNull("In testGetDealerOverviewDetails, dealerOverviewDetails object was null", dealerOverviewDetails);
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetDealerOverviewDetails test failed due to exception");
		}
	}
	
	@Test
	public void testGetDealerReviewDetails(){
		try{
			PowerMockito.when(dealerReviewService, "getDealerReviewDetails", Matchers.any(String.class), Matchers.any(String.class),
							  Matchers.any(String.class), Matchers.any(String.class), Matchers.any(String.class)).thenReturn(dealerReviewDetails);
			
			DealerReviewDetails dealerReviewDetails = customerInquiryController.getDealerReviewDetails("102602", "1", "10", "feature-most-recent", "New");
			
			Assert.assertNotNull("In testGetDealerReviewDetails, dealerReviewDetails object was null", dealerReviewDetails);
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetDealerReviewDetails test failed due to exception");
		}
	}
	
	@Test
	public void testGetDealerSpecialOffersDetails(){
		try{
			PowerMockito.when(dealerSpecialOfferService, "getLocalOffersByPartyId", Matchers.any(String.class)).thenReturn(dealerSpecialOfferDetails);
			
			DealerSpecialOfferDetails dealerSpecialOfferDetails = customerInquiryController.getDealerSpecialOffersDetails("102602");
			
			Assert.assertNotNull("In testGetDealerSpecialOffersDetails, dealerSpecialOfferDetails object was null", dealerSpecialOfferDetails);
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetDealerSpecialOffersDetails test failed due to exception");
		}
	}
	
	@Test
	public void testGetServiceRepairDetails(){
		try{
			PowerMockito.when(dealerServiceRepairService, "getServiceRepairDetails", Matchers.any(String.class)).thenReturn(dealerServiceRepairDetails);
			
			DealerServiceRepairDetails dealerServiceRepairDetails = customerInquiryController.getServiceRepairDetails("102602");
			
			Assert.assertNotNull("In testGetServiceRepairDetails, dealerServiceRepairDetails object was null", dealerServiceRepairDetails);
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetServiceRepairDetails test failed due to exception");
		}
	}
	
	@Test
	public void testSaveDealerReview(){
		BeanPropertyBindingResult beanPropertyBindingResult = new BeanPropertyBindingResult(dealerReviewView, "Errors");

		try{
			//PowerMockito.when(dealerReviewViewValidator, "validate", dealerReviewView, beanPropertyBindingResult).thenReturn(true);
			PowerMockito.when(dealerReviewService, "saveDealerReview", Matchers.any(DealerReviewView.class)).thenReturn(true);

			ResponseEntity<StringBuffer> status = customerInquiryController.saveDealerReview("102602",dealerReviewView);
			
			Assert.assertTrue("In testSaveDealerReview, Review not saved successfully", status.getBody().toString().equals("SAVE_SUCCESSFUL"));
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSaveDealerReview test failed due to exception");
		}
	}
	
	//@Test
	public void testSaveBadDealerReview(){
		
		DealerReviewView dealerBadReviewView = new DealerReviewView();
		BeanPropertyBindingResult beanPropertyBindingResult = new BeanPropertyBindingResult(dealerBadReviewView, "Errors");

		try{
			PowerMockito.when(dealerReviewService, "saveDealerReview", Matchers.any(DealerReviewView.class)).thenReturn(true);
			PowerMockito.when(dealerReviewViewValidator, "validate", dealerBadReviewView, beanPropertyBindingResult).thenReturn(true);
			ResponseEntity<StringBuffer> status = customerInquiryController.saveDealerReview("102602",dealerBadReviewView);
			
			Assert.assertTrue("In testSaveDealerReview, Review not saved successfully", status.getBody().toString().equals("SAVE_SUCCESSFUL"));
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSaveDealerReview test failed due to exception");
		}
	}
	
	@Test
	public void testSaveFeedback(){
		try{
			PowerMockito.when(dealerReviewService, "saveReviewFeedback", Matchers.any(String.class), Matchers.any(String.class),
							  Matchers.any(Boolean.class),Matchers.any(Boolean.class)).thenReturn(true);
			
			Boolean status = customerInquiryController.saveFeedback("102602", "213457", true, false);
			
			Assert.assertTrue("In testSaveFeedback, feedback not saved successfully", status);
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSaveFeedback test failed due to exception");
		}
	}
	
	@Test
	public void testSaveReviewResponse(){
		try{
			PowerMockito.when(dealerReviewService, "saveReviewResponse", Matchers.any(String.class),Matchers.any(String.class),Matchers.any(DealerResponseView.class)).thenReturn(true);
			
			ResponseEntity<StringBuffer> status = customerInquiryController.saveResponse("102602", "213457", dealerResponseView);
			
			Assert.assertTrue("In testSaveReviewResponse, review feedback not saved successfully", status.getBody().toString().equals("SAVE_SUCCESSFUL"));
			
			//Add more Assertion

		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSaveReviewResponse test failed due to exception");
		}
	}
	
	@Test
	public void testInvalidZip(){
		DealerSearchResults dealerSearchResults = customerInquiryController.dealersSearchSales("6066", null,"10","DISTANCE","DSC", "1","10", " ", "", "");

		DealerSearchResults dealerSearchResultsService = customerInquiryController.dealersSearchService("6066", null,"10","DISTANCE","DSC", "1","10", " ", "", "","");

		
		assertNull("testInvalidZip failed.", dealerSearchResults.getDealerSummaryList());
		
		assertNull("testInvalidZip failed.", dealerSearchResultsService.getDealerSummaryList());

	}
	
}
