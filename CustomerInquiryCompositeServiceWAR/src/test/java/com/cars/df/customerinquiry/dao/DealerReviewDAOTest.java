package com.cars.df.customerinquiry.dao;

import com.cars.DRMS.client.DRMSClient;

import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.api.schemas.dealer_review.ReturnCode;
import com.cars.df.customerinquiry.utility.TestReviewUtils;
import com.cars.df.customerinquiry.utility.TestUtils;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;

public class DealerReviewDAOTest {

	private DRMSClient drmsClient;
	
	private DealerReviewDAO dealerReviewDAO;
	
	private DealerReview dealerReview;
	@Before
	public void setUp(){
		
		drmsClient = PowerMockito.mock(DRMSClient.class);
		dealerReview = TestReviewUtils.createDealerReview();
		
		dealerReviewDAO = new DealerReviewDAO();
		dealerReviewDAO.setDrmsClient(drmsClient);
	}
	
	
	@Test
	public void testGetReviews(){
		
		try{
			
			PowerMockito.when(drmsClient, "getWiredReviewsWithPagination", "102602", 0, 3, "featured-most-recent", "all-results").thenReturn(dealerReview);
		
			DealerReview returnedDealerReview = dealerReviewDAO.getReviews("102602", 0, 3, "featured-most-recent", "all-results");

			Assert.assertNotNull("In testGetReviews, dealerReview object was null", returnedDealerReview);
			Assert.assertTrue("In testGetReviews, dealerReview has no consumer reviews", returnedDealerReview.getConsumerDealerReview().size() > 0);
			
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetReviews test failed due to exception");
		}
		
	}
	
	@Test
	public void testSaveReview(){
		
		ReturnCode returnCode  =new ReturnCode();
		
		returnCode.setCode("SAVE_SUCCESSFUL");
		returnCode.setMessage("Review Saved Successfully");
		try{
			
			PowerMockito.when(drmsClient, "saveReview", Matchers.any(DealerReview.class)).thenReturn(returnCode);
		
			ReturnCode message = dealerReviewDAO.saveDealerReview(dealerReview);

			Assert.assertNotNull("In testSaveReview, returncode object was null", message);
			Assert.assertTrue("In testSaveReview, review not saved successfully ", "SAVE_SUCCESSFUL".equals(message.getCode()));
			
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSaveReview test failed due to exception");
		}
		
	}
	@Test
	public void testSaveFeedback(){
		
		ReturnCode returnCode  =new ReturnCode();
		
		returnCode.setCode("SAVE_SUCCESSFUL");
		returnCode.setMessage("Feedback Saved Successfully");
		try{
			
			PowerMockito.when(drmsClient, "saveFeedback", Matchers.any(DealerReview.class)).thenReturn(returnCode);
		
			ReturnCode message = dealerReviewDAO.saveFeedback(dealerReview);

			Assert.assertNotNull("In testSaveFeedback, returncode object was null", message);
			Assert.assertTrue("In testSaveFeedback, review not saved successfully ", "SAVE_SUCCESSFUL".equals(message.getCode()));
			
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSaveFeedback test failed due to exception");
		}
		
	}
	@Test
	public void testSaveReviewResponse(){
		
		ReturnCode returnCode  =new ReturnCode();
		
		returnCode.setCode("SAVE_SUCCESSFUL");
		returnCode.setMessage("Response Saved Successfully");
		try{
			
			PowerMockito.when(drmsClient, "saveDealerResponse", Matchers.any(DealerReview.class)).thenReturn(returnCode);
		
			ReturnCode message = dealerReviewDAO.saveReviewResponse(dealerReview);

			Assert.assertNotNull("In testSaveReviewResponse, returncode object was null", message);
			Assert.assertTrue("In testSaveReviewResponse, review not saved successfully ", "SAVE_SUCCESSFUL".equals(message.getCode()));
			
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSaveReviewResponse test failed due to exception");
		}
		
	}




	public DRMSClient getDrmsClient() {
		return drmsClient;
	}

	public void setDrmsClient(DRMSClient drmsClient) {
		this.drmsClient = drmsClient;
	}
	
	
}
