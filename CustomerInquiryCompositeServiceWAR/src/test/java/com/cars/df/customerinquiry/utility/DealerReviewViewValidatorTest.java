package com.cars.df.customerinquiry.utility;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;

import com.cars.df.customerinquiry.bo.DealerReviewView;

public class DealerReviewViewValidatorTest {

	private DealerReviewView dealerReviewView;
	private DealerReviewView dealerNoReviewView;

	private DealerReviewViewValidator dealerReviewViewValidator;
	
	@Before
	public void setUp(){
		dealerNoReviewView = new DealerReviewView();
		dealerReviewView = TestReviewUtils.createDealerReviewView();
		dealerReviewViewValidator = new DealerReviewViewValidator();
	}
	
	@Test
	public void testSupports(){
		try{
			Boolean isReviewView = dealerReviewViewValidator.supports(DealerReviewView.class);
			Assert.assertTrue("In testSupports, isReviewView object was false",isReviewView);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testSupports test failed due to exception");
		}
	}
	
	@Test
	public void testNotSupports(){
		try{
			Boolean isReviewView = dealerReviewViewValidator.supports(String.class);
			Assert.assertFalse("In testNotSupports, isReviewView object was true",isReviewView);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testNotSupports test failed due to exception");
		}
	}
	
	@Test
	public void testValidate(){
		try{
			BeanPropertyBindingResult bindingResults = new BeanPropertyBindingResult(dealerNoReviewView, "Errors");
			dealerReviewViewValidator.validate(dealerNoReviewView, bindingResults);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testValidate test failed due to exception");
		}
	}
	
	@Test
	public void testValidateWithData(){
		try{
			BeanPropertyBindingResult bindingResults = new BeanPropertyBindingResult(dealerReviewView, "Errors");
			dealerReviewViewValidator.validate(dealerReviewView, bindingResults);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testValidateWithData test failed due to exception");
		}
	}
}
