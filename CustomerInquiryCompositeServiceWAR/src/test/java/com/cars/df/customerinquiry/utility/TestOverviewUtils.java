package com.cars.df.customerinquiry.utility;

import com.cars.df.customerinquiry.bo.DealerOverviewDetails;

public class TestOverviewUtils {

	public static DealerOverviewDetails createDealerOverviewDetails(){
		
		DealerOverviewDetails dealerOverviewDetails = new DealerOverviewDetails();
		
		dealerOverviewDetails.setPartyId("56482");
		dealerOverviewDetails.setName("Lennys Depressing Auto Barn");
		dealerOverviewDetails.setAddressLine1("1798 Pebble Beach Dr");
		dealerOverviewDetails.setAddressLine2("");
		dealerOverviewDetails.setCity("Hoffman Estates");
		dealerOverviewDetails.setState("IL");
		dealerOverviewDetails.setRating("0.0");
		dealerOverviewDetails.setTotalReviewsNum("2");
		
		return dealerOverviewDetails;
	}
}
