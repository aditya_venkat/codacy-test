package com.cars.df.customerinquiry.utility;

import com.cars.df.css.bo.BreadCrumb;
import com.cars.df.css.bo.BreadCrumbValue;
import com.cars.df.css.bo.Dealer;
import com.cars.df.css.bo.Dealers;
import com.cars.df.css.bo.Refinement;
import com.cars.df.css.bo.RefinementValue;

public class TestDealerUtils {

	
	public static Dealers createDealers(){
		Dealers dealers = new Dealers();
		
		dealers.addDealer(createDealer1());
		//dealers.addDealer(createDealer2());
		
		dealers.setTotalDealers(1L);
		dealers.setZip("60601");
		
		dealers.addRefinements(createRefinement());
		dealers.addBreadCrumbs(createBreadCrumb());
		
		return dealers;
	}
	
	public static Refinement createRefinement(){
		RefinementValue refinementValue = new RefinementValue();
		
		refinementValue.setId("1234");
		refinementValue.setName("mkId");
		refinementValue.setCount("123");
		
		Refinement refinement = new Refinement();
		refinement.addRefinementValues(refinementValue);
		return refinement;
	}
	
	public static BreadCrumb createBreadCrumb(){
		BreadCrumbValue breadCrumbValue = new BreadCrumbValue();
		
		breadCrumbValue.setId("4124");
		breadCrumbValue.setName("Toyota");
		
		BreadCrumb breadCrumb = new BreadCrumb();
		breadCrumb.addBreadCrumbValues(breadCrumbValue);
		
		return breadCrumb;
	}
	

	
	public static Dealer createDealer1() {
		Dealer dealer = new Dealer(); 
		
		dealer.setPartyId("56482");
		dealer.setName("Lennys Depressing Auto Barn");
		dealer.setAddressLine1("1798 Pebble Beach Dr");
		dealer.setAddressLine2("");
		dealer.setCity("Hoffman Estates");
		dealer.setState("IL");
		dealer.setZip("60169");
		dealer.setRating("0.0");
		dealer.setTotalReviewsNum("2");
		dealer.setRpcCertified(false);
		
		return dealer;
	}
	
	public static Dealer createDealer2() {
		Dealer dealer = new Dealer(); 
		
		dealer.setPartyId("123456");
		dealer.setName("Cars.com");
		dealer.setAddressLine1("175 West Jackson blvd");
		dealer.setAddressLine2("");
		dealer.setCity("Chicago");
		dealer.setState("IL");
		dealer.setZip("60601");
		dealer.setRating("3.0");
		dealer.setTotalReviewsNum("4");
		dealer.setRpcCertified(true);
		
		return dealer;
	}
}
