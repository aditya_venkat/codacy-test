package com.cars.df.customerinquiry.utility;

import java.util.ArrayList;
import java.util.List;

import com.cars.service.listingsearch.bo.RefinementBO;
import com.cars.service.listingsearch.bo.RefinementValueBO;
import com.cars.service.listingsearch.bo.SearchResultBO;

public class TestInventoryUtils {

	public static SearchResultBO createInventorySearchResults(){
	
		SearchResultBO inventorySearchResults = new SearchResultBO();
		
		RefinementBO refinementBO = new RefinementBO();
		
		List<RefinementValueBO> refinementValuesBOList = new ArrayList<RefinementValueBO>();
		refinementValuesBOList.add(createRefinementValueBO("New", 100));
		refinementValuesBOList.add(createRefinementValueBO("Used", 100));
		
		refinementBO.replaceRefinementValues(refinementValuesBOList);
		refinementBO.setName("stkTypId");
		
		inventorySearchResults.addRefinement(refinementBO);
		
		return inventorySearchResults;
	}
	
	private static RefinementValueBO createRefinementValueBO(String code, Integer value){
		
		RefinementValueBO refinementValueBO = new RefinementValueBO();
		
		refinementValueBO.setName(code);
		refinementValueBO.setCount(value);
		return refinementValueBO;
		
	}
}
