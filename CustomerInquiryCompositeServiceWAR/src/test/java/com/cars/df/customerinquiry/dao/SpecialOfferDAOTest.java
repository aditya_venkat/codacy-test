package com.cars.df.customerinquiry.dao;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;

import com.cars.df.customerinquiry.client.ServiceClients;
import com.cars.df.customerinquiry.utility.TestSpecialOfferUtils;
import com.cars.ss.lois.bo.generated.LocalOffers;

public class SpecialOfferDAOTest {

	private ServiceClients serviceClients;
	
	private SpecialOfferDAO specialOfferDAO;
	
	private LocalOffers localOffers;
	
	@Before
	public void setUp(){
		serviceClients = PowerMockito.mock(ServiceClients.class);
		
		localOffers = TestSpecialOfferUtils.createLocalOffers();
		specialOfferDAO = new SpecialOfferDAO();
		specialOfferDAO.setServiceClients(serviceClients);
	}
	
	@Test
	public void testGetOfferAndModelCount(){
		String offerAndModelCount = "120,120";
		String partyId = "416546";
		try{
			PowerMockito.when(serviceClients, "getOfferAndModelCount", Matchers.any(String.class)).thenReturn(offerAndModelCount);
			
			String returnedOfferAndModelCount = specialOfferDAO.getOfferAndModelCount(partyId);
			
			assertNotNull("In testGetOfferAndModelCount, returnedOfferAndModelCount is null", returnedOfferAndModelCount);
			assertTrue("In testGetOfferAndModelCount, returnedOfferAndModelCount is not true",  "120,120".equals(returnedOfferAndModelCount));
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetOfferAndModelCount Dao test failed due to exception");
		}
		
	}
	
	@Test
	public void testGetLocalOffers(){
		try{
			PowerMockito.when(serviceClients, "getLocalOffersByDealerId", Matchers.any(String.class)).thenReturn(localOffers);
			
			LocalOffers localOffers = specialOfferDAO.getLocalOffers("102602");
			
			assertNotNull("In testGetLocalOffers, localOffers is null", localOffers);
			//assertTrue("In testGetLocalOffers, returnedOfferAndModelCount is not true",  "120,120".equals(returnedOfferAndModelCount));
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetLocalOffers Dao test failed due to exception");
		}
		
	}
}
