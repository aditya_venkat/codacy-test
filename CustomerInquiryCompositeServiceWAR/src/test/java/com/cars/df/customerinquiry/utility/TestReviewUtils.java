package com.cars.df.customerinquiry.utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cars.api.schemas.dealer_review.Affiliate;
import com.cars.api.schemas.dealer_review.CategoryRate;
import com.cars.api.schemas.dealer_review.CategoryRateId;
import com.cars.api.schemas.dealer_review.CategoryRating;
import com.cars.api.schemas.dealer_review.CategoryType;
import com.cars.api.schemas.dealer_review.ConsumerDealerReview;
import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.api.schemas.dealer_review.DealerReviewResponse;
import com.cars.api.schemas.dealer_review.FeedbackResponse;
import com.cars.api.schemas.dealer_review.OverallCategoryRating;
import com.cars.api.schemas.dealer_review.OverallCategoryRatingId;
import com.cars.api.schemas.dealer_review.RejectReason;
import com.cars.api.schemas.dealer_review.ReturnCode;
import com.cars.api.schemas.dealer_review.Status;
import com.cars.api.schemas.dealer_review.VisitReason;
import com.cars.df.customerinquiry.bo.DealerResponseView;
import com.cars.df.customerinquiry.bo.DealerReviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewSummaryView;
import com.cars.df.customerinquiry.bo.DealerReviewView;

public class TestReviewUtils {

	//Start of Dealer Review
	public static DealerReview createDealerReview(){
		
		DealerReview dealerReview = new DealerReview();
		
		dealerReview.setDealerPartyIdentifier(416546L);
		dealerReview.setCustomerIdentifier(102602);
		dealerReview.setConsumerDealerReviewIdentifier(123456);
		dealerReview.setModeratorIdentifier("56789");
		dealerReview.setOverallDealershipRating(5.0);
		dealerReview.setTotalApprovedNumber(100);
		dealerReview.setTotalRecommendedNumber(100);
		dealerReview.setFilteredReviewCount(50);
		dealerReview.setNegativeFeedbackNumber(1);
		dealerReview.setPositiveFeedbackNumber(99);
		dealerReview.setOptOutCd("IN");
		
		List<ConsumerDealerReview> consumerDealerReviewList = new ArrayList<ConsumerDealerReview>();
		consumerDealerReviewList.add(createReview1());
		dealerReview.setConsumerDealerReview(consumerDealerReviewList);
		
		dealerReview.setOverallSubCategoryRating(createOverallSubCategoryRating());
		
		ReturnCode returnCode = new ReturnCode();
		returnCode.setCode("200");
		returnCode.setMessage("Review Found");
		
		dealerReview.setReturnCode(returnCode);
		return dealerReview;
	}
	
	public static DealerReview createDealerReviewWithNoReviews(){
		
		DealerReview dealerReview = new DealerReview();
		
		dealerReview.setDealerPartyIdentifier(416546L);
		dealerReview.setCustomerIdentifier(102602);
		dealerReview.setConsumerDealerReviewIdentifier(123456);
		dealerReview.setModeratorIdentifier("56789");
		dealerReview.setOverallDealershipRating(5.0);
		dealerReview.setTotalApprovedNumber(100);
		dealerReview.setTotalRecommendedNumber(100);
		dealerReview.setFilteredReviewCount(50);
		dealerReview.setNegativeFeedbackNumber(1);
		dealerReview.setPositiveFeedbackNumber(99);
		dealerReview.setOptOutCd("IN");
		
		List<ConsumerDealerReview> consumerDealerReviewList = new ArrayList<ConsumerDealerReview>();
		dealerReview.setConsumerDealerReview(consumerDealerReviewList);		
		
		dealerReview.setOverallSubCategoryRating(createOverallSubCategoryRating());
		
		ReturnCode returnCode = new ReturnCode();
		returnCode.setCode("200");
		returnCode.setMessage("Review Found");
		
		dealerReview.setReturnCode(returnCode);
		return dealerReview;
		
	}
	
	private static ConsumerDealerReview createReview1(){
		
		ConsumerDealerReview consumerDealerReview = new ConsumerDealerReview();
		
		consumerDealerReview.setConsumerDealerReviewIdentifier(123456);
		consumerDealerReview.setModeratorIdentifier("56789");
		consumerDealerReview.setSubmittedDate(new Date());
		consumerDealerReview.setModeratorSubmittedDate(new Date());
		consumerDealerReview.setModeratorResponseDate(new Date());
		consumerDealerReview.setTitle("Awesome Review");
		consumerDealerReview.setText("Awesome Review1. Awesome Review1. Awesome Review1. Awesome Review1. Awesome Review1. Awesome Review1. Awesome Review1");
		consumerDealerReview.setDisplayName("Aditya");
		consumerDealerReview.setLocationDescription("Chicago, IL");
		consumerDealerReview.setEmail("Lenny@Lenny.com");
		consumerDealerReview.setDealerEmployed(false);
		consumerDealerReview.setOverallRating(5.0);
		consumerDealerReview.setRecommendDealer(true);
		consumerDealerReview.setPurchaseVehicle(true);
		consumerDealerReview.setHelpfulCount(100);
		consumerDealerReview.setUnhelpfulCount(1);
		consumerDealerReview.setIpAddress("172.10.10.10");
		consumerDealerReview.setPromotionTrackingCode("promotion");
		consumerDealerReview.setUserToken("Lenny");
		consumerDealerReview.setDealerPartyIdentifier(416546L);
		consumerDealerReview.setFeaturedReview(1);
		consumerDealerReview.setVisitReason(createVisitReasonList());
		consumerDealerReview.setStatus(createApprovedStatus());
		consumerDealerReview.setRejectionReason(createRejectReason());
		
		consumerDealerReview.setCategoryRatings(createCategoryRatingList());
		
		consumerDealerReview.setDealerReviewResponse(createDealerReviewResponseList());
		
		consumerDealerReview.setFeedbackResponse(createFeedbackResponse());
		
		Affiliate affiliate = new Affiliate();
		affiliate.setId(550039);
		affiliate.setName("National");
		
		consumerDealerReview.setAffiliate(affiliate);
		
		return consumerDealerReview;
	}
	
	private static List<OverallCategoryRating> createOverallSubCategoryRating(){
		
		List<OverallCategoryRating> overallCategoryRatingList = new ArrayList<OverallCategoryRating>();
		
		overallCategoryRatingList.add(createSingleOverAllCategory(Constants.CUSTOMER_SERVICE));
		overallCategoryRatingList.add(createSingleOverAllCategory(Constants.BUYING_PROCESS));
		overallCategoryRatingList.add(createSingleOverAllCategory(Constants.QUALITY_OF_REPAIR));
		overallCategoryRatingList.add(createSingleOverAllCategory(Constants.OVERALL_FACILITIES));
		return overallCategoryRatingList;
		
		
	}
	
	private static OverallCategoryRating createSingleOverAllCategory(String code){
		OverallCategoryRating overallCategoryRating = new OverallCategoryRating();
		overallCategoryRating.setRating(5.0);
		
		OverallCategoryRatingId overallCategoryRatingId = new OverallCategoryRatingId();
		overallCategoryRatingId.setCategoryTypeCode(code);
		overallCategoryRating.setId(overallCategoryRatingId);
		return overallCategoryRating;
	}
	private static List<VisitReason> createVisitReasonList(){
		
		List<VisitReason> visitReasonList = new ArrayList<VisitReason>();
		
		visitReasonList.add(createVisitReason(Constants.SHOP_NEW));
		visitReasonList.add(createVisitReason(Constants.SHOP_USED));
		visitReasonList.add(createVisitReason(Constants.SERVICE_REPAIR));
		
		return visitReasonList;
	}
	
	private static VisitReason createVisitReason(String code){
		VisitReason visitReason = new VisitReason();
		visitReason.setCode(code);
		
		return visitReason;
	}
	
	private static List<CategoryRating> createCategoryRatingList(){
		
		List<CategoryRating> categoryRatingList = new ArrayList<CategoryRating>();
		
		categoryRatingList.add(createCategoryRating(Constants.CUSTOMER_SERVICE, "Customer Service", "5.0"));
		categoryRatingList.add(createCategoryRating(Constants.BUYING_PROCESS, "Buying Process", "5.0"));
		categoryRatingList.add(createCategoryRating(Constants.QUALITY_OF_REPAIR, "Quality Of Repair", "5.0"));
		categoryRatingList.add(createCategoryRating(Constants.OVERALL_FACILITIES, "OverallFacilities", "5.0"));
		return categoryRatingList;
	}
	
	private static CategoryRating createCategoryRating(String code, String description,
			String rating) {

		CategoryRating categoryRating = new CategoryRating();
		CategoryType categoryType = new CategoryType();
		categoryType.setDescription(description);
		categoryType.setCode(code);
		categoryRating.setCategoryType(categoryType);
		CategoryRate categoryRate = new CategoryRate();
		CategoryRateId categoryRateId = new CategoryRateId();
		categoryRateId.setCode(code);
		try {
			categoryRateId.setRatingNumber(Double.valueOf(rating));
		} catch (NumberFormatException e) {
			categoryRateId.setRatingNumber(0.0);
		}
		categoryRate.setId(categoryRateId);
		categoryRating.setRating(categoryRate);
		return categoryRating;
	}
	
	private static List<DealerReviewResponse> createDealerReviewResponseList(){
		List<DealerReviewResponse> dealerReviewResponseList =  new ArrayList<DealerReviewResponse>();
		
		DealerReviewResponse dealerReviewResponse = new DealerReviewResponse();
		
		dealerReviewResponse.setDealerReviewResponseIdentifer(78907);
		dealerReviewResponse.setSubmittedDate(new Date());
		dealerReviewResponse.setModeratorResponseDate(new Date());
		dealerReviewResponse.setModeratorSubmittedDate(new Date());
		dealerReviewResponse.setStatus(createApprovedStatus());
		dealerReviewResponse.setRejectionReason(createRejectReason());
		
		dealerReviewResponse.setResponseName("Mike");
		dealerReviewResponse.setText("Thank you for Wonderful Review");
		dealerReviewResponse.setIpAddress("172.89.10.14");
		dealerReviewResponse.setModeratorIdentifier("3465654");
		dealerReviewResponse.setEmail("Mike@Mike.com");
		dealerReviewResponse.setPhoneDisplay("Response");
		dealerReviewResponse.setAddressLine1("175 W Jackson");
		dealerReviewResponse.setAddressLine2("Floor 18");
		dealerReviewResponse.setCity("Chicago");
		dealerReviewResponse.setState(0); 
		dealerReviewResponse.setPostalCode("60601");
		dealerReviewResponse.setPostalPlus4("60601");
		dealerReviewResponse.setDealerReviewFeedback(createFeedbackResponse());
		
		dealerReviewResponseList.add(dealerReviewResponse);
		
		return dealerReviewResponseList;
		
	}
	
	private static List<FeedbackResponse> createFeedbackResponse(){
		List<FeedbackResponse> feedbackResponseList = new ArrayList<FeedbackResponse>();
		FeedbackResponse feedbackResponse = new FeedbackResponse();
		feedbackResponse.setAbuse(false);
		feedbackResponse.setFeedbackResponseIdentifier(1245);
		feedbackResponse.setHelpful(true);
		feedbackResponse.setIpAddress("168.10.10.123");
		feedbackResponse.setModeratorResponseDate(new Date());
		feedbackResponse.setModeratorSubmittedDate(new Date());
		
		return feedbackResponseList;
	}
	
	private static Status createApprovedStatus(){
		Status status = new Status();
		status.setCode("APP");
		status.setDescription("Approved");
		
		return status;
	}
	
	private static RejectReason createRejectReason(){
		RejectReason rejectReason = new RejectReason();
		rejectReason.setCode("REJECTED");
		rejectReason.setDescription("BAD REVIEW");
		
		return rejectReason;
	}
	
	
	
	//End Of Dealer Review
	
	//Start of Dealer Review Summary View
	public static DealerReviewSummaryView createDealerReviewSummaryView(){
		
		DealerReviewSummaryView dealerReviewSummaryView = new DealerReviewSummaryView();
		
		dealerReviewSummaryView.setOverallRating("5.0");
		dealerReviewSummaryView.setCustomerServiceRating("5.0");
		dealerReviewSummaryView.setBuyingProcessRating("5.0");
		dealerReviewSummaryView.setQualityOfRepairRating("5.0");
		dealerReviewSummaryView.setFacilityRating("5.0");
		dealerReviewSummaryView.setRecommendedNumber("100");
		dealerReviewSummaryView.setTotalReviews("100");
		dealerReviewSummaryView.setTotalReviewsOnPage("100");
		
		List<DealerReviewView> dealerReviewViewList = new ArrayList<DealerReviewView>();
		
		dealerReviewViewList.add(createDealerReviewView());
		dealerReviewSummaryView.setDealerReviewList(dealerReviewViewList);
		
		return dealerReviewSummaryView;
		
	}
	
	public static DealerReviewView createDealerReviewView(){
		
		DealerReviewView dealerReviewView = new DealerReviewView();
		
		dealerReviewView.setReviewId("12345");
		dealerReviewView.setReviewerEmail("Lenny@Lenny.com");
		dealerReviewView.setReviewerLoc("Chicago, IL");
		dealerReviewView.setReviewerName("Lenny");
		dealerReviewView.setReviewText("Awesome Dealer1 Awesome Dealer1 Awesome Dealer1 Awesome Dealer1");
		dealerReviewView.setReviewTitle("Awesome Dealer1");
		dealerReviewView.setHelpfulCount("100");
		dealerReviewView.setUnHelpfulCount("0");
		dealerReviewView.setOverallRating("5.0");
		dealerReviewView.setReviewDate("03-12-2015");
		dealerReviewView.setRecommend(true);
		dealerReviewView.setPurchase(true);
		dealerReviewView.setCustomerServiceRating("5.0");
		dealerReviewView.setBuyingProcessRating("5.0");
		dealerReviewView.setQualityOfRepairRating("5.0");
		dealerReviewView.setFacilityRating("5.0");
		dealerReviewView.setVisitNewCar(true);
		dealerReviewView.setVisitUsedCar(true);
		dealerReviewView.setVisitServiceCar(true);
		dealerReviewView.setIsDealer(false);
		
		dealerReviewView.setDealerResponseView(createDealerResponseView());
		
		
		dealerReviewView.setIsFeatured(true);
		
		return dealerReviewView;
		
		
	}
	
	public static DealerResponseView createDealerResponseView(){
		DealerResponseView dealerResponseView = new DealerResponseView();
		dealerResponseView.setDate("03-12-2016");
		dealerResponseView.setText("Thank you for your valuable feedback");
		dealerResponseView.setName("Aditya");
		dealerResponseView.setEmail("aditya@aditya.com");
		dealerResponseView.setZipCode("60601");
		dealerResponseView.setPhoneNumber("1234567890");
		
		return dealerResponseView;
	}
	//End of Dealer Review Summary View

	//Start of Dealer Review Details
	public static DealerReviewDetails createDealerReviewDetails(){
		DealerReviewDetails dealerReviewDetails = new DealerReviewDetails();
		
		dealerReviewDetails.setDealerReviewSummaryView(createDealerReviewSummaryView());
		dealerReviewDetails.setPartyId("56482");
		dealerReviewDetails.setName("Lennys Depressing Auto Barn");
		dealerReviewDetails.setAddressLine1("1798 Pebble Beach Dr");
		dealerReviewDetails.setAddressLine2("");
		dealerReviewDetails.setCity("Hoffman Estates");
		dealerReviewDetails.setState("IL");
		dealerReviewDetails.setRating("0.0");
		dealerReviewDetails.setTotalReviewsNum("2");
		return dealerReviewDetails;
	}
	
	public static DealerReviewDetails createDealerReviewsDetailsWithNoReviews(){
		DealerReviewDetails dealerReviewDetails = new DealerReviewDetails();
		
		dealerReviewDetails.setDealerReviewSummaryView(new DealerReviewSummaryView());
		dealerReviewDetails.setPartyId("56482");
		dealerReviewDetails.setName("Lennys Depressing Auto Barn");
		dealerReviewDetails.setAddressLine1("1798 Pebble Beach Dr");
		dealerReviewDetails.setAddressLine2("");
		dealerReviewDetails.setCity("Hoffman Estates");
		dealerReviewDetails.setState("IL");
		dealerReviewDetails.setRating("0.0");
		dealerReviewDetails.setTotalReviewsNum("2");
		return dealerReviewDetails;
	}
	//End Of Dealer Review Details
	
}
