package com.cars.df.customerinquiry.utility;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;

import com.cars.api.schemas.dealer_review.DealerReview;
import com.cars.df.css.bo.Dealers;
import com.cars.df.customerinquiry.bo.DealerOverviewDetails;
import com.cars.df.customerinquiry.bo.DealerReviewSummaryView;
import com.cars.service.listingsearch.bo.SearchResultBO;



public class DealerOverviewUtilityMapperTest {

	private DealerOverviewUtilityMapper dealerOverviewUtilityMapper;
	private DealerCommonUtilityMapper dealerCommonUtilityMapper;
	private DealerReviewUtilityMapper dealerReviewUtilityMapper;
	
	private Dealers dealers;
	private DealerReview dealerReview;
	private DealerReviewSummaryView dealerReviewSummaryView;
	private SearchResultBO inventorySearchResults; 
	
	@Before
	public void setUp(){
		dealerCommonUtilityMapper = PowerMockito.mock(DealerCommonUtilityMapper.class);
		dealerReviewUtilityMapper = PowerMockito.mock(DealerReviewUtilityMapper.class);
		dealers = TestDealerUtils.createDealers();
		dealerReview = TestReviewUtils.createDealerReview();
		dealerReviewSummaryView = TestReviewUtils.createDealerReviewSummaryView();
		
		inventorySearchResults = TestInventoryUtils.createInventorySearchResults();
		dealerOverviewUtilityMapper = new DealerOverviewUtilityMapper();
		dealerOverviewUtilityMapper.setDealerCommonUtilityMapper(dealerCommonUtilityMapper);
		dealerOverviewUtilityMapper.setDealerReviewUtilityMapper(dealerReviewUtilityMapper);
	}
	
	@Test
	public void testCreateDealerOverviewDetails(){
		
		String countOffersAndModels = "12,12";
		try{
			
			PowerMockito.when(dealerReviewUtilityMapper, "createDealerReviewSummaryView", Matchers.any(DealerReview.class)).thenReturn(dealerReviewSummaryView);
			DealerOverviewDetails dealerOverviewDetails = dealerOverviewUtilityMapper.createDealerOverviewDetails(dealers, dealerReview, inventorySearchResults, countOffersAndModels);
			
			Assert.assertNotNull("In testCreateDealerOverviewDetails, dealerOverviewDetails is null", dealerOverviewDetails);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testCreateDealerOverviewDetails failed due to exception");
		}
	}
}
