/**
 * 
 */
package com.cars.df.customerinquiry.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;

import com.cars.api.schemas.lead.NonInventoryLead;
import com.cars.df.customerinquiry.client.ServiceClients;

/**
 * @author lgiovenco
 *
 */
public class LeadpathDAOTest {
	private ServiceClients serviceClients;
	private LeadpathDAO leadpathDAO;
	
	@Before
	public void setUp(){
		serviceClients = PowerMockito.mock(ServiceClients.class);
		
		leadpathDAO = new LeadpathDAO();
		leadpathDAO.setServiceClients(serviceClients);
	}
	
	@Test
	public void testSendDealerEmailLead(){

		try{
			PowerMockito.when(serviceClients, "sendDealerEmailLead", Matchers.any(NonInventoryLead.class)).thenReturn(new Boolean(true));
			
			Boolean returnValue = leadpathDAO.sendDealerEmailLead(new NonInventoryLead());
			
			assertNotNull("In testSendDealerEmailLead, returned Boolean is null", returnValue);
			assertTrue("In testSendDealerEmailLead, returned Boolean is not true",  returnValue);
		} catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test testGetOfferAndModelCount Dao test failed due to exception");
		}
		
	}
}
