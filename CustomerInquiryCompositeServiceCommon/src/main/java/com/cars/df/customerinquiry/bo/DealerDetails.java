package com.cars.df.customerinquiry.bo;

public class DealerDetails {

	private String name;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String zip;
	private String rating;
	private String totalReviewsNum;
	private String partyId;
	private String customerId; 
	
	private String newCarEmail;
	private String usedCarEmail;
	
	
	private String newCarTollFreePhoneNumber; 
	private String usedCarTollFreePhoneNumber; 
	private String serviceTollFreePhoneNumber; 
	

	//ask nathan for promo tagline
	private String promoTagLine;

	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getTotalReviewsNum() {
		return totalReviewsNum;
	}
	public void setTotalReviewsNum(String totalReviewsNum) {
		this.totalReviewsNum = totalReviewsNum;
	}

	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getPromoTagLine() {
		return promoTagLine;
	}
	public void setPromoTagLine(String promoTagLine) {
		this.promoTagLine = promoTagLine;
	}
	public String getNewCarEmail() {
		return newCarEmail;
	}
	public void setNewCarEmail(String newCarEmail) {
		this.newCarEmail = newCarEmail;
	}
	public String getUsedCarEmail() {
		return usedCarEmail;
	}
	public void setUsedCarEmail(String usedCarEmail) {
		this.usedCarEmail = usedCarEmail;
	}
	public String getNewCarTollFreePhoneNumber() {
		return newCarTollFreePhoneNumber;
	}
	public void setNewCarTollFreePhoneNumber(String newCarTollFreePhoneNumber) {
		this.newCarTollFreePhoneNumber = newCarTollFreePhoneNumber;
	}
	public String getUsedCarTollFreePhoneNumber() {
		return usedCarTollFreePhoneNumber;
	}
	public void setUsedCarTollFreePhoneNumber(String usedCarTollFreePhoneNumber) {
		this.usedCarTollFreePhoneNumber = usedCarTollFreePhoneNumber;
	}
	public String getServiceTollFreePhoneNumber() {
		return serviceTollFreePhoneNumber;
	}
	public void setServiceTollFreePhoneNumber(String serviceTollFreePhoneNumber) {
		this.serviceTollFreePhoneNumber = serviceTollFreePhoneNumber;
	} 
}
