package com.cars.df.customerinquiry.bo;

import java.math.BigDecimal;

public class DealerCommonService {

    private String code;
    private String description;
    private BigDecimal price;
    private String priceType;
    private String productDescription;
    private String freeIndicator;
    private String detail;
    private String disclaimer;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getPriceType() {
		return priceType;
	}
	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getFreeIndicator() {
		return freeIndicator;
	}
	public void setFreeIndicator(String freeIndicator) {
		this.freeIndicator = freeIndicator;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getDisclaimer() {
		return disclaimer;
	}
	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}
}
