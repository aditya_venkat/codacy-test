package com.cars.df.customerinquiry.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DealerServiceRepairDetails extends DealerDetails{

	//private String serviceAppointmentUrl;
	//private String serviceTollFreeNumber;
	//private Boolean rpcCertified;
	
	//Class from repair service for service coupons
	
	//Class from repair service for Fast Facts
	
	//class for repair service for service amenities
	
	private String serviceOverview;
	
	private WarrantyInformation warrantyInformation;
	
	private List<DealerCommonService> commonServicesList;
	
	private List<DealerFastFact> fastFactsList;
	
	private List<DealerAmenity> amenitiesList;
	
	private DealerServiceWarranty dealerServiceWarranty;

	private String serviceAppointmentUrl;
	
	private Map<String, String> serviceHoursOperation;

	

	public String getServiceOverview() {
		return serviceOverview;
	}

	public void setServiceOverview(String serviceOverview) {
		this.serviceOverview = serviceOverview;
	}

	public WarrantyInformation getWarrantyInformation() {
		return warrantyInformation;
	}

	public void setWarrantyInformation(WarrantyInformation warrantyInformation) {
		this.warrantyInformation = warrantyInformation;
	}

	public List<DealerCommonService> getCommonServicesList() {
		return commonServicesList;
	}

	public void setCommonServicesList(List<DealerCommonService> commonServicesList) {
		this.commonServicesList = commonServicesList;
	}

	public void addCommonService(DealerCommonService commonService){
		if(commonServicesList == null){
			commonServicesList = new ArrayList<DealerCommonService>();
		}
		commonServicesList.add(commonService);
	}
	public List<DealerFastFact> getFastFactsList() {
		return fastFactsList;
	}

	public void setFastFactsList(List<DealerFastFact> fastFactsList) {
		this.fastFactsList = fastFactsList;
	}
	
	public void addFastFact(DealerFastFact fastFact){
		if(fastFactsList == null){
			fastFactsList = new ArrayList<DealerFastFact>();
		}
		
		fastFactsList.add(fastFact);
	}

	public List<DealerAmenity> getAmenitiesList() {
		return amenitiesList;
	}

	public void setAmenitiesList(List<DealerAmenity> amenitiesList) {
		this.amenitiesList = amenitiesList;
	}
	
	public void addAmenity(DealerAmenity amenity) {
		
		if(amenitiesList == null){
			amenitiesList = new ArrayList<DealerAmenity>();
		}
		amenitiesList.add(amenity);
	}
	public void addAllAmenity(int index, List<DealerAmenity> amenities){
		if(amenitiesList == null){
			amenitiesList = new ArrayList<DealerAmenity>();
		}
		amenitiesList.addAll(index, amenities);
	}

	public DealerServiceWarranty getDealerServiceWarranty() {
		return dealerServiceWarranty;
	}

	public void setDealerServiceWarranty(DealerServiceWarranty dealerServiceWarranty) {
		this.dealerServiceWarranty = dealerServiceWarranty;
	}

	public String getServiceAppointmentUrl() {
		return serviceAppointmentUrl;
	}

	public void setServiceAppointmentUrl(String serviceAppointmentUrl) {
		this.serviceAppointmentUrl = serviceAppointmentUrl;
	}

	public Map<String, String> getServiceHoursOperation() {
		return serviceHoursOperation;
	}

	public void setServiceHoursOperation(Map<String, String> serviceHoursOperation) {
		this.serviceHoursOperation = serviceHoursOperation;
	}
	
}
