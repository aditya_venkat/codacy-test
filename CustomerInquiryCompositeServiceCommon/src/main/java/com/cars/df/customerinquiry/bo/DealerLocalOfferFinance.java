package com.cars.df.customerinquiry.bo;

public class DealerLocalOfferFinance extends DealerLocalOffer{

	private String bonusCash;
	private String term;
	private String apr;
	
	
	public String getBonusCash() {
		return bonusCash;
	}
	public void setBonusCash(String bonusCash) {
		this.bonusCash = bonusCash;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getApr() {
		return apr;
	}
	public void setApr(String apr) {
		this.apr = apr;
	}
	
	
	
}
