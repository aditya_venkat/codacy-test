package com.cars.df.customerinquiry.bo;

import java.util.ArrayList;
import java.util.List;

public class DealerReviewSummaryView {

	private String overallRating;
	private String customerServiceRating;
	private String buyingProcessRating;
	private String qualityOfRepairRating;
	private String facilityRating;
	private String recommendedNumber;
	private String totalReviews;
	private String totalReviewsOnPage;
	private boolean featuredReview;
	
	private List<DealerReviewView> dealerReviewList;

	public String getOverallRating() {
		return overallRating;
	}

	public void setOverallRating(String overallRating) {
		this.overallRating = overallRating;
	}
	

	public String getCustomerServiceRating() {
		return customerServiceRating;
	}

	public void setCustomerServiceRating(String customerServiceRating) {
		this.customerServiceRating = customerServiceRating;
	}

	public String getBuyingProcessRating() {
		return buyingProcessRating;
	}

	public void setBuyingProcessRating(String buyingProcessRating) {
		this.buyingProcessRating = buyingProcessRating;
	}

	public String getQualityOfRepairRating() {
		return qualityOfRepairRating;
	}

	public void setQualityOfRepairRating(String qualityOfRepairRating) {
		this.qualityOfRepairRating = qualityOfRepairRating;
	}

	public String getFacilityRating() {
		return facilityRating;
	}

	public void setFacilityRating(String facilityRating) {
		this.facilityRating = facilityRating;
	}

	public String getRecommendedNumber() {
		return recommendedNumber;
	}

	public void setRecommendedNumber(String recommendedNumber) {
		this.recommendedNumber = recommendedNumber;
	}

	public String getTotalReviews() {
		return totalReviews;
	}

	public void setTotalReviews(String totalReviews) {
		this.totalReviews = totalReviews;
	}

	public String getTotalReviewsOnPage() {
		return totalReviewsOnPage;
	}

	public void setTotalReviewsOnPage(String totalReviewsOnPage) {
		this.totalReviewsOnPage = totalReviewsOnPage;
	}

	public boolean isFeaturedReview() {
		return featuredReview;
	}

	public void setFeaturedReview(boolean featuredReview) {
		this.featuredReview = featuredReview;
	}


	public List<DealerReviewView> getDealerReviewList() {
		return dealerReviewList;
	}

	public void setDealerReviewList(List<DealerReviewView> dealerReviewList) {
		this.dealerReviewList = dealerReviewList;
	}

	public void addDealerReview(DealerReviewView dealerReview) {
		if(dealerReviewList == null){
			dealerReviewList = new ArrayList<DealerReviewView>();
		}
		
		dealerReviewList.add(dealerReview);
	}

	
	
}
