package com.cars.df.customerinquiry.bo;

import java.util.ArrayList;
import java.util.List;

public class DealerSpecialOfferDetails extends DealerDetails {



	private List<YearMakeModelOffer> yearMakeModelOfferList;

	public List<YearMakeModelOffer> getYearMakeModelOfferList() {
		return yearMakeModelOfferList;
	}

	public void setYearMakeModelOfferList(List<YearMakeModelOffer> yearMakeModelOfferList) {
		this.yearMakeModelOfferList = yearMakeModelOfferList;
	}

	
	public void addYearMakeModelOffer(YearMakeModelOffer yearMakeModelOffer) {
		
		if(yearMakeModelOfferList == null){
			yearMakeModelOfferList = new ArrayList<YearMakeModelOffer>();
		}
		
		yearMakeModelOfferList.add(yearMakeModelOffer);
	}

	
	
	
}
