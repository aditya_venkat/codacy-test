package com.cars.df.customerinquiry.bo;

import java.util.ArrayList;
import java.util.List;

public class DirectoryRefinement {

	private String id; 
	private String name;
	private List<DirectoryRefinementValue> refinementValues;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<DirectoryRefinementValue> getRefinementValues() {
		return refinementValues;
	}
	public void setRefinementValues(List<DirectoryRefinementValue> refinementValues) {
		this.refinementValues = refinementValues;
	}
	
	public void addRefinementValue(DirectoryRefinementValue refinementValue){
		if(refinementValues == null){
			refinementValues = new ArrayList<DirectoryRefinementValue>();
		}
		refinementValues.add(refinementValue);
		
	}
}
