package com.cars.df.customerinquiry.bo;

public class DealerReviewView {
	
	private String reviewTitle;
	private String reviewText;
	private boolean recommend;
	private boolean purchase;
	private boolean visitNewCar;
	private boolean visitServiceCar;
	private boolean visitUsedCar;
	private boolean isDealer;
	private boolean noCustomerServiceRating;
	private boolean noBuyingRating;
	private boolean noRepairsRating;
	private boolean noFacilitiesRating;
	private boolean isFeatured;
	private String reviewerName;
	private String reviewerLoc;
	private String promoCode;
	private String overallRating;
	private String customerServiceRating;
	private String buyingProcessRating;
	private String qualityOfRepairRating;
	private String facilityRating;
	private String reviewerEmail;
	private String dealerId;
	private String helpfulCount;
	private String unHelpfulCount;
	private String reviewId;
	private String reviewDate;
	private String ipAddress;
	
	public DealerReviewView(){
		
	}
	public DealerReviewView(String reviewTitle, String reviewText, boolean recommend, boolean purchase,
			boolean visitNewCar, boolean visitServiceCar, boolean visitUsedCar, boolean isDealer,
			boolean noCustomerServiceRating, boolean noBuyingRating, boolean noRepairsRating,
			boolean noFacilitiesRating, String reviewerName, String reviewerLoc, String promoCode,
			String overallRating, String customerServiceRating, String buyingProcessRating, String qualityOfRepairRating,
			String facilityRating, String reviewerEmail, String dealerId, String ipAddress) {
		this.reviewTitle = reviewTitle;
		this.reviewText = reviewText;
		this.recommend = recommend;
		this.purchase = purchase;
		this.visitNewCar = visitNewCar;
		this.visitServiceCar = visitServiceCar;
		this.visitUsedCar = visitUsedCar;
		this.isDealer = isDealer;
		this.noCustomerServiceRating = noCustomerServiceRating;
		this.noBuyingRating = noBuyingRating;
		this.noRepairsRating = noRepairsRating;
		this.noFacilitiesRating = noFacilitiesRating;
		this.reviewerName = reviewerName;
		this.reviewerLoc = reviewerLoc;
		this.promoCode = promoCode;
		this.overallRating = overallRating;
		this.customerServiceRating = customerServiceRating;
		this.buyingProcessRating = buyingProcessRating;
		this.qualityOfRepairRating = qualityOfRepairRating;
		this.facilityRating = facilityRating;
		this.reviewerEmail = reviewerEmail;
		this.dealerId = dealerId;
		this.ipAddress = ipAddress;
	}

	private DealerResponseView dealerResponseView;

	public String getReviewTitle() {
		return reviewTitle;
	}

	public void setReviewTitle(String reviewTitle) {
		this.reviewTitle = reviewTitle;
	}

	public String getReviewText() {
		return reviewText;
	}

	public void setReviewText(String reviewText) {
		this.reviewText = reviewText;
	}


	public String getReviewerName() {
		return reviewerName;
	}

	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}

	public String getReviewerLoc() {
		return reviewerLoc;
	}

	public void setReviewerLoc(String reviewerLoc) {
		this.reviewerLoc = reviewerLoc;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}


	public String getOverallRating() {
		return overallRating;
	}

	public void setOverallRating(String overallRating) {
		this.overallRating = overallRating;
	}

	public String getFacilityRating() {
		return facilityRating;
	}

	public void setFacilityRating(String facilityRating) {
		this.facilityRating = facilityRating;
	}

	public String getReviewerEmail() {
		return reviewerEmail;
	}

	public void setReviewerEmail(String reviewerEmail) {
		this.reviewerEmail = reviewerEmail;
	}

	public String getDealerId() {
		return dealerId;
	}

	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	public String getHelpfulCount() {
		return helpfulCount;
	}

	public void setHelpfulCount(String helpfulCount) {
		this.helpfulCount = helpfulCount;
	}

	public String getUnHelpfulCount() {
		return unHelpfulCount;
	}

	public void setUnHelpfulCount(String unHelpfulCount) {
		this.unHelpfulCount = unHelpfulCount;
	}

	public String getReviewId() {
		return reviewId;
	}

	public void setReviewId(String reviewId) {
		this.reviewId = reviewId;
	}



	public DealerResponseView getDealerResponseView() {
		return dealerResponseView;
	}

	public void setDealerResponseView(DealerResponseView dealerResponseView) {
		this.dealerResponseView = dealerResponseView;
	}

	public String getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(String reviewDate) {
		this.reviewDate = reviewDate;
	}

	public boolean getRecommend() {
		return recommend;
	}

	public void setRecommend(boolean recommend) {
		this.recommend = recommend;
	}

	public boolean getPurchase() {
		return purchase;
	}

	public void setPurchase(boolean purchase) {
		this.purchase = purchase;
	}

	public boolean getVisitNewCar() {
		return visitNewCar;
	}

	public void setVisitNewCar(boolean visitNewCar) {
		this.visitNewCar = visitNewCar;
	}

	public boolean getVisitServiceCar() {
		return visitServiceCar;
	}

	public void setVisitServiceCar(boolean visitServiceCar) {
		this.visitServiceCar = visitServiceCar;
	}

	public boolean getVisitUsedCar() {
		return visitUsedCar;
	}

	public void setVisitUsedCar(boolean visitUsedCar) {
		this.visitUsedCar = visitUsedCar;
	}

	public boolean getIsDealer() {
		return isDealer;
	}

	public void setIsDealer(boolean isDealer) {
		this.isDealer = isDealer;
	}

	public boolean getNoCustomerServiceRating() {
		return noCustomerServiceRating;
	}

	public void setNoCustomerServiceRating(boolean noCustomerServiceRating) {
		this.noCustomerServiceRating = noCustomerServiceRating;
	}

	public boolean getNoBuyingRating() {
		return noBuyingRating;
	}

	public void setNoBuyingRating(boolean noBuyingRating) {
		this.noBuyingRating = noBuyingRating;
	}

	public boolean getNoRepairsRating() {
		return noRepairsRating;
	}

	public void setNoRepairsRating(boolean noRepairsRating) {
		this.noRepairsRating = noRepairsRating;
	}

	public boolean getNoFacilitiesRating() {
		return noFacilitiesRating;
	}

	public void setNoFacilitiesRating(boolean noFacilitiesRating) {
		this.noFacilitiesRating = noFacilitiesRating;
	}

	public boolean getIsFeatured() {
		return isFeatured;
	}

	public void setIsFeatured(boolean isFeatured) {
		this.isFeatured = isFeatured;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getCustomerServiceRating() {
		return customerServiceRating;
	}
	public void setCustomerServiceRating(String customerServiceRating) {
		this.customerServiceRating = customerServiceRating;
	}
	public String getBuyingProcessRating() {
		return buyingProcessRating;
	}
	public void setBuyingProcessRating(String buyingProcessRating) {
		this.buyingProcessRating = buyingProcessRating;
	}
	public String getQualityOfRepairRating() {
		return qualityOfRepairRating;
	}
	public void setQualityOfRepairRating(String qualityOfRepairRating) {
		this.qualityOfRepairRating = qualityOfRepairRating;
	}
}
