package com.cars.df.customerinquiry.bo;

public class DealerAmenity {

    private String category;
    private String code;
    private String description;
    private String freeFormText;
    
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFreeFormText() {
		return freeFormText;
	}
	public void setFreeFormText(String freeFormText) {
		this.freeFormText = freeFormText;
	}
}
