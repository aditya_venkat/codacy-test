package com.cars.df.customerinquiry.bo;

public class SortType {

	private String attribute;
	private String order;
	
	public SortType(String attribute, String order){
		this.attribute = attribute;
		this.order = order;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
}
