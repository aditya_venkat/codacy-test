package com.cars.df.customerinquiry.bo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cars.api.schemas.dealer_review.DealerReview;

public class DealerOverviewDetails extends DealerDetails {



	private List<String> photoUrlList;
	private String promoVideoUrl;
	private String logoUrl; 
	private String dealerWebsiteLink;
		
	
	private Map<String, String> salesHoursOperation;

	private Map<String, String> serviceHoursOperation;
		
	private Boolean hasDealerReview;
	private DealerReviewSummaryView dealerReviewSummaryView;

	
	private boolean hasInventory;
	private String newInventoryCount;
	private String usedInventoryCount;
	
	private Boolean hasServiceRepair;
	private Boolean rpcCertified;
	private String serviceAppointmentUrl;
	
	//Whats is service coupons available? How are we determining this?

	private Boolean hasSellAndTrade;

	
	private Boolean hasSpecialOffer;
	private String offerAndModelString;
	//find a better way of getting offers and models
	
	
	private Boolean hasAboutUs;
	private String memberSince;
	private String dealerDescription;
	
	//add a DDCS about us object for retrreving ddcs object
	
	
	//Unknown stuffs..will find out more once we progress
	private String dealerType;

	
	public List<String> getPhotoUrlList() {
		return photoUrlList;
	}

	public void setPhotoUrlList(List<String> photoUrlList) {
		this.photoUrlList = photoUrlList;
	}

	public String getPromoVideoUrl() {
		return promoVideoUrl;
	}

	public void setPromoVideoUrl(String promoVideoUrl) {
		this.promoVideoUrl = promoVideoUrl;
	}
	
	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getDealerWebsiteLink() {
		return dealerWebsiteLink;
	}

	public void setDealerWebsiteLink(String dealerWebsiteLink) {
		this.dealerWebsiteLink = dealerWebsiteLink;
	}

	public Map<String, String> getSalesHoursOperation() {
		return salesHoursOperation;
	}

	public void setSalesHoursOperation(Map<String, String> salesHoursOperation) {
		this.salesHoursOperation = salesHoursOperation;
	}
	
	public void addSalesHours(String key, String value) {
		if(salesHoursOperation == null){
			salesHoursOperation = new HashMap<String, String>();
		}
		salesHoursOperation.put(key, value);
	}


	public Map<String, String> getServiceHoursOperation() {
		return serviceHoursOperation;
	}

	public void setServiceHoursOperation(Map<String, String> serviceHoursOperation) {
		this.serviceHoursOperation = serviceHoursOperation;
	}
	
	public void addServiceHours(String key, String value) {
		if(serviceHoursOperation == null){
			serviceHoursOperation = new HashMap<String, String>();
		}
		serviceHoursOperation.put(key, value);
	}


	public Boolean getHasDealerReview() {
		return hasDealerReview;
	}

	public void setHasDealerReview(Boolean hasDealerReview) {
		this.hasDealerReview = hasDealerReview;
	}

	public DealerReviewSummaryView getDealerReviewSummaryView() {
		return dealerReviewSummaryView;
	}

	public void setDealerReviewSummaryView(DealerReviewSummaryView dealerReviewSummaryView) {
		this.dealerReviewSummaryView = dealerReviewSummaryView;
	}
	
	public boolean isHasInventory() {
		return hasInventory;
	}

	public void setHasInventory(boolean hasInventory) {
		this.hasInventory = hasInventory;
	}

	public String getNewInventoryCount() {
		return newInventoryCount;
	}

	public void setNewInventoryCount(String newInventoryCount) {
		this.newInventoryCount = newInventoryCount;
	}

	public String getUsedInventoryCount() {
		return usedInventoryCount;
	}

	public void setUsedInventoryCount(String usedInventoryCount) {
		this.usedInventoryCount = usedInventoryCount;
	}

	public Boolean getHasServiceRepair() {
		return hasServiceRepair;
	}

	public void setHasServiceRepair(Boolean hasServiceRepair) {
		this.hasServiceRepair = hasServiceRepair;
	}

	public Boolean getRpcCertified() {
		return rpcCertified;
	}

	public void setRpcCertified(Boolean rpcCertified) {
		this.rpcCertified = rpcCertified;
	}

	public String getServiceAppointmentUrl() {
		return serviceAppointmentUrl;
	}

	public void setServiceAppointmentUrl(String serviceAppointmentUrl) {
		this.serviceAppointmentUrl = serviceAppointmentUrl;
	}

	public Boolean getHasSellAndTrade() {
		return hasSellAndTrade;
	}

	public void setHasSellAndTrade(Boolean hasSellAndTrade) {
		this.hasSellAndTrade = hasSellAndTrade;
	}

	public Boolean getHasSpecialOffer() {
		return hasSpecialOffer;
	}

	public void setHasSpecialOffer(Boolean hasSpecialOffer) {
		this.hasSpecialOffer = hasSpecialOffer;
	}
	public String getOfferAndModelString() {
		return offerAndModelString;
	}

	public void setOfferAndModelString(String offerAndModelString) {
		this.offerAndModelString = offerAndModelString;
	}



	public Boolean getHasAboutUs() {
		return hasAboutUs;
	}

	public void setHasAboutUs(Boolean hasAboutUs) {
		this.hasAboutUs = hasAboutUs;
	}

	public String getMemberSince() {
		return memberSince;
	}

	public void setMemberSince(String memberSince) {
		this.memberSince = memberSince;
	}


	public String getDealerType() {
		return dealerType;
	}

	public void setDealerType(String dealerType) {
		this.dealerType = dealerType;
	}

	public String getDealerDescription() {
		return dealerDescription;
	}

	public void setDealerDescription(String dealerDescription) {
		this.dealerDescription = dealerDescription;
	}



	

	
	

	
}
