package com.cars.df.customerinquiry.bo;

import java.util.ArrayList;
import java.util.List;

public class DealerSearchResults {

	
	List<DealerSummary> dealerSummaryList;
	
	List<DirectoryRefinement> refinementList;
	
	List<DirectoryBreadCrumb> breadCrumbList;
	
	private Pagination pagination;
	
	private Sort sort;
	
	private String zipCode;
	
	private String radius;
	
	private Long totalDealers;
	
	

	public List<DealerSummary> getDealerSummaryList() {
		return dealerSummaryList;
	}

	public void setDealerSummaryList(List<DealerSummary> dealerSummaryList) {
		this.dealerSummaryList = dealerSummaryList;
	}
	
	public void addDealerSummary(DealerSummary dealerSummary) {
		
		if(dealerSummaryList == null){
			dealerSummaryList = new ArrayList<DealerSummary>();
		}
		dealerSummaryList.add(dealerSummary);
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	public Sort getSort() {
		return sort;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public Long getTotalDealers() {
		return totalDealers;
	}

	public void setTotalDealers(Long totalDealers) {
		this.totalDealers = totalDealers;
	}

	public List<DirectoryRefinement> getRefinementList() {
		return refinementList;
	}

	public void setRefinementList(List<DirectoryRefinement> refinementList) {
		this.refinementList = refinementList;
	}
	public void addRefinement(DirectoryRefinement refinement) {
		
		if(refinementList == null){
			refinementList = new ArrayList<DirectoryRefinement>();
		}
		
		refinementList.add(refinement);
	}
	
	public List<DirectoryBreadCrumb> getBreadCrumbList() {
		return breadCrumbList;
	}

	public void setBreadCrumbList(List<DirectoryBreadCrumb> breadCrumbList) {
		this.breadCrumbList = breadCrumbList;
	}
	
	public void addBreadCrumb(DirectoryBreadCrumb breadCrumb) {
		
		if(breadCrumbList == null){
			breadCrumbList = new ArrayList<DirectoryBreadCrumb>();
		}
		breadCrumbList.add(breadCrumb);
	}
}
