package com.cars.df.customerinquiry.bo;

public class DealerLocalOfferCash extends DealerLocalOffer {

	private Double cashbackAmount;
	private String cashLabel;
	
	
	public Double getCashbackAmount() {
		return cashbackAmount;
	}
	public void setCashbackAmount(Double cashbackAmount) {
		this.cashbackAmount = cashbackAmount;
	}
	public String getCashLabel() {
		return cashLabel;
	}
	public void setCashLabel(String cashLabel) {
		this.cashLabel = cashLabel;
	}
}
