package com.cars.df.customerinquiry.bo;

public class Pagination {

	private Integer page;
	private Integer perPage;
	private Integer numberOfPages;
	
	public Pagination(int page, int perPage, int numberOfPages) {
	       this.page = page;
	       this.perPage = perPage;
	       this.numberOfPages = numberOfPages;
	   }

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPerPage() {
		return perPage;
	}

	public void setPerPage(Integer perPage) {
		this.perPage = perPage;
	}

	public Integer getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(Integer numberOfPages) {
		this.numberOfPages = numberOfPages;
	}
	
}
