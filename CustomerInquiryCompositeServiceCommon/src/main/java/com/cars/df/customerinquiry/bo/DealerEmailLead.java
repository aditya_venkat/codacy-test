package com.cars.df.customerinquiry.bo;

public class DealerEmailLead {

	protected String firstName;
	protected String lastName;
	protected String email;
	protected String zipCode;
	protected String eveningNumber;
	protected String daytimeNumber;
	protected String comments;
	protected String odsId;
	protected String dealerName;
	protected String dealerEmail;
	protected String dealerNumber;
	protected String leadType;
	protected String isNew;
	protected String phone;
	protected String[] phonetype;
	protected String jscCollector;
	protected String webPageTypeId;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getEveningNumber() {
		return eveningNumber;
	}
	public void setEveningNumber(String eveningNumber) {
		this.eveningNumber = eveningNumber;
	}
	public String getDaytimeNumber() {
		return daytimeNumber;
	}
	public void setDaytimeNumber(String daytimeNumber) {
		this.daytimeNumber = daytimeNumber;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getOdsId() {
		return odsId;
	}
	public void setOdsId(String odsId) {
		this.odsId = odsId;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getDealerEmail() {
		return dealerEmail;
	}
	public void setDealerEmail(String dealerEmail) {
		this.dealerEmail = dealerEmail;
	}
	public String getDealerNumber() {
		return dealerNumber;
	}
	public void setDealerNumber(String dealerNumber) {
		this.dealerNumber = dealerNumber;
	}
	public String getLeadType() {
		return leadType;
	}
	public void setLeadType(String leadType) {
		this.leadType = leadType;
	}
	public String getIsNew() {
		return isNew;
	}
	public void setIsNew(String isNew) {
		this.isNew = isNew;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String[] getPhonetype() {
		return phonetype;
	}
	public void setPhonetype(String[] phonetype) {
		this.phonetype = phonetype;
	}
	public String getJscCollector() {
		return jscCollector;
	}
	public void setJscCollector(String jscCollector) {
		this.jscCollector = jscCollector;
	}
	public String getWebPageTypeId() {
		return webPageTypeId;
	}
	public void setWebPageTypeId(String webPageTypeId) {
		this.webPageTypeId = webPageTypeId;
	}

}
