package com.cars.df.customerinquiry.bo;


public class DealerReviewDetails extends DealerDetails {

	private DealerReviewSummaryView dealerReviewSummaryView;

	public DealerReviewSummaryView getDealerReviewSummaryView() {
		return dealerReviewSummaryView;
	}

	public void setDealerReviewSummaryView(DealerReviewSummaryView dealerReviewSummaryView) {
		this.dealerReviewSummaryView = dealerReviewSummaryView;
	}
	
	
}
