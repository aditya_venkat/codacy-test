package com.cars.df.customerinquiry.bo;

import java.util.ArrayList;
import java.util.List;

public class DirectoryBreadCrumb {

	private String id;
	private String name;
	private List<DirectoryBreadCrumbValue> breadCrumbValues;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<DirectoryBreadCrumbValue> breadCrumbValues() {
		return breadCrumbValues;
	}
	public void setBreadCrumbValues(List<DirectoryBreadCrumbValue> breadCrumbValues) {
		this.breadCrumbValues = breadCrumbValues;
	}
	
	public void addBreadCrumbValue(DirectoryBreadCrumbValue breadCrumbValue) {
		if(breadCrumbValues == null){
			breadCrumbValues = new ArrayList<DirectoryBreadCrumbValue>();
		}
		
		breadCrumbValues.add(breadCrumbValue);
		
	}
}
