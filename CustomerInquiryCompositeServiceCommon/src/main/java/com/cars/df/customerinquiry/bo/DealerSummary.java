package com.cars.df.customerinquiry.bo;

import java.util.ArrayList;
import java.util.List;

public class DealerSummary {

	private String name;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String zip;
	private String rating;
	private String totalReviewsNum;
	private Boolean rpcCertified;
	private String dealerType;
	private String partyId;
	private String customerId; 
	private String logoUrl; 
	private String newCarTollFreePhoneNumber; 
	private String usedCarTollFreePhoneNumber; 
	private String serviceTollFreePhoneNumber; 
	private String promoTagLine; 
	private Boolean hasSellAndTradeMarket;
	private Boolean hasServiceAndRepairContent;
	private String websiteUrl;
	private String newCarEmail;
	private String usedCarEmail;
	private String salesHours;
	private String serviceHours;
	private List<String> productList;
	private String serviceAppointmentURl;
	private String dealerLongDescription;
	private String memberSince;
	private String photos;
	private String promoUrl;

	private Double distance;
	private Double latitude;
	private Double longitude;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getTotalReviewsNum() {
		return totalReviewsNum;
	}
	public void setTotalReviewsNum(String totalReviewsNum) {
		this.totalReviewsNum = totalReviewsNum;
	}
	public Boolean getRpcCertified() {
		return rpcCertified;
	}
	public void setRpcCertified(Boolean rpcCertified) {
		this.rpcCertified = rpcCertified;
	}
	public String getDealerType() {
		return dealerType;
	}
	public void setDealerType(String dealerType) {
		this.dealerType = dealerType;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getNewCarTollFreePhoneNumber() {
		return newCarTollFreePhoneNumber;
	}
	public void setNewCarTollFreePhoneNumber(String newCarTollFreePhoneNumber) {
		this.newCarTollFreePhoneNumber = newCarTollFreePhoneNumber;
	}
	public String getUsedCarTollFreePhoneNumber() {
		return usedCarTollFreePhoneNumber;
	}
	public void setUsedCarTollFreePhoneNumber(String usedCarTollFreePhoneNumber) {
		this.usedCarTollFreePhoneNumber = usedCarTollFreePhoneNumber;
	}
	public String getServiceTollFreePhoneNumber() {
		return serviceTollFreePhoneNumber;
	}
	public void setServiceTollFreePhoneNumber(String serviceTollFreePhoneNumber) {
		this.serviceTollFreePhoneNumber = serviceTollFreePhoneNumber;
	}
	public String getPromoTagLine() {
		return promoTagLine;
	}
	public void setPromoTagLine(String promoTagLine) {
		this.promoTagLine = promoTagLine;
	}
	public Boolean getHasSellAndTradeMarket() {
		return hasSellAndTradeMarket;
	}
	public void setHasSellAndTradeMarket(Boolean hasSellAndTradeMarket) {
		this.hasSellAndTradeMarket = hasSellAndTradeMarket;
	}
	public Boolean getHasServiceAndRepairContent() {
		return hasServiceAndRepairContent;
	}
	public void setHasServiceAndRepairContent(Boolean hasServiceAndRepairContent) {
		this.hasServiceAndRepairContent = hasServiceAndRepairContent;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}
	public String getNewCarEmail() {
		return newCarEmail;
	}
	public void setNewCarEmail(String newCarEmail) {
		this.newCarEmail = newCarEmail;
	}
	public String getUsedCarEmail() {
		return usedCarEmail;
	}
	public void setUsedCarEmail(String usedCarEmail) {
		this.usedCarEmail = usedCarEmail;
	}
	public String getSalesHours() {
		return salesHours;
	}
	public void setSalesHours(String salesHours) {
		this.salesHours = salesHours;
	}
	public String getServiceHours() {
		return serviceHours;
	}
	public void setServiceHours(String serviceHours) {
		this.serviceHours = serviceHours;
	}
	public List<String> getProductList() {
		return productList;
	}
	public void setProductList(List<String> productList) {
		this.productList = productList;
	}
	public void addProduct(String product) {
		if(productList == null){
			productList = new ArrayList<String>();
		}
		productList.add(product);
	}
	
	public String getServiceAppointmentURl() {
		return serviceAppointmentURl;
	}
	public void setServiceAppointmentURl(String serviceAppointmentURl) {
		this.serviceAppointmentURl = serviceAppointmentURl;
	}
	public String getDealerLongDescription() {
		return dealerLongDescription;
	}
	public void setDealerLongDescription(String dealerLongDescription) {
		this.dealerLongDescription = dealerLongDescription;
	}
	public String getMemberSince() {
		return memberSince;
	}
	public void setMemberSince(String memberSince) {
		this.memberSince = memberSince;
	}
	public String getPhotos() {
		return photos;
	}
	public void setPhotos(String photos) {
		this.photos = photos;
	}
	public String getPromoUrl() {
		return promoUrl;
	}
	public void setPromoUrl(String promoUrl) {
		this.promoUrl = promoUrl;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
}
