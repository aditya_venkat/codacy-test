package com.cars.df.customerinquiry.bo;

public class DealerLocalOffer {

	private String type;
	private Long offerId;
	private String description;
	private String partyId;
	private String productId;
	private String stockType;
	private String vehicleYearId;
	private String vehicleMakeId;
	private String vehicleModelId;
	private String vehicleTrimId;
	private String vehicleBodyStyleId;
	private String vehicleYearDescription;
	private String vehicleMakeDescription;
	private String vehicleModelDescription;
	private String vehicleTrimDescription;
	private String vehicleBodyStyleDescription;
	private String stockNumber;
	private String offerDisclaimer;
	private String startDate;
	private String endDate;
	private Boolean isDisplay;
	private Boolean isOem;
	private Boolean isCertified;
	private String offerTitle;
	private String offerDescription;
	private String photoUrl;
	private String listingId;
		
	//private List<OfferMedia> mediaList;
	//private List<OfferLease> leaseList;
	//private List<offerFiannce> financeList;
	//private OfferCash offerCash;
	//private OfferService offerService;
	//private OfferAddOn offerAddOn;
	//private OfferDealerPromo offerDealerPromo;
	/*

public String getOfferTitle() {
		StringBuffer retval = new StringBuffer();
		
		if (this.getType().getCode().equals("CASH")) {
			Formatter f = new Formatter(retval, Locale.US);
			if (this.getCash().getAmount() - this.getCash().getAmount().intValue() > 0) {
				f.format("$%.2f " + this.getCash().getCashLabel(), this.getCash().getAmount());
			} else {
				f.format("$%.0f " + this.getCash().getCashLabel(), this.getCash().getAmount());
			}
			
		} else if (this.getType().getCode().equals("LEASE")) {
			if (this.getLease().size() > 1) {
				retval.append("Dealership lease specials");
				
			} else {
				OfferLease ol = this.getLease().get(0);
				Formatter f = new Formatter(retval, Locale.US);
				f.format("%s/month for %.0f months", this.formatCurrency(ol.getMonthlyPayment()), ol.getMonthlyTerm());
				
				if ((ol.getDueAtSigning() != null) && (ol.getDueAtSigning() != 0)) {
					f.format(" + %s due at signing", this.formatCurrency(ol.getDueAtSigning()));
				}
			}
			
		} else if (this.getType().getCode().equals("FINANCE")) {
			if (this.getFinancing().size() > 1) {
				if (this.isOemInd()) {
					retval.append("Manufacturer financing specials");
				} else {
					retval.append("Dealership financing specials");
				}
				
			} else {
				OfferFinance of = this.getFinancing().get(0);
				Formatter f = new Formatter(retval, Locale.US);
				f.format("%s for %.0f months", this.formatPercentage(of.getApr()), of.getTerm());
				
				if ((of.getBonusCash() != null) && (of.getBonusCash() != 0)) {
					f.format(" + %s Bonus Cash", this.formatCurrency(of.getBonusCash()));
				}
			}
			
		} else if (this.getType().getCode().equals("SERVICE")) {
			retval.append(this.getService().getTitle());
			
		} else if (this.getType().getCode().equals("ADD_ON")) {
			retval.append(this.getAddOn().getTitle());
			
		} else {
			retval.append("Offer title coming...");
			
		}
		
		return retval.toString();
	}
	
	public String getOfferDescription() {
		String retval = null;

		if (getDealerPromo() != null) {
			retval = getDealerPromo().getDescription();
			
		} else if (getService() != null) {
			retval = getService().getDescription();
			
		} else if (getAddOn() != null) {
			retval = getAddOn().getDescription();
			
		}

		return retval;
	}
	
	public String getExpirationDate() {
		Date d = this.getEndDate().toGregorianCalendar().getTime();
		Format f = new SimpleDateFormat("M/d/yy");
		return f.format(d);
	}
	
	public String getLogo() {
		if (!"".equals(getVehicleMakeDescription())) {
			MakeLogosClient mlc = new MakeLogosClient();;
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("http://car-pictures.cars.com/images/?IMG=");
			sb.append(mlc.getLogo(getVehicleMakeDescription()));
			sb.append("&width=90&height=72&AUTOTRIM=1&ACT=F");
			
			return sb.toString();
		} else {
			return null;
		}
	}*/
	
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getOfferId() {
		return offerId;
	}
	public void setOfferId(Long offerId) {
		this.offerId = offerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getStockType() {
		return stockType;
	}
	public void setStockType(String stockType) {
		this.stockType = stockType;
	}
	public String getVehicleYearId() {
		return vehicleYearId;
	}
	public void setVehicleYearId(String vehicleYearId) {
		this.vehicleYearId = vehicleYearId;
	}
	public String getVehicleMakeId() {
		return vehicleMakeId;
	}
	public void setVehicleMakeId(String vehicleMakeId) {
		this.vehicleMakeId = vehicleMakeId;
	}
	public String getVehicleModelId() {
		return vehicleModelId;
	}
	public void setVehicleModelId(String vehicleModelId) {
		this.vehicleModelId = vehicleModelId;
	}
	public String getVehicleTrimId() {
		return vehicleTrimId;
	}
	public void setVehicleTrimId(String vehicleTrimId) {
		this.vehicleTrimId = vehicleTrimId;
	}
	public String getVehicleBodyStyleId() {
		return vehicleBodyStyleId;
	}
	public void setVehicleBodyStyleId(String vehicleBodyStyleId) {
		this.vehicleBodyStyleId = vehicleBodyStyleId;
	}
	public String getVehicleYearDescription() {
		return vehicleYearDescription;
	}
	public void setVehicleYearDescription(String vehicleYearDescription) {
		this.vehicleYearDescription = vehicleYearDescription;
	}
	public String getVehicleMakeDescription() {
		return vehicleMakeDescription;
	}
	public void setVehicleMakeDescription(String vehicleMakeDescription) {
		this.vehicleMakeDescription = vehicleMakeDescription;
	}
	public String getVehicleModelDescription() {
		return vehicleModelDescription;
	}
	public void setVehicleModelDescription(String vehicleModelDescription) {
		this.vehicleModelDescription = vehicleModelDescription;
	}
	public String getVehicleTrimDescription() {
		return vehicleTrimDescription;
	}
	public void setVehicleTrimDescription(String vehicleTrimDescription) {
		this.vehicleTrimDescription = vehicleTrimDescription;
	}
	public String getVehicleBodyStyleDescription() {
		return vehicleBodyStyleDescription;
	}
	public void setVehicleBodyStyleDescription(String vehicleBodyStyleDescription) {
		this.vehicleBodyStyleDescription = vehicleBodyStyleDescription;
	}
	public String getStockNumber() {
		return stockNumber;
	}
	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}
	public String getOfferDisclaimer() {
		return offerDisclaimer;
	}
	public void setOfferDisclaimer(String offerDisclaimer) {
		this.offerDisclaimer = offerDisclaimer;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Boolean getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	public Boolean getIsOem() {
		return isOem;
	}
	public void setIsOem(Boolean isOem) {
		this.isOem = isOem;
	}
	public Boolean getIsCertified() {
		return isCertified;
	}
	public void setIsCertified(Boolean isCertified) {
		this.isCertified = isCertified;
	}
	public String getOfferTitle() {
		return offerTitle;
	}
	public void setOfferTitle(String offerTitle) {
		this.offerTitle = offerTitle;
	}
	public String getOfferDescription() {
		return offerDescription;
	}
	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public String getListingId() {
		return listingId;
	}
	public void setListingId(String listingId) {
		this.listingId = listingId;
	}


	
	
}
