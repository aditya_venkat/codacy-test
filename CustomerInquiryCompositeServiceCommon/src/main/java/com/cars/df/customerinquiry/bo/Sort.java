package com.cars.df.customerinquiry.bo;

public class Sort {

	private SortType primary;
	private SortType secondary;
	private SortType tertiary;
	
	public Sort(SortType primary, SortType secondary, SortType tertiary){
		this.primary = primary;
		this.secondary = secondary;
		this.tertiary = tertiary;
	}
	
	public SortType getPrimary() {
		return primary;
	}
	public void setPrimary(SortType primary) {
		this.primary = primary;
	}
	public SortType getSecondary() {
		return secondary;
	}
	public void setSecondary(SortType secondary) {
		this.secondary = secondary;
	}

	public SortType getTertiary() {
		return tertiary;
	}

	public void setTertiary(SortType tertiary) {
		this.tertiary = tertiary;
	}
}
