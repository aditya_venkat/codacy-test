package com.cars.df.customerinquiry.bo;

public class DealerLocalOfferLease extends DealerLocalOffer {

	private String monthlyPayment;
	private String term;
	private String dueAtSigning;
	
	
	public String getMonthlyPayment() {
		return monthlyPayment;
	}
	public void setMonthlyPayment(String monthlyPayment) {
		this.monthlyPayment = monthlyPayment;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getDueAtSigning() {
		return dueAtSigning;
	}
	public void setDueAtSigning(String dueAtSigning) {
		this.dueAtSigning = dueAtSigning;
	}
	
}
