package com.cars.df.customerinquiry.bo;

import java.util.ArrayList;
import java.util.List;

public class YearMakeModelOffer {

	private String name;
	
	private Integer count;
	
	private List<DealerLocalOfferFinance> financeOffers;
	
	private List<DealerLocalOfferLease> leaseOffers;
	
	private List<DealerLocalOfferAddOn> addonOffers;
	
	private List<DealerLocalOfferCash> cashOffers;
	
	private List<DealerLocalOfferDealerPromo> dealerpromoOffers;
	
	private List<DealerLocalOfferService> serviceOffers;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCount() {
		return count;
	}
	
	public void incrementCount(){
		if(count == null){
			count = 0;
		}
		count++;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<DealerLocalOfferFinance> getFinanceOffers() {
		return financeOffers;
	}

	public void setFinanceOffers(List<DealerLocalOfferFinance> financeOffers) {
		this.financeOffers = financeOffers;
	}
	
	public void addFinanceOffer(DealerLocalOfferFinance financeOffer) {
		if(financeOffers == null){
			financeOffers = new ArrayList<DealerLocalOfferFinance>();
		}
		
		financeOffers.add(financeOffer);
	}


	public List<DealerLocalOfferLease> getLeaseOffers() {
		return leaseOffers;
	}

	public void setLeaseOffers(List<DealerLocalOfferLease> leaseOffers) {
		this.leaseOffers = leaseOffers;
	}
	
	public void addLeaseOffer(DealerLocalOfferLease leaseOffer) {
		if(leaseOffers == null){
			leaseOffers = new ArrayList<DealerLocalOfferLease>();
		}
		
		leaseOffers.add(leaseOffer);
	}

	public List<DealerLocalOfferAddOn> getAddonOffers() {
		return addonOffers;
	}

	public void setAddonOffers(List<DealerLocalOfferAddOn> addonOffers) {
		this.addonOffers = addonOffers;
	}
	
	public void addAddonOffer(DealerLocalOfferAddOn addonOffer) {
		if(addonOffers == null){
			addonOffers = new ArrayList<DealerLocalOfferAddOn>();
		}
		
		addonOffers.add(addonOffer);
	}

	public List<DealerLocalOfferCash> getCashOffers() {
		return cashOffers;
	}

	public void setCashOffers(List<DealerLocalOfferCash> cashOffers) {
		this.cashOffers = cashOffers;
	}

	public void addCashOffer(DealerLocalOfferCash cashOffer) {
		if(cashOffers == null){
			cashOffers = new ArrayList<DealerLocalOfferCash>();
		}
		
		cashOffers.add(cashOffer);
	}
	
	public List<DealerLocalOfferDealerPromo> getDealerpromoOffers() {
		return dealerpromoOffers;
	}

	public void setDealerpromoOffers(List<DealerLocalOfferDealerPromo> dealerpromoOffers) {
		this.dealerpromoOffers = dealerpromoOffers;
	}
	
	public void addDealerpromoOffer(DealerLocalOfferDealerPromo dealerpromoOffer) {
		if(dealerpromoOffers == null){
			dealerpromoOffers = new ArrayList<DealerLocalOfferDealerPromo>();
		}
		
		dealerpromoOffers.add(dealerpromoOffer);
	}

	public List<DealerLocalOfferService> getServiceOffers() {
		return serviceOffers;
	}

	public void setServiceOffers(List<DealerLocalOfferService> serviceOffers) {
		this.serviceOffers = serviceOffers;
	}
	
	public void addServiceOffer(DealerLocalOfferService serviceOffer) {
		if(serviceOffers == null){
			serviceOffers = new ArrayList<DealerLocalOfferService>();
		}
		
		serviceOffers.add(serviceOffer);
	}
	
}
